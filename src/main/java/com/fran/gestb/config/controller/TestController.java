package com.fran.gestb.config.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.access.model.User;
import com.fran.gestb.access.service.UserService;
import com.fran.gestb.config.model.Admin;
import com.fran.gestb.config.service.AdminService;

@RestController
@RequestMapping("/databases")
public class TestController {

	@Autowired
	private AdminService adminService; 
	
	@Autowired
	private UserService userService; 
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/admin")
	public void registrarAdmin(@RequestBody Admin admin) {
		adminService.registrar(admin);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/user")
	public void registrarUser(@RequestBody User usuario) {
		userService.registrar(usuario);
	}
}
