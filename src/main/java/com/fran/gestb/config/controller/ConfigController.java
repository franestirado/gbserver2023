package com.fran.gestb.config.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.SQLException;
import com.fran.gestb.config.model.Bares;
import com.fran.gestb.config.service.ConfigService;


@RestController
@RequestMapping("/config")
public class ConfigController {
	@Autowired
	private ConfigService configService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	// --------------------------------- Config ------------------------------------------------
	// -------------------Config----->getAllBares-------------------------------------------
	@GetMapping(value = "/getAllBares")
	@ResponseStatus(HttpStatus.OK)
	public List<Bares> getAllBares() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->ConfigController->getAllBares","Entering","..>>...................");
		List<Bares> baresList = configService.getAllBares();
		if (baresList == null) throw new EmptyFieldException("No Bar Data found");
		consoleLog.logMsg(showLogMsg,"->ConfigController->getAllBares","Exiting","...................>>..");
		return baresList; 
	}
	// -------------------Config----->createOneBar------------------------------------------------
	@PostMapping(value = "/createOneBar")
	@ResponseStatus(HttpStatus.CREATED)
	public Bares createOneBar(@RequestBody Bares newBar) {
		consoleLog.logMsg(showLogMsg,"->ConfigController->createOneBar","Entering","..>>...................");
		Bares checkBar = configService.createOneBar(newBar);
		consoleLog.logMsg(showLogMsg,"->ConfigController->createOneBar","Exiting","...................>>..");
		return checkBar;	
	}
	// -------------------Config----->getOneBar------------------------------------------------
	@PostMapping(value="/getOneBar")
	@ResponseStatus(HttpStatus.CREATED)
	public Bares getOneBar(@RequestBody Bares modBar) {
		consoleLog.logMsg(showLogMsg,"->ConfigController->getOneBar","Entering","..>>...................");
		configService.getOneBar(modBar);
		consoleLog.logMsg(showLogMsg,"->ConfigController->getOneBar","Exiting","...................>>..");
		return modBar;
	}
	// -------------------Config----->updateOneBar------------------------------------------------
	@PostMapping(value="/updateOneBar")
	@ResponseStatus(HttpStatus.OK)
	public Bares updateOneBar(@RequestBody Bares updBar) {
		consoleLog.logMsg(showLogMsg,"->ConfigController->updateOneBar","Entering","..>>...................");
		Bares checkBar = configService.updateOneBar(updBar);
		consoleLog.logMsg(showLogMsg,"->ConfigController->updateOneBar","Exiting","...................>>..");
		return checkBar;	
	}
	// -------------------Config----->changeWorkingBar------------------------------------------------
	@PostMapping(value="/changeWorkingBar")
	@ResponseStatus(HttpStatus.OK)
	public Bares changeWorkingBar(@RequestBody Bares selectedBar) throws SQLException, EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException, Exception{
		consoleLog.logMsg(showLogMsg,"->ConfigController->changeWorkingBar","Entering","..>>...................");
		Bares checkBar = configService.changeWorkingBar(selectedBar);
		consoleLog.logMsg(showLogMsg,"->ConfigController->changeWorkingBar","Exiting","...................>>..");
		return checkBar;	
	}
	// ------------------- Config----->delOneBar------------------------------------------------
	@PostMapping(value="/delOneBar")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneBar(@RequestBody Bares delBar) {
		consoleLog.logMsg(showLogMsg,"->ConfigController->delOneBar","Entering","..>>...................");
		configService.delOneBar(delBar);
		consoleLog.logMsg(showLogMsg,"->ConfigController->delOneBar","Exiting","...................>>..");
		return;
	}
	// ------------------- Config----->delAllBares------------------------------------------------
	@PostMapping(value="/delAllBares")
	@ResponseStatus(HttpStatus.CREATED)
	public void delAllBares() {
		consoleLog.logMsg(showLogMsg,"->ConfigController->delOneBar","Entering","..>>...................");
		configService.delAllBares();
		consoleLog.logMsg(showLogMsg,"->ConfigController->delOneBar","Exiting","...................>>..");
		return;
	}

}
