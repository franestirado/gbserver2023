package com.fran.gestb.config.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.config.model.Bares;

public interface BaresRepository extends JpaRepository<Bares, Long> {
	Bares findByBarId(Long barId);
	Bares findByBarName(String barName);
	Bares findByBarDbName(String barDbName);

	List<Bares> findAll();
	int deleteAllByBarId(Long barId);

}
