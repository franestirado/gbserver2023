package com.fran.gestb.config.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fran.gestb.config.model.Admin;
import com.fran.gestb.config.repo.IAdminRepo;

@Service
public class AdminService{

	@Autowired
	private IAdminRepo repo;
	
	public void registrar(Admin t) {
		repo.save(t);
	}
}
