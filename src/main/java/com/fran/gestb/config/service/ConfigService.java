package com.fran.gestb.config.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fran.gestb.DSourceDbBarXXMultiTenantManager;
import com.fran.gestb.TenantNotFoundException;
import com.fran.gestb.TenantResolvingException;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.config.model.Bares;
import com.fran.gestb.config.repo.BaresRepository;
import com.fran.gestb.dbbarc.service.FieldsService;

@Service
public class ConfigService {
	@Autowired
	private BaresRepository baresRepository;
	@Autowired
	private FieldsService fieldsService;
	@Autowired
	private DSourceDbBarXXMultiTenantManager multiTenantManager;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	// ------------------------------------Config-----------------------------------------------	
	// -------------------Config----->getAllBares-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Bares> getAllBares() {
		consoleLog.logMsg(showLogMsg,"->ConfigService->getAllBares","Entering","..>>...................");
		List<Bares> baresList = baresRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ConfigService->getAllBares","Exiting","...................>>..");
		return baresList;
	}
	// -------------------Bares----->createOneBar------------------------------------------------
	@Transactional
	public Bares createOneBar(Bares newBar) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException  {
		consoleLog.logMsg(showLogMsg,"->ConfigService->createOneBar","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ((newBar==null)||(newBar.getBarName()=="")||(newBar.getBarDbName()=="")||(newBar.getBarDbUserName()=="")
				||(newBar.getBarDbPassword()=="")||(newBar.getBarDbDriverClassName()=="")) 
			throw new EmptyFieldException("->ConfigService->createOneBar->Null Object or Empty Mandatoy Parameters");
		// Checking attributes of new object */
		if (newBar.checkBarAtributes()==false) 
			throw new WrongParametersException("--Bar Name-->"+newBar.getBarName()+"--Bar Long Name-->"+newBar.getBarLongName());
		// Checking there no other object with same name 
		Bares readBar = baresRepository.findByBarName(newBar.getBarName());
		if (readBar != null) throw new EntityAlreadyExistsException("--Bar Name-->"+newBar.getBarName());
		// Checking there no other object with same reference 
		readBar = baresRepository.findByBarDbName(newBar.getBarDbName());
		if (readBar != null) throw new EntityAlreadyExistsException("--Bar Db Name-->"+newBar.getBarDbName());
		// Checking database connection can be done
		consoleLog.logMsg(showLogMsg,"->ConfigService->createOneBar","checking DataBaseInfo","..>>..................."); 	
		DataSource dataSource = DataSourceBuilder.create()
				.driverClassName(newBar.getBarDbDriverClassName())
				.url(newBar.getBarDbUrl()+newBar.getBarDbName())
				.username(newBar.getBarDbUserName())
				.password(newBar.getBarDbPassword())
				.build();
		// Check that new connection is 'live'.
		try(Connection c = dataSource.getConnection()) {
			readBar = baresRepository.save(newBar);
		}  catch   	(Exception e) {
			throw new EntityNotFoundException("--Wrong Bar Db Info--.>"+newBar.getBarDbName()+"--->"+newBar.getBarDbUserName()); 
		}
		consoleLog.logMsg(showLogMsg,"->ConfigService->createOneBar","Exiting","...................>>..");
		return readBar;
	}
	// -------------------Bares----->getOneBar------------------------------------------------
	@Transactional
	public Bares getOneBar(Bares modBar) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ConfigService->getOneBar","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ((modBar==null) | (modBar.getBarName()=="")) throw new EmptyFieldException("->ConfigService->getOneBar->Null Object or Empty Mandatoy Parameters");	
		Bares readBar = baresRepository.findByBarId(modBar.getBarId());
		if (readBar == null) throw new EntityNotFoundException("--Bar Name-->"+modBar.getBarName());
		consoleLog.logMsg(showLogMsg,"->ConfigService->getOneBar","Exiting","...................>>..");
		return readBar;
	}
	// -------------------Bares----->updateOneBar------------------------------------------------
	@Transactional
	public Bares updateOneBar(Bares modBar) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ConfigService->updateOneBar","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ((modBar==null)||(modBar.getBarName()=="")||(modBar.getBarDbName()=="")||(modBar.getBarDbUserName()=="")
				||(modBar.getBarDbPassword()=="")||(modBar.getBarDbDriverClassName()=="")) 
			throw new EmptyFieldException("->ConfigService->updateOneBar->Null Object or Empty Mandatoy Parameters");	
		// Check data changed, readBar versus modBar
		Bares readBar = baresRepository.findByBarId(modBar.getBarId());
		Bares checkBar;
		// Check if changes in BarName
		if (!modBar.getBarName().matches(readBar.getBarName())) {
			checkBar = baresRepository.findByBarName(modBar.getBarName());
			if (checkBar != null) throw new EntityAlreadyExistsException("--Bar Name in use-->"+modBar.getBarName());
		}
		// Check if changes in BarDbName
		if (!modBar.getBarDbName().matches(readBar.getBarDbName())) {
			checkBar = baresRepository.findByBarDbName(modBar.getBarDbName());
			if (checkBar != null) throw new EntityAlreadyExistsException("--Bar DB Name in use-->"+modBar.getBarDbName());
		}
		// Checking database connection can be done
		consoleLog.logMsg(showLogMsg,"->ConfigService->createOneBar","checking DataBaseInfo","..>>..................."); 	
		DataSource dataSource = DataSourceBuilder.create()
				.driverClassName(modBar.getBarDbDriverClassName())
				.url(modBar.getBarDbUrl()+modBar.getBarDbName())
				.username(modBar.getBarDbUserName())
				.password(modBar.getBarDbPassword())
				.build();
		// Check that new connection is 'live'.
		try(Connection c = dataSource.getConnection()) {
			readBar = baresRepository.save(modBar);
		}  catch   	(Exception e) {
			throw new EntityNotFoundException("--Wrong Bar Db Info--.>"+modBar.getBarDbName()+"--->"+modBar.getBarDbUserName()); 
		}
		consoleLog.logMsg(showLogMsg,"->ConfigService->updateOneBar","Exiting","...................>>..");
		return readBar;
	}
	// -------------------Bares----->changeWorkingBar------------------------------------------------
	@Transactional
	public void setBaresDbConnections() {
		consoleLog.logMsg(showLogMsg,"->ConfigService->setBaresDbConnections","Entering","..<<...................");
		List<Bares> baresList = baresRepository.findAll();
		Bares currentBar;
		int maxDbConnections = 15;
		for(int k = 0;k<baresList.size();k++) {
			currentBar = baresList.get(k);
			if (k < maxDbConnections) multiTenantManager.addTenant(	currentBar.getBarName(),
																	currentBar.getBarDbUrl()+currentBar.getBarDbName(),
																	currentBar.getBarDbUserName(),currentBar.getBarDbPassword());
		}		
		consoleLog.logMsg(showLogMsg,"->ConfigService->setBaresDbConnections","Exiting","..>>...................");
	}
	// -------------------Bares----->changeWorkingBar------------------------------------------------
	@Transactional
	public Bares changeWorkingBar(Bares selectedBar) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException, 
															SQLException, TenantNotFoundException, TenantResolvingException {
		consoleLog.logMsg(showLogMsg,"->ConfigService->changeWorkingBar","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ((selectedBar==null) | (selectedBar.getBarName()=="")) throw new EmptyFieldException("->ConfigService->changeWorkingBar->Null Object or Empty Mandatoy Parameters");	
		Bares readBar = baresRepository.findByBarId(selectedBar.getBarId());
		if (readBar == null) throw new EntityNotFoundException("--Bar Name-->"+selectedBar.getBarName());		
    	multiTenantManager.setCurrentTenant(readBar.getBarName());
    	//multiTenantManager.addTenant(readBar.getBarName(),readBar.getBarDbUrl()+readBar.getBarDbName(),readBar.getBarDbUserName(),readBar.getBarDbPassword());

		fieldsService.setCurrentBarFieldAndValue(selectedBar.getBarName());		
		consoleLog.logMsg(showLogMsg,"->ConfigService->changeWorkingBar","Exiting","...................>>..");
		return readBar;
	}
	// -------------------Bares----->delOneBar------------------------------------------------
	@Transactional
	public void delOneBar(Bares delBar) {
		consoleLog.logMsg(showLogMsg,"->ConfigService->delOneBar","Entering","..>>...................");
		// Check if the field exist 
		baresRepository.deleteById(delBar.getBarId());
		consoleLog.logMsg(showLogMsg,"->ConfigService->delOneBar","Exiting","...................>>..");
	}
	// -------------------Bares----->delAllBares------------------------------------------------
	@Transactional
	public void delAllBares() {
		consoleLog.logMsg(showLogMsg,"->ConfigService->delAllBares","Entering","..>>...................");
		baresRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->ConfigService->delAllBares","Exiting","...................>>..");
	}
}
