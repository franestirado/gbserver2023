package com.fran.gestb.config.model;


import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


@Entity
@Table(name = "bares")
public class Bares {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "barid", updatable = false, nullable = false)
	private Long barId;
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date barDate;
	@NotNull
	@Size(max = 64)
	private String barName;
	@NotNull
	@Size(max = 64)
	private String barLongName;
	@NotNull
	@Size(max = 64)
	private String barLegalName;
	@NotNull
	@Size(max = 64)
	private String barCIF;	
	@NotNull
	@Size(max = 64)
	private String barAddress;
	@NotNull
	@Size(max = 64)
	private String barEmail;
	@NotNull
	@Size(max = 64)
	private String barWebSite;
	@NotNull
	@Size(max = 64)
	private String barPhone;
	@NotNull
	@Size(max = 64)
	private String barDbName;
	@NotNull
	@Size(max = 64)
	private String barDbUrl;
	@NotNull
	@Size(max = 64)
	private String barDbPassword;
	@NotNull
	@Size(max = 64)
	private String barDbUserName;
	@NotNull
	@Size(max = 128)
	private String barDbDriverClassName;
	@NotNull
	@Size(max = 128)
	private String barDbDdlAuto;
	@NotNull
	@Size(max = 128)
	private String barDbPhysNameStrategy;
	@Column(nullable=false)
	private boolean barStatus;
	// Checking attributes
	public boolean checkBarAtributes() {
		if (this.barDate == null) return false;
		if (this.barDbName == null) return false;
		if ( (this.barDbName.length() < 3) | (this.barDbName.length() >64) )return false;
		if ( (this.barDbUrl.length() < 3) | (this.barDbUrl.length() >64) )return false;
		if ( (this.barDbPassword.length() < 3) | (this.barDbPassword.length() >64) )return false;
		if ( (this.barDbUserName.length() < 3) | (this.barDbUserName.length() >64) )return false;
		if ( (this.barDbDriverClassName.length() < 3) | (this.barDbDriverClassName.length() >128) )return false;
		if ( (this.barDbDdlAuto.length() < 3) | (this.barDbDdlAuto.length() >128) )return false;
		if ( (this.barDbPhysNameStrategy.length() < 3) | (this.barDbPhysNameStrategy.length() >128) )return false;
		return true;
	}
	// constructors
	public Bares() {}
	public Bares(Long barId,Date barDate,String barName,String barLongName, String barLegalName,String barCIF, String barAddress,
			 	String barEmail,String barWebSite,String barPhone,String barDbName,
			 	String barDbUrl,String barDbPassword,String barDbUserName,String barDbDriverClassName,String barDbDdlAuto,String barDbPhysNameStrategy,
			 	boolean barStatus) {
		super();
		this.barId = barId;
		this.barDate = barDate;
		this.barName = barName;
		this.barLongName = barLongName;
		this.barLegalName = barLegalName;
		this.barCIF = barCIF;
		this.barAddress = barAddress;
		this.barEmail = barEmail;
		this.barWebSite = barWebSite;
		this.barPhone = barPhone;
		this.barDbName = barDbName;
		this.barDbUrl = barDbUrl;
		this.barDbPassword = barDbPassword;
		this.barDbUserName = barDbUserName;
		this.barDbDriverClassName = barDbDriverClassName;
		this.barDbDdlAuto = barDbDdlAuto;
		this.barDbPhysNameStrategy = barDbPhysNameStrategy;
		this.barStatus = barStatus;
	}	
	//setters and getters
	public Long getBarId() {
		return barId;
	}
	public void setBarId(Long barId) {
		this.barId = barId;
	}
	public Date getBarDate() {
		return barDate;
	}
	public void setBarDate(Date barDate) {
		this.barDate = barDate;
	}
	public String getBarName() {
		return barName;
	}
	public void setBarName(String barName) {
		this.barName = barName;
	}
	public String getBarLongName() {
		return barLongName;
	}
	public void setBarLongName(String barLongName) {
		this.barLongName = barLongName;
	}
	public String getBarLegalName() {
		return barLegalName;
	}
	public void setBarLegalName(String barLegalName) {
		this.barLegalName = barLegalName;
	}
	public String getBarCIF() {
		return barCIF;
	}
	public void setBarCIF(String barCIF) {
		this.barCIF = barCIF;
	}
	public String getBarAddress() {
		return barAddress;
	}
	public void setBarAddress(String barAddress) {
		this.barAddress = barAddress;
	}
	public String getBarEmail() {
		return barEmail;
	}
	public void setBarEmail(String barEmail) {
		this.barEmail = barEmail;
	}
	public String getBarWebSite() {
		return barWebSite;
	}
	public void setBarWebSite(String barWebSite) {
		this.barWebSite = barWebSite;
	}
	public String getBarPhone() {
		return barPhone;
	}
	public void setBarPhone(String barPhone) {
		this.barPhone = barPhone;
	}
	public String getBarDbName() {
		return barDbName;
	}
	public void setBarDbName(String barDbName) {
		this.barDbName = barDbName;
	}
	public String getBarDbUrl() {
		return barDbUrl;
	}
	public void setBarDbUrl(String barDbUrl) {
		this.barDbUrl = barDbUrl;
	}
	public String getBarDbPassword() {
		return barDbPassword;
	}
	public void setBarDbPassword(String barDbPassword) {
		this.barDbPassword = barDbPassword;
	}
	public String getBarDbUserName() {
		return barDbUserName;
	}
	public void setBarDbUserName(String barDbUserName) {
		this.barDbUserName = barDbUserName;
	}
	public String getBarDbDriverClassName() {
		return barDbDriverClassName;
	}
	public void setBarDbDriverClassName(String barDbDriverClassName) {
		this.barDbDriverClassName = barDbDriverClassName;
	}
	public String getBarDbDdlAuto() {
		return barDbDdlAuto;
	}
	public void setBarDbDdlAuto(String barDbDdlAuto) {
		this.barDbDdlAuto = barDbDdlAuto;
	}
	public String getBarDbPhysNameStrategy() {
		return barDbPhysNameStrategy;
	}
	public void setBarDbPhysNameStrategy(String barDbPhysNameStrategy) {
		this.barDbPhysNameStrategy = barDbPhysNameStrategy;
	}
	public boolean isBarStatus() {
		return barStatus;
	}
	public void setBarStatus(boolean barStatus) {
		this.barStatus = barStatus;
	}	
}
