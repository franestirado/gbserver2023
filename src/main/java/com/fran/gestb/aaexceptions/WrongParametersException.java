package com.fran.gestb.aaexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class WrongParametersException extends RuntimeException {
	public WrongParametersException(String exception) {
		super(exception);
	}
}