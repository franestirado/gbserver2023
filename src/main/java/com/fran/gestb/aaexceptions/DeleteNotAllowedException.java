package com.fran.gestb.aaexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
public class DeleteNotAllowedException extends RuntimeException {
	public DeleteNotAllowedException(String exception) {
		super(exception);
	}
}