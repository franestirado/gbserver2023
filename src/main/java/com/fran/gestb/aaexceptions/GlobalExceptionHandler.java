package com.fran.gestb.aaexceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	  @ExceptionHandler(Exception.class)
	  public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
		  ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),request.getDescription(false));
		  System.out.printf("Unexpected Exception --Ex1-->"+ ex.getMessage()+"...\n");
		  System.out.printf("Unexpected Exception --Ex1-->"+ ex.getCause()+"...\n");
		  System.out.printf("Unexpected Exception --Ex1-->"+ request.getDescription(false) +"...\n");
	    return new ResponseEntity<ErrorDetails>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	  }
	  
	  @ExceptionHandler(EmptyFieldException.class)
	  public final ResponseEntity<ExceptionResponse> handleEmptyFieldException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.BAD_REQUEST.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf(" Not All Mandatory Data Received --Ex2--> "+ ex.getMessage()+"..\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.BAD_REQUEST);
	  }	  
	  
	  @ExceptionHandler(EntityAlreadyExistsException.class)
	  public final ResponseEntity<ExceptionResponse> handleEntityAlreadyExistsException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.CONFLICT.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("Entity Already Exists --Ex3--> "+ ex.getMessage()+"..\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.CONFLICT);
	  }	  
 
	  @ExceptionHandler(UserAlreadyExistsException.class)
	  public final ResponseEntity<ExceptionResponse> handleUserAlreadyExistsException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.CONFLICT.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("User Already Exists --Ex4-->"+ ex.getMessage()+"..\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.CONFLICT);
	  }
	  
	  @ExceptionHandler(EntityNotFoundException.class)
	  public final ResponseEntity<ExceptionResponse> handleEntityNotFoundException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.NOT_FOUND.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("Entity NOT FOUND --Ex5-->"+ ex.getMessage()+"...\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.NOT_FOUND);
	  }	 
	  
	  @ExceptionHandler(UserDoesNotExistException.class)
	  public final ResponseEntity<ExceptionResponse> handleUserDoesNotExistException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.NOT_FOUND.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("User DOES NOT Exist--Ex6-->"+ ex.getMessage()+"...\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.NOT_FOUND);
	  }	 
	  
	  @ExceptionHandler(WrongParametersException.class)
	  public final ResponseEntity<ExceptionResponse> handleWrongParametersException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("Invalid Parameters Recevied--Ex7-->"+ ex.getMessage()+"...\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.UNPROCESSABLE_ENTITY);
	  }	 
	  
	  @ExceptionHandler(ParseException.class)
	  public final ResponseEntity<ExceptionResponse> handleParseException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("Invalid Date Parameters Recevied--Ex8-->"+ ex.getMessage()+"...\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.UNPROCESSABLE_ENTITY);
	  }	 

	  @ExceptionHandler(SQLException.class)
	  public final ResponseEntity<ExceptionResponse> handleSQLException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.BAD_REQUEST.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("SQL exception--Ex9-->"+ ex.getMessage()+"...\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.BAD_REQUEST);
	  }	
	  
	  @ExceptionHandler(DeleteNotAllowedException.class)
	  public final ResponseEntity<ExceptionResponse> handleDeleteNotAllowedException(Exception ex, WebRequest request) {
		  ExceptionResponse eR = new ExceptionResponse();
		  eR.setCode(HttpStatus.FAILED_DEPENDENCY.value());
		  eR.setMessage(ex.getMessage());
		  System.out.printf("Delete NOT Allowed --Ex10--> "+ ex.getMessage()+"..\n");
	    return new ResponseEntity<ExceptionResponse>(eR, HttpStatus.FAILED_DEPENDENCY);
	  }	
	  
}

