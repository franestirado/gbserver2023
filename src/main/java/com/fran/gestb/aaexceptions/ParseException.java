package com.fran.gestb.aaexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class ParseException extends RuntimeException {
	public ParseException(String exception) {
		super(exception);
	}
}
