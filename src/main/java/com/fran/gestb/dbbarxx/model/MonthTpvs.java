package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "monthtpvs")
public class MonthTpvs {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "monthTpvid", updatable = false, nullable = false)
	private Long monthTpvId;
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date monthTpvDate;		
	@NotNull
	@Size(max = 64)
	private String monthTpvName;	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)	
	private String monthTpvType;
	@Column(nullable=false)
	private Long monthTpvTickets;
	@Column(nullable=false)
	private double monthTpvTicket;
	@Column(nullable=false)
	private double monthTpvMoney;
	// Checking attributes
	public boolean checkMonthTpvAtributes() {
		if (this.monthTpvName.length() == 0) return false;
		if ( (this.monthTpvName.length() < 3) | (this.monthTpvName.length() >64) )return false;
		if (this.monthTpvType.length() == 0) return false;
		if ( (this.monthTpvType.length() < 3) | (this.monthTpvType.length() >64) )return false;
		return true;
	}	
	// constructors
	public MonthTpvs() {}
	public MonthTpvs(Long monthTpvId,Date monthTpvDate, String monthTpvName, String monthTpvType, 
					Long monthTpvTickets, double monthTpvTicket, double monthTpvMoney) {
		super();
		this.monthTpvId = monthTpvId;
		this.monthTpvDate = monthTpvDate;
		this.monthTpvName = monthTpvName;
		this.monthTpvType = monthTpvType;
		this.monthTpvTickets = monthTpvTickets;
		this.monthTpvTicket = monthTpvTicket;
		this.monthTpvMoney = monthTpvMoney;
	}
	//setters and getters
	public Long getMonthTpvId() {
		return monthTpvId;
	}
	public void setMonthTpvId(Long monthTpvId) {
		this.monthTpvId = monthTpvId;
	}
	public Date getMonthTpvDate() {
		return monthTpvDate;
	}
	public void setMonthTpvDate(Date monthTpvDate) {
		this.monthTpvDate = monthTpvDate;
	}
	public String getMonthTpvName() {
		return monthTpvName;
	}
	public void setMonthTpvName(String monthTpvName) {
		this.monthTpvName = monthTpvName;
	}
	public String getMonthTpvType() {
		return monthTpvType;
	}
	public void setMonthTpvType(String monthTpvType) {
		this.monthTpvType = monthTpvType;
	}
	public Long getMonthTpvTickets() {
		return monthTpvTickets;
	}
	public void setMonthTpvTickets(Long monthTpvTickets) {
		this.monthTpvTickets = monthTpvTickets;
	}
	public double getMonthTpvTicket() {
		return monthTpvTicket;
	}
	public void setMonthTpvTicket(double monthTpvTicket) {
		this.monthTpvTicket = monthTpvTicket;
	}
	public double getMonthTpvMoney() {
		return monthTpvMoney;
	}
	public void setMonthTpvMoney(double monthTpvMoney) {
		this.monthTpvMoney = monthTpvMoney;
	}
	
}
