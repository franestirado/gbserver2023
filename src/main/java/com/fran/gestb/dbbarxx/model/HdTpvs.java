package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "hdtpvs")
public class HdTpvs {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "hdtpvid", updatable = false, nullable = false)
	private Long hdTpvId;
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date hdTpvDate;		
	@NotNull
	@Size(max = 64)
	private String hdTpvName;	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)	
	private String hdTpvType;
	@Column(nullable=false)
	private int hdTpvOrder;
	@Column(nullable=false)
	private Long hdTpvNumber;
	@Column(nullable=false)
	private Long hdTpvTickets;
	@Column(nullable=false)
	private double hdTpvTicket;
	@Column(nullable=false)
	private double hdTpvMoney;
	// Checking attributes
	public boolean checkHdTpvAtributes() {
		if (this.hdTpvName.length() == 0) return false;
		if ( (this.hdTpvName.length() < 3) | (this.hdTpvName.length() >64) )return false;
		if (this.hdTpvType.length() == 0) return false;
		if ( (this.hdTpvType.length() < 3) | (this.hdTpvType.length() >64) )return false;
		return true;
	}
	// constructors
	public HdTpvs() {		
	}
	public HdTpvs(Date hdTpvDate, String hdTpvName, String hdTpvType, int hdTpvNumber,
			int hdTpvOrder, int hdTpvTickets, double hdTpvTicket,	double hdTpvMoney) {		
		this.hdTpvDate = hdTpvDate;
		this.hdTpvName = hdTpvName;
		this.hdTpvType = hdTpvType;
		this.hdTpvOrder = hdTpvOrder;
		this.hdTpvNumber = (long) 0;
		this.hdTpvTickets =(long) 0;
		this.hdTpvTicket = hdTpvTicket;
		this.hdTpvMoney = hdTpvMoney;
	}
	public HdTpvs(Long hdTpvId, @NotNull Date hdTpvDate, @NotNull @Size(max = 64) String hdTpvName,
			@NotNull @Size(max = 64) String hdTpvType, Long hdTpvNumber, int hdTpvOrder, Long hdTpvTickets, double hdTpvTicket,
			double hdTpvMoney) {
		super();
		this.hdTpvId = hdTpvId;
		this.hdTpvDate = hdTpvDate;
		this.hdTpvName = hdTpvName;
		this.hdTpvType = hdTpvType;
		this.hdTpvOrder = hdTpvOrder;
		this.hdTpvNumber = hdTpvNumber;
		this.hdTpvTickets = hdTpvTickets;
		this.hdTpvTicket = hdTpvTicket;
		this.hdTpvMoney = hdTpvMoney;
	}
	//setters and getters
	public Long getHdTpvId() {
		return hdTpvId;
	}
	public void setHdTpvId(Long hdTpvId) {
		this.hdTpvId = hdTpvId;
	}
	public Date getHdTpvDate() {
		return hdTpvDate;
	}
	public void setHdTpvDate(Date hdTpvDate) {
		this.hdTpvDate = hdTpvDate;
	}
	public String getHdTpvName() {
		return hdTpvName;
	}
	public void setHdTpvName(String hdTpvName) {
		this.hdTpvName = hdTpvName;
	}
	public String getHdTpvType() {
		return hdTpvType;
	}
	public void setHdTpvType(String hdTpvType) {
		this.hdTpvType = hdTpvType;
	}
	public int getHdTpvOrder() {
		return hdTpvOrder;
	}
	public void setHdTpvOrder(int hdTpvOrder) {
		this.hdTpvOrder = hdTpvOrder;
	}
	public Long getHdTpvNumber() {
		return hdTpvNumber;
	}
	public void setHdTpvNumber(Long hdTpvNumber) {
		this.hdTpvNumber = hdTpvNumber;
	}
	public Long getHdTpvTickets() {
		return hdTpvTickets;
	}
	public void setHdTpvTickets(Long hdTpvTickets) {
		this.hdTpvTickets = hdTpvTickets;
	}
	public double getHdTpvTicket() {
		return hdTpvTicket;
	}
	public void setHdTpvTicket(double hdTpvTicket) {
		this.hdTpvTicket = hdTpvTicket;
	}
	public double getHdTpvMoney() {
		return hdTpvMoney;
	}
	public void setHdTpvMoney(double hdTpvMoney) {
		this.hdTpvMoney = hdTpvMoney;
	}
	
	
}
