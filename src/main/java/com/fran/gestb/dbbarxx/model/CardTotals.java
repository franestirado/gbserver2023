package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


@Entity
@Table(name = "cardTotals")
public class CardTotals {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "cardTotalId", updatable = false, nullable = false)
	private Long cardTotalId;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date cardTotalDate;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date cardTotalDateStart;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date cardTotalDateEnd;
	@NotNull
	@Column(nullable=false)
	private Long cardTotalCardTpvId;
	@NotNull
	@Size(max = 32)
	@Column(nullable=false)
	private String cardTotalCardTpvName;	
	@NotNull
	@Size(max = 32)
	@Column(nullable=false)
	private String cardTotalCardTpvBank;
	@Column(nullable=false)
	private double cardTotalTotalOper;
	@Column(nullable=false)
	private double cardTotalTotalMoney;
	@Column(nullable=false)
	private double cardTotalExp1Perc;
	@Column(nullable=false)
	private double cardTotalExp1Money;
	@Column(nullable=false)
	private double cardTotalExp2Perc;
	@Column(nullable=false)
	private double cardTotalExp2Money;
	@Column(nullable=false)
	private double cardTotalExp3Perc;
	@Column(nullable=false)
	private double cardTotalExp3Money;
	@Column(nullable=false)
	private double cardTotalExp4Perc;
	@Column(nullable=false)
	private double cardTotalExp4Money;
	@Column(nullable=false)
	private double cardTotalExpTotal;	
	@Column(nullable=false)
	private double cardTotalExpTaxesPerc;	
	@Column(nullable=false)
	private double cardTotalExpTaxesMoney;	
	@Column(nullable=false)
	private double cardTotalBankMoney;
	@Column(nullable=true)
	private long cardTotalFactId;
	@Column(nullable=false)
	private boolean cardTotalStatus;
	// Checking attributes
	public boolean checkCardTotalAtributes() {	
		if   (this.cardTotalDate == null) return false;
		if   (this.cardTotalDateStart == null) return false;
		if   (this.cardTotalDateEnd == null) return false;
		if   (this.cardTotalCardTpvId == null)return false;
		if   (this.cardTotalCardTpvName == null) return false;
		if   (this.cardTotalCardTpvBank == null) return false;
		if ( (this.cardTotalCardTpvName.length() < 1) 		| (this.cardTotalCardTpvName.length() 	> 32) )return false;		
		if ( (this.cardTotalCardTpvBank.length() < 1) 		| (this.cardTotalCardTpvBank.length() 	> 32) )return false;		
		return true;
	}
	// constructors
	public CardTotals() {}	
	public CardTotals(Long cardTotalId, @NotNull Date cardTotalDate, @NotNull Date cardTotalDateStart,
			@NotNull Date cardTotalDateEnd, @NotNull Long cardTotalCardTpvId,
			@NotNull @Size(max = 32) String cardTotalCardTpvName, @NotNull @Size(max = 32) String cardTotalCardTpvBank,
			double cardTotalTotalOper, double cardTotalTotalMoney, double cardTotalExp1Perc, double cardTotalExp1Money, double cardTotalExp2Perc,
			double cardTotalExp2Money, double cardTotalExp3Perc, double cardTotalExp3Money, double cardTotalExp4Perc,
			double cardTotalExp4Money, double cardTotalExpTotal, double cardTotalExpTaxesPerc,
			double cardTotalExpTaxesMoney, double cardTotalBankMoney, long cardTotalFactId, boolean cardTotalStatus) {
		super();
		this.cardTotalId = cardTotalId;
		this.cardTotalDate = cardTotalDate;
		this.cardTotalDateStart = cardTotalDateStart;
		this.cardTotalDateEnd = cardTotalDateEnd;
		this.cardTotalCardTpvId = cardTotalCardTpvId;
		this.cardTotalCardTpvName = cardTotalCardTpvName;
		this.cardTotalCardTpvBank = cardTotalCardTpvBank;
		this.cardTotalTotalOper = cardTotalTotalOper;
		this.cardTotalTotalMoney = cardTotalTotalMoney;
		this.cardTotalExp1Perc = cardTotalExp1Perc;
		this.cardTotalExp1Money = cardTotalExp1Money;
		this.cardTotalExp2Perc = cardTotalExp2Perc;
		this.cardTotalExp2Money = cardTotalExp2Money;
		this.cardTotalExp3Perc = cardTotalExp3Perc;
		this.cardTotalExp3Money = cardTotalExp3Money;
		this.cardTotalExp4Perc = cardTotalExp4Perc;
		this.cardTotalExp4Money = cardTotalExp4Money;
		this.cardTotalExpTotal = cardTotalExpTotal;
		this.cardTotalExpTaxesPerc = cardTotalExpTaxesPerc;
		this.cardTotalExpTaxesMoney = cardTotalExpTaxesMoney;
		this.cardTotalBankMoney = cardTotalBankMoney;
		this.cardTotalFactId = cardTotalFactId;
		this.cardTotalStatus = cardTotalStatus;
	}
	//setters and getters
	public Long getCardTotalId() {
		return cardTotalId;
	}
	public void setCardTotalId(Long cardTotalId) {
		this.cardTotalId = cardTotalId;
	}
	public Date getCardTotalDate() {
		return cardTotalDate;
	}
	public void setCardTotalDate(Date cardTotalDate) {
		this.cardTotalDate = cardTotalDate;
	}
	public Date getCardTotalDateStart() {
		return cardTotalDateStart;
	}
	public void setCardTotalDateStart(Date cardTotalDateStart) {
		this.cardTotalDateStart = cardTotalDateStart;
	}
	public Date getCardTotalDateEnd() {
		return cardTotalDateEnd;
	}
	public void setCardTotalDateEnd(Date cardTotalDateEnd) {
		this.cardTotalDateEnd = cardTotalDateEnd;
	}
	public Long getCardTotalCardTpvId() {
		return cardTotalCardTpvId;
	}
	public void setCardTotalCardTpvId(Long cardTotalCardTpvId) {
		this.cardTotalCardTpvId = cardTotalCardTpvId;
	}
	public String getCardTotalCardTpvName() {
		return cardTotalCardTpvName;
	}
	public void setCardTotalCardTpvName(String cardTotalCardTpvName) {
		this.cardTotalCardTpvName = cardTotalCardTpvName;
	}
	public String getCardTotalCardTpvBank() {
		return cardTotalCardTpvBank;
	}
	public void setCardTotalCardTpvBank(String cardTotalCardTpvBank) {
		this.cardTotalCardTpvBank = cardTotalCardTpvBank;
	}
	public double getCardTotalTotalOper() {
		return cardTotalTotalOper;
	}
	public void setCardTotalTotalOper(double cardTotalTotalOper) {
		this.cardTotalTotalOper = cardTotalTotalOper;
	}
	public double getCardTotalTotalMoney() {
		return cardTotalTotalMoney;
	}
	public void setCardTotalTotalMoney(double cardTotalTotalMoney) {
		this.cardTotalTotalMoney = cardTotalTotalMoney;
	}
	public double getCardTotalExp1Perc() {
		return cardTotalExp1Perc;
	}
	public void setCardTotalExp1Perc(double cardTotalExp1Perc) {
		this.cardTotalExp1Perc = cardTotalExp1Perc;
	}
	public double getCardTotalExp1Money() {
		return cardTotalExp1Money;
	}
	public void setCardTotalExp1Money(double cardTotalExp1Money) {
		this.cardTotalExp1Money = cardTotalExp1Money;
	}
	public double getCardTotalExp2Perc() {
		return cardTotalExp2Perc;
	}
	public void setCardTotalExp2Perc(double cardTotalExp2Perc) {
		this.cardTotalExp2Perc = cardTotalExp2Perc;
	}
	public double getCardTotalExp2Money() {
		return cardTotalExp2Money;
	}
	public void setCardTotalExp2Money(double cardTotalExp2Money) {
		this.cardTotalExp2Money = cardTotalExp2Money;
	}
	public double getCardTotalExp3Perc() {
		return cardTotalExp3Perc;
	}
	public void setCardTotalExp3Perc(double cardTotalExp3Perc) {
		this.cardTotalExp3Perc = cardTotalExp3Perc;
	}
	public double getCardTotalExp3Money() {
		return cardTotalExp3Money;
	}
	public void setCardTotalExp3Money(double cardTotalExp3Money) {
		this.cardTotalExp3Money = cardTotalExp3Money;
	}
	public double getCardTotalExp4Perc() {
		return cardTotalExp4Perc;
	}
	public void setCardTotalExp4Perc(double cardTotalExp4Perc) {
		this.cardTotalExp4Perc = cardTotalExp4Perc;
	}
	public double getCardTotalExp4Money() {
		return cardTotalExp4Money;
	}
	public void setCardTotalExp4Money(double cardTotalExp4Money) {
		this.cardTotalExp4Money = cardTotalExp4Money;
	}
	public double getCardTotalExpTotal() {
		return cardTotalExpTotal;
	}
	public void setCardTotalExpTotal(double cardTotalExpTotal) {
		this.cardTotalExpTotal = cardTotalExpTotal;
	}
	public double getCardTotalExpTaxesPerc() {
		return cardTotalExpTaxesPerc;
	}
	public void setCardTotalExpTaxesPerc(double cardTotalExpTaxesPerc) {
		this.cardTotalExpTaxesPerc = cardTotalExpTaxesPerc;
	}
	public double getCardTotalExpTaxesMoney() {
		return cardTotalExpTaxesMoney;
	}
	public void setCardTotalExpTaxesMoney(double cardTotalExpTaxesMoney) {
		this.cardTotalExpTaxesMoney = cardTotalExpTaxesMoney;
	}
	public double getCardTotalBankMoney() {
		return cardTotalBankMoney;
	}
	public void setCardTotalBankMoney(double cardTotalBankMoney) {
		this.cardTotalBankMoney = cardTotalBankMoney;
	}
	public Long getCardTotalFactId() {
		return cardTotalFactId;
	}
	public void setCardTotalFactId(Long cardTotalFactId) {
		this.cardTotalFactId = cardTotalFactId;
	}	
	public boolean isCardTotalStatus() {
		return cardTotalStatus;
	}
	public void setCardTotalStatus(boolean cardTotalStatus) {
		this.cardTotalStatus = cardTotalStatus;
	}	

}