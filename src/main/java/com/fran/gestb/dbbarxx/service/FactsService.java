package com.fran.gestb.dbbarxx.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.UserDoesNotExistException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarc.model.FactModels;
import com.fran.gestb.dbbarc.repo.ArtsRepository;
import com.fran.gestb.dbbarc.repo.FactModelsRepository;
import com.fran.gestb.dbbarxx.model.ArtFacts;
import com.fran.gestb.dbbarxx.model.Facts;
import com.fran.gestb.dbbarxx.repo.ArtFactsRepository;
import com.fran.gestb.dbbarxx.repo.FactsRepository;

@Service
public class FactsService {
	@Autowired
	private FactsRepository factsRepository;
	@Autowired
	private FactModelsRepository modelFactsRepository;
	@Autowired

	private ArtsRepository artsRepository;
	@Autowired
	private ArtFactsRepository artsFactRepository;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	// -------------------Facts----->findAllFacts-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Facts> findAllFacts() {
		consoleLog.logMsg(showLogMsg,"->FactsService->findAllFacts","Entering","..>>...................");
		List<Facts> factsList = factsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->FactsService->findAllFacts","Exiting","...................>>..");
		return factsList;
	}
	// -------------------Facts----->getAllFactsMonth-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Facts> getAllFactsMonth(List <Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsMonth","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->HdsService->getAllFactsMonth->Null Object or Empty Mandatoy Parameters");	
		List<Facts> factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqual(firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsMonth","Exiting","...................>>..");
		return factsList;
	}
	// -------------------Facts----->getAllFactsTypeMonth-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Facts> getAllFactsTypeMonth(List<ValueRecord> factsTypeVRList) {
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsTypeMonth","Entering","..>>...................");

		/* Checking null new object or empty mandatory attributes  */
		if ( (factsTypeVRList == null) | (factsTypeVRList.size() == 0) ) 
			throw new EmptyFieldException("->FactsService->getAllFactsTypeMonth.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<factsTypeVRList.size() ; j++) {
			if ( (factsTypeVRList.get(j) == null) | (factsTypeVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->FactsService->getAllFactsTypeMonth.2.->Null Object or Empty Mandatoy Parameters");
			//consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsTypeMonth","....",factsTypeVRList.get(j).getValue());
		}		
		// check if received factPayType exists 
		/*
		CardTpvs readCardTpv = null;
		if ( factsTypeVRList.get(0).getValue().equals("ALL") == false ) {
			readFactType = cardTpvsRepository.findByCardTpvName(factsTypeVRList.get(0).getValue());
			if (readCardTpv == null)
				throw new EntityNotFoundException("->FactsService->getAllFactsTypeMonth.1.->Not Found"+cardPayoutVRList.get(0).getValue());
		}  */
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		//formatter.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));	 
		//String dateInString = "22-01-2015 10:15:55 AM"; 
		Date startDate,endDate;
		try {
			endDate = formatter.parse(factsTypeVRList.get(2).getValue());
			//consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsTypeMonth","..endDate........",endDate.toString());
			startDate = formatter.parse(factsTypeVRList.get(1).getValue());
			//consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsTypeMonth","..startDate..",startDate.toString());

		} catch (ParseException e) {
			throw new EmptyFieldException("->FactsService->getAllFactsTypeMonth.1.->Wrong DATE Object ");
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		//String formattedDateString = formatter.format(date);
		List<Facts> factsList = null;
		if ( factsTypeVRList.get(0).getValue().equals("ALL") == true )  {
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqual(startDate,endDate);
		} else {
			factsList = factsRepository.findByFactPayTypeAndFactDateGreaterThanEqualAndFactDateLessThanEqual(factsTypeVRList.get(0).getValue(),startDate,endDate);
		}		
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsTypeMonth","Exiting","...................>>..");
		return factsList;
	}		
	// -------------------Facts----->getAllFactsFilter-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Facts> getAllFactsFilter(List<ValueRecord> factsFilterVRList) {
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsFilter","Entering","..>>...................");	
		/* Checking null new object or empty mandatory attributes  */
		if ( (factsFilterVRList == null) | (factsFilterVRList.size() == 0) | (factsFilterVRList.size() < 6) ) 
			throw new EmptyFieldException("->FactsService->getAllFactsFilter.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<factsFilterVRList.size() ; j++) {
			if ( factsFilterVRList.get(j) == null) 	throw new EmptyFieldException("->FactsService->getAllFactsFilter.2.->Null Object or Empty Mandatoy Parameters");
			//consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsFilter","....",factsFilterVRList.get(j).getValue());
		}		
		// 0-> startDate, 1-> endDate, 2->Prov, 3->FactType, 4->PayType, 5->FactOrNote
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate,endDate;
		try { 	endDate = formatter.parse(factsFilterVRList.get(1).getValue());
				startDate = formatter.parse(factsFilterVRList.get(0).getValue());
		} catch (ParseException e) {throw new EmptyFieldException("->FactsService->getAllFactsFilter.1.->Wrong DATE Object ");}

		//consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsFilter","..startDate..",startDate.toString());
		//consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsFilter","..endDate........",endDate.toString());
		List<Facts> factsList = null;
		// only dates, others empty ---------------------------------------------------------------------------
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()=="") )
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqual(startDate,endDate);
		// dates, and just one of the other conditions
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvName(startDate,endDate,provName);
		}
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String factType = factsFilterVRList.get(3).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactType(startDate,endDate,factType);
		}
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String factPayType = factsFilterVRList.get(4).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactPayType(startDate,endDate,factPayType);
		}
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()!="") ) {
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactNote(startDate,endDate,factNote);
		}
		// dates and 2 of the other conditions---------------------------------------------------------------------------
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			String factType = factsFilterVRList.get(3).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactType(startDate,endDate,provName,factType);
		}
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			String factPayType = factsFilterVRList.get(4).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactPayType(startDate,endDate,provName,factPayType);
		}
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()!="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactNote(startDate,endDate,provName,factNote);
		}
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String factType = factsFilterVRList.get(3).getValue();
			String factPayType = factsFilterVRList.get(4).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactTypeAndFactPayType(startDate,endDate,factType,factPayType);
		}
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()!="") ) {
			String factType = factsFilterVRList.get(3).getValue();
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactTypeAndFactNote(startDate,endDate,factType,factNote);
		}
		// dates and 3 of the other conditions---------------------------------------------------------------------------
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()=="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			String factType = factsFilterVRList.get(3).getValue();
			String factPayType = factsFilterVRList.get(4).getValue();
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactTypeAndFactPayType(startDate,endDate,provName,factType,factPayType);
		}
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()=="") && (factsFilterVRList.get(5).getValue()!="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			String factType = factsFilterVRList.get(3).getValue();
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactTypeAndFactNote(startDate,endDate,provName,factType,factNote);
		}
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()=="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()!="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			String factPayType = factsFilterVRList.get(4).getValue();
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactPayTypeAndFactNote(startDate,endDate,provName,factPayType,factNote);
		}
		if ( (factsFilterVRList.get(2).getValue()=="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()!="") ) {
			String factType = factsFilterVRList.get(3).getValue();
			String factPayType = factsFilterVRList.get(4).getValue();
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactTypeAndFactPayTypeAndFactNote(startDate,endDate,factType,factPayType,factNote);
		}		
		// dates and the other 4 conditions---------------------------------------------------------------------------
		if ( (factsFilterVRList.get(2).getValue()!="") && (factsFilterVRList.get(3).getValue()!="") &&
				(factsFilterVRList.get(4).getValue()!="") && (factsFilterVRList.get(5).getValue()!="") ) {
			String provName = factsFilterVRList.get(2).getValue();
			String factType = factsFilterVRList.get(3).getValue();
			String factPayType = factsFilterVRList.get(4).getValue();
			boolean factNote = true;
			if ( factsFilterVRList.get(5).getValue().equals("Nota") ) factNote = false;
			factsList = factsRepository.findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactTypeAndFactPayTypeAndFactNote(startDate,endDate,provName,factType,factPayType,factNote);
		}
		
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsFilter","Exiting","...................>>..");
		return factsList;
	}
	// -------------------Facts----->saveFact------------------------------------------------
	@Transactional
	public Facts saveFact(Facts newFact) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->FactsService->saveFact","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newFact==null) | (newFact.getFactNumber()=="")) throw new EmptyFieldException("->FactsService->saveFact->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newFact.checkFactAtributes()==false) throw new 
				WrongParametersException("--Prov Name-->"+newFact.getFactProvName()+"--Fact Number-->"+newFact.getFactNumber());
		/* Checking there no other object with same name */				
		Facts readFact = factsRepository.save(newFact);
		consoleLog.logMsg(showLogMsg,"->FactsService->saveFact","Exiting","...................>>..");
		return readFact;
	}
	// -------------------Facts----->getOneFact------------------------------------------------
	@Transactional
	public Facts getOneFact(Facts modFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->getOneFact","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modFact==null) | (modFact.getFactNumber()=="")) throw new EmptyFieldException("->FactsService->getOneFact->Null Object or Empty Mandatoy Parameters");	
		Facts readFact = factsRepository.findByFactId(modFact.getFactId());
		if (readFact == null) throw new EntityNotFoundException("--Prov Name-->"+modFact.getFactProvName()+"--Fact Number-->"+modFact.getFactNumber());
		consoleLog.logMsg(showLogMsg,"->FactsService->getOneFact","Exiting","...................>>..");
		return readFact;
	}
	// -------------------Facts----->getOneFactById------------------------------------------------
	@Transactional
	public Facts getOneFactById(long factId) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->getOneFactById","Entering","..>>...................");
		Facts readFact = factsRepository.findByFactId(factId);
		// if (readFact == null) throw new EntityNotFoundException("--Fact with this id not found-->");
		consoleLog.logMsg(showLogMsg,"->FactsService->getOneFactById","Exiting","...................>>..");
		return readFact;
	}
	// -------------------Facts----->updateOneFact------------------------------------------------
	@Transactional
	public Facts updateOneFact(Facts modFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->updateOneFact","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modFact==null) | (modFact.getFactNumber()=="")) throw new EmptyFieldException("->FactsService->updateOneFact->Null Object or Empty Mandatoy Parameters");	
		Facts readFact = factsRepository.findByFactId(modFact.getFactId());
		if (readFact == null) throw new EntityNotFoundException("--Prov Name-->"+modFact.getFactProvName()+"--Fact Number-->"+modFact.getFactNumber());
		readFact = factsRepository.save(modFact);
		consoleLog.logMsg(showLogMsg,"->FactsService->updateOneFact","Exiting","...................>>..");
		return readFact;
	}
	// -------------------Facts----->DeleteOne------------------------------------------------
	@Transactional
	public void deleteOneFact(Facts delFact) {
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteOneFact","Entering","..>>...................");
		/* Check if the field exist */
		Facts checkFact = factsRepository.findByFactId(delFact.getFactId());
		if (checkFact == null) throw new UserDoesNotExistException("Field does NOT exists-->");
		artsFactRepository.deleteAllByArtFactFactId(delFact.getFactId());
		factsRepository.deleteById(delFact.getFactId());
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteOneFact","Exiting","...................>>..");
	}
	// -------------------Facts----->deleteOneFactById------------------------------------------------
	@Transactional
	public void deleteOneFactById(long delFactId) {
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteOneFactById","Entering","..>>...................");
		/* Check if the field exist */
		Facts checkFact = factsRepository.findByFactId(delFactId);
		if (checkFact == null) throw new UserDoesNotExistException("Fact iwth this Id does NOT exists-->");
		artsFactRepository.deleteAllByArtFactFactId(delFactId);
		factsRepository.deleteById(delFactId);
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteOneFactById","Exiting","...................>>..");
	}
	// -------------------Facts----->DeleteAll------------------------------------------------
	@Transactional
	public void deleteAllFacts() {
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFacts","Entering","..>>...................");
		artsFactRepository.deleteAll();
		factsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFacts","Exiting","...................>>..");
	}
	// -------------------Facts----->getAllFactsHd-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Facts> getAllFactsHd(Date workingDate) {
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsHd","Entering","..>>...................");
		List<Facts> factsList = factsRepository.findByFactDate(workingDate);
		consoleLog.logMsg(showLogMsg,"->FactsService->getAllFactsHd","Exiting","...................>>..");
		return factsList;
	}
	// -------------------Facts----->deleteAllFactsHd-------------------------------------------	
	@Transactional
	public List<Facts> deleteAllFactsDatePayType(Date workingDate, String factPayType) {
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFactsDatePayType","Entering","..>>...................");
		// get all facts working date and pay type hd
		List<Facts> factsList = factsRepository.findByFactDateAndFactPayType(workingDate, factPayType);
		// for each fact, delete all artfacts and the fact
		int delArts = 0;
		int delFacts = 0;
		for (int j=0; j<factsList.size(); j++) {
			//consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFactsDatePayType","...j...",Integer.toString(j));			
			//consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFactsDatePayType","...FactNumber...",factsList.get(j).getFactNumber());
			delArts = artsFactRepository.deleteAllByArtFactFactId(factsList.get(j).getFactId());
			delFacts = factsRepository.deleteByFactId(factsList.get(j).getFactId());
			//consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFactsDatePayType","...delArts...",Integer.toString(delArts));
			//consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFactsDatePayType","...delFacts...",Integer.toString(delFacts));
		}		
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllFactsDatePayType","Exiting","...................>>..");
		return null;
	}

	// -------------------Arts Facts------------------------------------------------
	// -------------------Arts Facts----->createArtsFact------------------------------------------------
	@Transactional
	public List<ArtFacts> createArtsFact(List<ArtFacts> newArtsFactList) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->createArtsFact","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if (newArtsFactList.size() == 0) { 
			throw new EmptyFieldException("->FactsService->createArtsFact-> Null Object or Empty Mandatoy Parameters");}	
		// Check if factId exist, for instance, getting first element of arts list 
		Facts readFact=factsRepository.findByFactId(newArtsFactList.get(0).getArtFactFactId());
		if ((readFact==null)) 
		  throw new EntityNotFoundException("->FactsService->createArtsFact-> Fact NOT Found"+newArtsFactList.get(0).getArtFactFactId().toString());
		List<ArtFacts> createdArtsFactList = new ArrayList<ArtFacts>();
		ArtFacts createdArtFact;
		for(int i = 0;i<newArtsFactList.size();i++)	{
			// Check Art exists in Arts entity
			if ( artsRepository.findByArtId(newArtsFactList.get(i).getArtFactArtId()) == null ) 
			  throw new EntityNotFoundException("->FactsService->createArtsFact-> ART NOT Found"+newArtsFactList.get(i).getArtFactArtId().toString());
			createdArtFact = artsFactRepository.save(newArtsFactList.get(i));			
			if (createdArtFact == null) {
				throw new EntityAlreadyExistsException("->FactsService->createArtsFact->Error Creating ArtFact");
			}
			createdArtsFactList.add(createdArtFact);			
		}
		consoleLog.logMsg(showLogMsg,"->FactsService->createArtsFact","Exiting","...................>>..");
		return createdArtsFactList;		
	}
	// -------------------Arts Facts----->getArtsFact------------------------------------------------
	@Transactional
	public List<ArtFacts> getArtsFact(Facts updateFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->getArtsFact","Entering","..>>...................");
		if ((updateFact==null)) 
			  throw new EntityNotFoundException("->FactsService->getArtsFact-> Null Object or Empty Mandatoy Parameters");
		// Check if factId exist, for instance, getting first element of arts list 
		Facts readFact=factsRepository.findByFactId(updateFact.getFactId());
		if ((readFact==null)) 
		  throw new EntityNotFoundException("->FactsService->getArtsFact-> Fact NOT Found"+updateFact.getFactNumber().toString());
		List<ArtFacts> readArtsFactList;
		readArtsFactList = artsFactRepository.findByArtFactFactId(readFact.getFactId());
		consoleLog.logMsg(showLogMsg,"->FactsService->getArtsFact","Exiting","...................>>..");
		return readArtsFactList;		
	}
	// -------------------Arts Facts----->getArtsFactByFactId------------------------------------------------
	@Transactional
	public List<ArtFacts> getArtsFactByFactId(long factId) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->getArtsFactByFactId","Entering","..>>...................");
		// Check if factId exist, for instance, getting first element of arts list 
		List<ArtFacts> readArtsFactList;
		readArtsFactList = artsFactRepository.findByArtFactFactId(factId);
		consoleLog.logMsg(showLogMsg,"->FactsService->getArtsFactByFactId","Exiting","...................>>..");
		return readArtsFactList;		
	}	
	
	// -------------------Arts Facts----->getArtFactsWithArt------------------------------------------------
	@Transactional
	public List<ArtFacts> getArtFactsWithArt(Arts selArt) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->getArtFactsWithArt","Entering","..>>...................");
		if ((selArt == null)) 
			  throw new EntityNotFoundException("->FactsService->getArtFactsWithArt-> Null Object or Empty Mandatoy Parameters");
		// Check if art exist, for instance,  
		Arts readArt = artsRepository.findByArtId(selArt.getArtId());
		if ((readArt == null)) 
		  throw new EntityNotFoundException("->FactsService->getArtFactsWithArt-> Fact NOT Found"+selArt.getArtName());
		List<ArtFacts> readArtsFactList;
		readArtsFactList = artsFactRepository.findAllByArtFactArtId(selArt.getArtId());
		consoleLog.logMsg(showLogMsg,"->FactsService->getArtFactsWithArt","Exiting","...................>>..");
		return readArtsFactList;		
	}
	
	// -------------------Arts Facts----->updateArtsFact------------------------------------------------
	@Transactional
	public List<ArtFacts> updateArtsFact(List<ArtFacts> updateArtsFactList) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->updateArtsFact","Entering","..>>...................");
		if ((updateArtsFactList.size() == 0)) 
			  throw new EntityNotFoundException("->FactsService->updateArtsFact-> Null Object or Empty Mandatoy Parameters");
		// Check if factId exist, for instance, getting first element of arts list 
		consoleLog.logMsg(showLogMsg,"->FactsService->updateArtsFact","...fact id...",updateArtsFactList.get(0).getArtFactFactId().toString());
		Facts readFact=factsRepository.findByFactId(updateArtsFactList.get(0).getArtFactFactId());				
		if (readFact == null) {
		  throw new EntityNotFoundException("->FactsService->updateArtsFact-> Fact NOT Found"+updateArtsFactList.get(0).getArtFactFactId().toString()); }	
		int deletedArtsFact = artsFactRepository.deleteAllByArtFactFactId(updateArtsFactList.get(0).getArtFactFactId());
		consoleLog.logMsg(showLogMsg,"->FactsService->updateArtsFact",".....deletedArtsFact.....",Integer.toString(deletedArtsFact));
		List<ArtFacts> readArtsFactList;
		readArtsFactList = createArtsFact(updateArtsFactList);
		consoleLog.logMsg(showLogMsg,"->FactsService->updateArtsFact","Exiting","...................>>..");
		return readArtsFactList;		
	}
	// -------------------Arts Facts----->deleteArtsFact------------------------------------------------
	@Transactional
	public int deleteArtsFact(List<ArtFacts> deleteArtsFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteArtsFact","Entering","..>>...................");
		if ((deleteArtsFact.size() == 0)) 
			  throw new EntityNotFoundException("->FactsService->deleteArtsFact-> Null Object or Empty Mandatoy Parameters");
		int deletedArtsFact = artsFactRepository.deleteAllByArtFactFactId(deleteArtsFact.get(0).getArtFactFactId());
		consoleLog.logMsg(showLogMsg,"->FactsService->updateArtsFact",".....deletedArtsFact.....",Integer.toString(deletedArtsFact));
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteArtsFact","Exiting","...................>>..");
		return deletedArtsFact;		
	}
	// -------------------Arts Facts----->deleteAllArtsFact------------------------------------------------
	@Transactional
	public int deleteAllArtsFact() throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllArtsFact","Entering","..>>...................");
		artsFactRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->FactsService->deleteAllArtsFact","Exiting","...................>>..");
		return 0;		
	}
	// ------------------------------------INITIALIZE FACTS MODEL DATE--------------------------------------------------------------	

	// -------------------Facts Model----->setModelFactsIsFactOrNote---------------------------------------------------------------	
	@Transactional
	public void setModelFactsIsFactOrNote() {
		consoleLog.logMsg(showLogMsg,"->FactsService->setModelFactsIsFactOrNote","Updating ...Model Facts","..>>...................");
		List<FactModels> modelFactsList = modelFactsRepository.findAll();
		for(int j=0; j<modelFactsList.size(); j++) {
			modelFactsList.get(j).setFactModelsFactType("NO Data");
			modelFactsList.get(j).setFactModelsPayType("NO Data");
			modelFactsList.get(j).setFactModelsNote(true);
			modelFactsRepository.save(modelFactsList.get(j));
		}	
		consoleLog.logMsg(showLogMsg,"->FactsService->setModelFactsIsFactOrNote","Updating ...Facts","..>>...................");
		List<Facts> factsList = factsRepository.findAll();
		for(int k=0; k<factsList.size(); k++) {
			factsList.get(k).setFactNote(true);
			factsRepository.save(factsList.get(k));
		}
		consoleLog.logMsg(showLogMsg,"->FactsService->setModelFactsIsFactOrNote","Exiting","...................>>..");
		return ;
	}
}
