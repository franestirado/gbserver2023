package com.fran.gestb.dbbarxx.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.dbbarxx.model.Hds;
import com.fran.gestb.dbbarxx.model.MonthTpvs;
import com.fran.gestb.dbbarxx.model.Months;
import com.fran.gestb.dbbarxx.repo.MonthTpvsRepository;
import com.fran.gestb.dbbarxx.repo.MonthsRepository;

@Service
public class MonthsService {

	@Autowired
	private MonthsRepository monthsRepository;
	@Autowired
	private MonthTpvsRepository monthTpvsRepository;

	@Autowired
	private ConsoleLog consoleLog;	
	private boolean showLogMsg = true;	
	
	// -------------------MONTHS---------------------------------------------------------------
	// -------------------MONTHS----->getMonthSummRecords------------------------------------------------
	@Transactional
	public List<Months> getMonthSummRecords(List <Date> firstAndLastDayOfYear) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MonthsService->getMonthSummRecords","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfYear.get(0)==null) || (firstAndLastDayOfYear.get(1)==null) ) 
			throw new EmptyFieldException("->MonthsService->getMonthSummRecords->Null Object or Empty Mandatoy Parameters");					
		List<Months> listMonths = monthsRepository.findAllByMonthDateGreaterThanEqualAndMonthDateLessThanEqual(firstAndLastDayOfYear.get(0),firstAndLastDayOfYear.get(1));
		if (listMonths == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfYear.get(0));
		consoleLog.logMsg(showLogMsg,"->MonthsService->getMonthSummRecords","Exiting","...................>>..");
		return listMonths;
	}
	// -------------------MONTHS----->getRetCountPrevMonthSummRecords------------------------------------------------
	@Transactional
	public List<Months> getRetCountPrevMonthSummRecords(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MonthsService->getRetCountPrevMonthSummRecords","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->MonthsService->getRetCountPrevMonthSummRecords->Null Object or Empty Mandatoy Parameters");			
		Calendar cal = Calendar.getInstance();
		cal.setTime(firstAndLastDayOfMonth.get(0));
		cal.add(Calendar.MONTH, -1);		
		Date firstDateOfPreviousMonth = cal.getTime();
		cal.setTime(firstAndLastDayOfMonth.get(1));
		cal.add(Calendar.MONTH, -1);		
		Date lastDateOfPreviousMonth = cal.getTime();	
		consoleLog.logMsg(showLogMsg,"->MonthsService->getRetCountPrevMonthSummRecords","Dates..>>...................",firstDateOfPreviousMonth.toString()+"....."+lastDateOfPreviousMonth.toString());
		var monthTypesList = new ArrayList <String>();
		monthTypesList.add(0,"RBC-DCOUNT");
		monthTypesList.add(1,"RRT-DCOUNT");
		List<Months> listMonths = monthsRepository.findByMonthTypeInAndMonthDateGreaterThanEqualAndMonthDateLessThanEqual(monthTypesList,firstDateOfPreviousMonth,lastDateOfPreviousMonth);
		if (listMonths == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfMonth.get(0));
		consoleLog.logMsg(showLogMsg,"->MonthsService->getRetCountPrevMonthSummRecords","Exiting","...................>>..");
		return listMonths;
	}
	// -------------------MONTHS----->getRetStartCountMonthSummRecords------------------------------------------------
	@Transactional
	public List<Months> getRetStartCountMonthSummRecords(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MonthsService->getRetStartCountMonthSummRecords","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->MonthsService->getRetStartCountMonthSummRecords->Null Object or Empty Mandatoy Parameters");			
		var monthTypesList = new ArrayList <String>();
		monthTypesList.add(0,"RBC-DCOUNT");
		monthTypesList.add(1,"RRT-DCOUNT");
		monthTypesList.add(2,"RBC-DSTART");
		monthTypesList.add(3,"RRT-DSTART");
		monthTypesList.add(4,"RBC-DSUM");
		monthTypesList.add(5,"RRT-DSUM");
		monthTypesList.add(6,"RBC-DRES");
		monthTypesList.add(7,"RRT-DRES");
		List<Months> listMonths = monthsRepository.findByMonthTypeInAndMonthDateGreaterThanEqualAndMonthDateLessThanEqual(monthTypesList,firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		if (listMonths == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfMonth.get(0));
		consoleLog.logMsg(showLogMsg,"->MonthsService->getRetStartCountMonthSummRecords","Exiting","...................>>..");
		return listMonths;
	}
	// -------------------Months----->createMonthsList------------------------------------------------
	@Transactional
	public List<Months> createMonthsList(List<Months> newMonthsList) throws EmptyFieldException, EntityAlreadyExistsException {
		List<Months> readMonthsList;
		Months createdMonth;	
		boolean createResult;
		
		consoleLog.logMsg(showLogMsg,"->MonthsService->createMonthsList","Entering","..>>...................");
		/* Checking null new object */
		if (newMonthsList==null) throw new EmptyFieldException("->MonthsService->createMonthsList->Null Object");
		/* Checking empty mandatory attributes  */
		for(int j = 0;j<newMonthsList.size();j++) {
			if ( newMonthsList.get(j).checkMonthAtributes() == false) 
					throw new EmptyFieldException("->MonthsService->createMonthsList->Empty Mandatory Parameters");
		}
		/* Checking there no other object with same name */
		readMonthsList = monthsRepository.findByMonthDate(newMonthsList.get(0).getMonthDate());
		if (readMonthsList.size() != 0) throw new EntityAlreadyExistsException("--Hd Date-->"+newMonthsList.get(0).getMonthDate());
		/* CREATING DATA */
		createResult = true;			
		readMonthsList = new ArrayList <Months>();
		for(int j = 0;j<newMonthsList.size();j++) {
			createdMonth = monthsRepository.save(newMonthsList.get(j));
			if (createdMonth == null) createResult = false;
			else readMonthsList.add(j,createdMonth);
		}
		if (createResult == false) readMonthsList = null;
		consoleLog.logMsg(showLogMsg,"->MonthsService->createMonthsList","Exiting","...................>>..");
		return readMonthsList;
	}
	// -------------------Months----->updateMonthsList------------------------------------------------
	@Transactional
	public List<Months> updateMonthsList(List<Months> updatedMonthsList) throws EmptyFieldException, EntityAlreadyExistsException {
		List<Months> readMonthsList;
		Months createdMonth;	
		boolean createResult;	
		consoleLog.logMsg(showLogMsg,"->MonthsService->updateMonthsList","Entering","..>>...................");
		/* Checking null new object */
		if (updatedMonthsList==null) throw new EmptyFieldException("->MonthsService->updateMonthsList->Null Object");
		/* Checking empty mandatory attributes  */
		for(int j = 0;j<updatedMonthsList.size();j++) {
			if ( updatedMonthsList.get(j).checkMonthAtributes() == false) 
					throw new EmptyFieldException("->MonthsService->updateMonthsList->Empty Mandatory Parameters");
		}
		/* SAVING DATA */
		createResult = true;			
		readMonthsList = new ArrayList <Months>();
		for(int j = 0;j<updatedMonthsList.size();j++) {
			createdMonth = monthsRepository.save(updatedMonthsList.get(j));
			if (createdMonth == null) createResult = false;
			else readMonthsList.add(j,createdMonth);
		}
		if (createResult == false) readMonthsList = null;
		consoleLog.logMsg(showLogMsg,"->MonthsService->updateMonthsList","Exiting","...................>>..");
		return readMonthsList;
	}
	// -------------------Months----->deleteMonthsRecords------------------------------------------------
	@Transactional
	public long deleteMonthsRecords(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MonthsService->deleteMonthsRecords","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->MonthsService->deleteMonthsRecords->Null Object or Empty Mandatoy Parameters");			
		consoleLog.logMsg(showLogMsg,"->MonthsService->deleteMonthsRecords","Dates..>>...................",firstAndLastDayOfMonth.get(0).toString()+"....."+firstAndLastDayOfMonth.get(1).toString());	
		long deletedMonthRecords = monthsRepository.deleteAllByMonthDateGreaterThanEqualAndMonthDateLessThanEqual(firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		long deletedMonthTpvRecords = monthTpvsRepository.deleteAllByMonthTpvDateGreaterThanEqualAndMonthTpvDateLessThanEqual(firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		long deletedMonthTotalRecords = deletedMonthRecords + deletedMonthTpvRecords;
		consoleLog.logMsg(showLogMsg,"->MonthsService->deleteMonthsRecords","Exiting","...................>>..");
		return deletedMonthTotalRecords;
	}
	
	
	// -------------------MONTHS TPVS---------------------------------------------------------------
	// -------------------Months TPVS----->createMonthsTpvsList------------------------------------------------
	@Transactional
	public List<MonthTpvs> createMonthsTpvsList(List<MonthTpvs> newMonthsTpvsList) throws EmptyFieldException, EntityAlreadyExistsException {
		List<MonthTpvs> readMonthsTpvsList;
		MonthTpvs createdMonthsTpv;	
		boolean createResult;
		
		consoleLog.logMsg(showLogMsg,"->MonthsService->createMonthsTpvsList","Entering","..>>...................");
		/* Checking null new object */
		if ((newMonthsTpvsList==null) || (newMonthsTpvsList.size()<1)) throw new EmptyFieldException("->MonthsService->createMonthsTpvsList->Null Object");
		/* Checking empty mandatory attributes  */
		for(int j = 0;j<newMonthsTpvsList.size();j++) {
			if ( newMonthsTpvsList.get(j).checkMonthTpvAtributes() == false) 
					throw new EmptyFieldException("->MonthsService->createMonthsTpvsList->Empty Mandatory Parameters");
		}
		/* Checking there no other object with same name */
		readMonthsTpvsList = monthTpvsRepository.findByMonthTpvDate(newMonthsTpvsList.get(0).getMonthTpvDate());
		if (readMonthsTpvsList.size() != 0) throw new EntityAlreadyExistsException("--Hd Date-->"+newMonthsTpvsList.get(0).getMonthTpvDate());
		/* CREATING DATA */
		createResult = true;			
		readMonthsTpvsList = new ArrayList <MonthTpvs>();
		for(int j = 0;j<newMonthsTpvsList.size();j++) {
			createdMonthsTpv = monthTpvsRepository.save(newMonthsTpvsList.get(j));
			if (createdMonthsTpv == null) createResult = false;
			else readMonthsTpvsList.add(j,createdMonthsTpv);
		}
		if (createResult == false) readMonthsTpvsList = null;
		consoleLog.logMsg(showLogMsg,"->MonthsService->createMonthsTpvsList","Exiting","...................>>..");
		return readMonthsTpvsList;
	}
	// -------------------MONTHS TPVS----->getMonthTpvRecords------------------------------------------------
	@Transactional
	public List<MonthTpvs> getMonthTpvRecords(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MonthsService->getMonthTpvRecords","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->MonthsService->getMonthTpvRecords->Null Object or Empty Mandatoy Parameters");			
		firstAndLastDayOfMonth.get(0).toString();
		consoleLog.logMsg(showLogMsg,"->MonthsService->getMonthTpvRecords","Dates..>>...................",firstAndLastDayOfMonth.get(0).toString()+"....."+firstAndLastDayOfMonth.get(1).toString());
		List<MonthTpvs> listMonthTpvs = monthTpvsRepository.findAllByMonthTpvDateGreaterThanEqualAndMonthTpvDateLessThanEqual(firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		if (listMonthTpvs == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfMonth.get(0));
		consoleLog.logMsg(showLogMsg,"->MonthsService->getMonthTpvRecords","Exiting","...................>>..");
		return listMonthTpvs;
	}
	
}
