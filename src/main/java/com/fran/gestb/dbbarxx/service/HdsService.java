package com.fran.gestb.dbbarxx.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.UserDoesNotExistException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.Fields;
import com.fran.gestb.dbbarc.repo.FieldsRepository;
import com.fran.gestb.dbbarxx.model.HdTpvs;
import com.fran.gestb.dbbarxx.model.HdTypes;
import com.fran.gestb.dbbarxx.model.Hds;
import com.fran.gestb.dbbarxx.repo.HdTpvsRepository;
import com.fran.gestb.dbbarxx.repo.HdTypesRepository;
import com.fran.gestb.dbbarxx.repo.HdsRepository;

@Service
public class HdsService {
	
	@Autowired
	private HdsRepository hdsRepository;
	@Autowired
	private HdTpvsRepository hdTpvsRepository;
	@Autowired
	private HdTypesRepository hdTypesRepository;
	@Autowired
	private FieldsRepository fieldsRepository;
	@Autowired
	private ConsoleLog consoleLog;
	
	private boolean showLogMsg = true;	
	private Date startDate, endDate;
	
	// -------------------HDS---------------------------------------------------------------
	// -------------------Hds----->GetListHds------------------------------------------------
	@Transactional
	public List<Hds> getListHds(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->getListHds","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->HdsService->getListHds->Null Object or Empty Mandatoy Parameters");			
		firstAndLastDayOfMonth.get(0).toString();
		consoleLog.logMsg(showLogMsg,"->HdsService->getListHds","Dates..>>...................",firstAndLastDayOfMonth.get(0).toString()+"....."+firstAndLastDayOfMonth.get(1).toString());
		List<Hds> listHds = hdsRepository.findAllByHdTypeAndHdDateGreaterThanEqualAndHdDateLessThanEqual("DAY-D",firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		if (listHds == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfMonth.get(0));
		consoleLog.logMsg(showLogMsg,"->HdsService->getListHds","Exiting","...................>>..");
		return listHds;
	}
	// -------------------Hds----->getHdsMonth------------------------------------------------
	@Transactional
	public List<Hds> getHdsMonth(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->getHdsMonth","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->HdsService->getHdsMonth->Null Object or Empty Mandatoy Parameters");			
		firstAndLastDayOfMonth.get(0).toString();
		consoleLog.logMsg(showLogMsg,"->HdsService->getHdsMonth","Dates..>>...................",firstAndLastDayOfMonth.get(0).toString()+"....."+firstAndLastDayOfMonth.get(1).toString());
		List<Hds> listHds = hdsRepository.findAllByHdDateGreaterThanEqualAndHdDateLessThanEqual(firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		if (listHds == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfMonth.get(0));
		consoleLog.logMsg(showLogMsg,"->HdsService->getHdsMonth","Exiting","...................>>..");
		return listHds;
	}
	// -------------------Hds----->GetDateHds------------------------------------------------
	@Transactional
	public List<Hds> getDateHds(Date workingDate) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->getDateHds","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((workingDate==null) ) throw new EmptyFieldException("->HdsService->getDateHds->Null Object or Empty Mandatoy Parameters");	
		List<Hds> readHds = hdsRepository.findByHdDate(workingDate);
		if (readHds == null) throw new EntityNotFoundException("--Hd Date-->"+workingDate);
		consoleLog.logMsg(showLogMsg,"->HdsService->getDateHds","Exiting","...................>>..");
		return readHds;
	}
	// -------------------Hds----->createSumHd------------------------------------------------
	@Transactional
	public Hds createSumHd(Hds sumHd) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->HdsService->saveHd","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((sumHd==null) | (sumHd.getHdName()=="")) throw new EmptyFieldException("->HdsService->createSumHd->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (sumHd.checkHdAtributes()==false) throw new 
				WrongParametersException("--Hd Name-->"+sumHd.getHdName()+"--Hd Long Name-->"+sumHd.getHdType());
		/* Checking there no other object with same name 
		Hds readHd = hdsRepository.findByHdName(sumHd.getHdName());
		if (readHd != null) throw new EntityAlreadyExistsException("--Hd Name-->"+readHd.getHdName()); */
		Hds readHd;
		readHd = hdsRepository.save(sumHd);
		consoleLog.logMsg(showLogMsg,"->HdsService->createSumHd","Exiting","...................>>..");
		return readHd;
	}
	// -------------------Hds----->createDateHds------------------------------------------------
	@Transactional
	public List<Hds> createDateHds(List<Hds> newDateHds) throws EmptyFieldException, EntityAlreadyExistsException {
		List<Hds> readDateHds;
		Hds createdHd;	
		boolean createResult;
		
		consoleLog.logMsg(showLogMsg,"->HdsService->createDateHds","Entering","..>>...................");
		/* Checking null new object */
		if (newDateHds==null) throw new EmptyFieldException("->HdsService->createDateHds->Null Object");
		/* Checking empty mandatory attributes  */
		for(int j = 0;j<newDateHds.size();j++) {
			if ( newDateHds.get(j).checkHdAtributes() == false) 
					throw new EmptyFieldException("->HdsService->createDateHds->Empty Mandatory Parameters");
		}
		/* Checking there no other object with same name */
		consoleLog.logMsg(showLogMsg,"->HdsService->createDateHds","....1......",newDateHds.get(0).getHdDate().toString());		
		readDateHds = hdsRepository.findByHdDate(newDateHds.get(0).getHdDate());
		consoleLog.logMsg(showLogMsg,"->HdsService->createDateHds","....2......",".."+readDateHds.size()+"..");
		if (readDateHds.size() != 0) throw new EntityAlreadyExistsException("--Hd Date-->"+newDateHds.get(0).getHdDate());
		/* CREATING DATA */
		createResult = true;			
		readDateHds = new ArrayList <Hds>();
		for(int j = 0;j<newDateHds.size();j++) {
			consoleLog.logMsg(showLogMsg,"->HdsService->createDateHds","....3......",".."+j+"..");
			createdHd = hdsRepository.save(newDateHds.get(j));
			if (createdHd == null) createResult = false;
			else readDateHds.add(j,createdHd);
		}
		if (createResult == false) readDateHds = null;
		consoleLog.logMsg(showLogMsg,"->HdsService->createDateHds","Exiting","...................>>..");
		return readDateHds;
	}
	// -------------------Hds----->updateDateHds------------------------------------------------
	@Transactional
	public List<Hds> updateDateHds(List<Hds> updDateHds) throws EmptyFieldException, EntityAlreadyExistsException {
		List<Hds> readDateHds;
		Hds savedHd;	
		boolean saveResult;
		saveResult = true;
		consoleLog.logMsg(showLogMsg,"->HdsService->updateDateHds","Entering","..>>...................");
		/* Checking null new object */
		if (updDateHds==null) throw new EmptyFieldException("->HdsService->saveDateHds->Null Object");
		/* Checking empty mandatory attributes  */
		for(int j = 0;j<updDateHds.size();j++) {
			if ( updDateHds.get(j).checkHdAtributes() == false) 
					throw new EmptyFieldException("->HdsService->saveDateHds->Empty Mandatory Parameters");
		}
		readDateHds = new ArrayList <Hds>();
		for(int j = 0;j<updDateHds.size();j++) {
			//savedHd = hdsRepository.findByHdId(updDateHds.get(j).getHdId());
			//consoleLog.logMsg(showLogMsg,"->HdsService->updateDateHds.....",updDateHds.get(j).getHdType(),updDateHds.get(j).getHdNotes());	
			savedHd = hdsRepository.save(updDateHds.get(j));
			if (savedHd == null) saveResult = false;
			else readDateHds.add(j,savedHd);
		}
		consoleLog.logMsg(showLogMsg,"->HdsService->updateDateHds","Exiting","...................>>..");
		if (saveResult == false) readDateHds = null;
		return readDateHds;
	}	
	// -------------------Hds----->deleteOneSumHd------------------------------------------------
	@Transactional
	public long deleteOneSumHd(Hds delSumHd) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteDateHds","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((delSumHd==null) ) throw new EmptyFieldException("->HdsService->deleteOneSumHd->Null Object or Empty Mandatoy Parameters");	
		hdsRepository.deleteById(delSumHd.getHdId());	
		long deleteResult = 1;
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteOneSumHd","Exiting","...................>>..");
		return deleteResult;
	}
	// -------------------Hds----->deleteAllSumHds------------------------------------------------
	@Transactional
	public long deleteAllSumHds(List<Hds> delSumHds) throws EmptyFieldException, EntityAlreadyExistsException {	
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteAllSumHds","Entering","..>>...................");
		/* Checking null new object */
		if (delSumHds==null) throw new EmptyFieldException("->HdsService->deleteAllSumHds->Null Object");
		/* Checking empty mandatory attributes  */
		long deletedSumHds = 0;
		for(int j = 0;j<delSumHds.size();j++) {
			deletedSumHds = deletedSumHds + hdsRepository.deleteByHdId(delSumHds.get(j).getHdId());
		}
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteAllSumHds","Exiting","...................>>..");		
		return deletedSumHds;
	}
	// -------------------Hds----->DeleteDateHds------------------------------------------------
	@Transactional
	public long deleteDateHds(Date workingDate) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteDateHds","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((workingDate==null) ) throw new EmptyFieldException("->HdsService->deleteDateHds->Null Object or Empty Mandatoy Parameters");	
		long deleteResult = hdsRepository.deleteAllByHdDate(workingDate);		
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteDateHds","Deletind date hdTpvs","...................>>..");
		deleteResult = deleteResult + hdTpvsRepository.deleteAllByHdTpvDate(workingDate);	
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteDateHds","Exiting","...................>>..");
		return deleteResult;
	}
	// -------------------Hds----->DeleteMonthHds------------------------------------------------
	@Transactional
	public long deleteMonthHds(List <Date> firstAndLastDayOfMonth) 
		throws EmptyFieldException, EntityAlreadyExistsException,EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->DeleteMonthHds","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) {
			throw new EmptyFieldException("->HdsService->getListHds->Null Object or Empty Mandatoy Parameters");
		}
		long deleteResult = 0;
		startDate = firstAndLastDayOfMonth.get(0);
		endDate = firstAndLastDayOfMonth.get(1);	
		consoleLog.logMsg(showLogMsg,"->HdsService->DeleteMonthHds","...startDate...."+startDate.toString(),"...endDate...."+endDate.toString());
		Calendar start = Calendar.getInstance();
		start.setTime(startDate);
		Calendar end = Calendar.getInstance();
		end.setTime(endDate);
		end.add(Calendar.DATE, 1);		
		for (Date date = start.getTime(); start.before(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
			deleteResult = deleteResult + hdsRepository.deleteAllByHdDate(date);	
			deleteResult = deleteResult + hdTpvsRepository.deleteAllByHdTpvDate(date);	
		}						
		consoleLog.logMsg(showLogMsg,"->HdsService->DeleteMonthHds","Exiting","...................>>..");
		return deleteResult;
	}	
	// -------------------HD TPVS----------------------------------------------------------------------
	// -------------------HdTpvs----->GetDateHdTpvs------------------------------------------------
	@Transactional
	public List<HdTpvs> getDateHdTpvs(Date workingDate) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->getDateHdTpvs","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((workingDate==null) ) throw new EmptyFieldException("->HdTpvsService->getDateHdTpvs->Null Object or Empty Mandatoy Parameters");	
		List<HdTpvs> readHdTpvs = hdTpvsRepository.findByHdTpvDate(workingDate);
		if (readHdTpvs == null) throw new EntityNotFoundException("--HdTpv Date-->"+workingDate);
		consoleLog.logMsg(showLogMsg,"->HdsService->getDateHdTpvs","Exiting","...................>>..");
		return readHdTpvs;
	}
	// -------------------Hds----->getHdTpvsMonth------------------------------------------------
	@Transactional
	public List<HdTpvs> getHdTpvsMonth(List <Date> firstAndLastDayOfMonth) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->getHdTpvsMonth","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfMonth.get(0)==null) || (firstAndLastDayOfMonth.get(1)==null) ) 
			throw new EmptyFieldException("->HdsService->getHdTpvsMonth->Null Object or Empty Mandatoy Parameters");			
		firstAndLastDayOfMonth.get(0).toString();
		consoleLog.logMsg(showLogMsg,"->HdsService->getHdTpvsMonth","Dates..>>...................",firstAndLastDayOfMonth.get(0).toString()+"....."+firstAndLastDayOfMonth.get(1).toString());
		List<HdTpvs> listHdTpvs = hdTpvsRepository.findAllByHdTpvDateGreaterThanEqualAndHdTpvDateLessThanEqual(firstAndLastDayOfMonth.get(0),firstAndLastDayOfMonth.get(1));
		if (listHdTpvs == null) throw new EntityNotFoundException("--Hd Date-->"+firstAndLastDayOfMonth.get(0));
		consoleLog.logMsg(showLogMsg,"->HdsService->getHdTpvsMonth","Exiting","...................>>..");
		return listHdTpvs;
	}
	// -------------------HdTpvs----->createDateHdTpvs------------------------------------------------
	@Transactional
	public List<HdTpvs> createDateHdTpvs(List<HdTpvs> newDateHdTpvs) throws EmptyFieldException, EntityAlreadyExistsException {
		List<HdTpvs> readDateHdTpvs;
		HdTpvs createdHdTpv;	
		boolean createResult;
		
		consoleLog.logMsg(showLogMsg,"->HdsService->createDateHdTpvs","Entering","..>>...................");
		/* Checking null new object */
				if ( (newDateHdTpvs==null) || (newDateHdTpvs.size()==0) ) throw new EmptyFieldException("->HdsService->createDateHdTpvs->Null Object");
		/* Checking empty mandatory attributes  */
				for(int j = 0;j<newDateHdTpvs.size();j++) {
					if ( newDateHdTpvs.get(j).checkHdTpvAtributes() == false) 
		throw new EmptyFieldException("->HdsService->createDateHdTpvs->Empty Mandatory Parameters");
		}
		/* Checking there no other object with same name */
		readDateHdTpvs = hdTpvsRepository.findByHdTpvDate(newDateHdTpvs.get(0).getHdTpvDate());
		if (readDateHdTpvs.size() != 0) throw new EntityAlreadyExistsException("--Hd Date-->"+newDateHdTpvs.get(0).getHdTpvDate());
		/* CREATING DATA	 */
		createResult = true;			
		readDateHdTpvs = new ArrayList <HdTpvs>();
		for(int j = 0;j<newDateHdTpvs.size();j++) {
			createdHdTpv = hdTpvsRepository.save(newDateHdTpvs.get(j));
			if (createdHdTpv == null) { createResult = false; }
			else { readDateHdTpvs.add(j,createdHdTpv); }						
		}
		if (createResult == false) { readDateHdTpvs = null; }		
		consoleLog.logMsg(showLogMsg,"->HdsService->createDateHdTpvs","Exiting","...................>>..");		
		return readDateHdTpvs;		
	}
	// -------------------HdTpvs----->updateDateHdTpvs------------------------------------------------
	@Transactional
	public List<HdTpvs>  updateDateHdTpvs(List<HdTpvs> updDateHdTpvs) throws EmptyFieldException, EntityAlreadyExistsException {
		List<HdTpvs> readDateHdTpvs;
		HdTpvs savedHdTpv;	
		boolean saveResult;
		saveResult = false;
		consoleLog.logMsg(showLogMsg,"->HdsService->updateDateHdTpvs","Entering","..>>...................");
		/* Checking null new object */
		if (updDateHdTpvs==null) throw new EmptyFieldException("->HdsService->updateDateHdTpvs->Null Object");
		/* Checking empty mandatory attributes  */
		for(int j = 0;j<updDateHdTpvs.size();j++) {
			if ( updDateHdTpvs.get(j).checkHdTpvAtributes() == false) 
					throw new EmptyFieldException("->HdsService->updateDateHdTpvs->Empty Mandatory Parameters");
		}
		/* UPDATING DATA	 */
		saveResult = true;
		readDateHdTpvs = new ArrayList <HdTpvs>();
		for(int j = 0;j<updDateHdTpvs.size();j++) {			
			savedHdTpv = hdTpvsRepository.save(updDateHdTpvs.get(j));
			if (savedHdTpv == null) { saveResult = false; }
			else { readDateHdTpvs.add(j,savedHdTpv); }		
		}
		if (saveResult == false) { readDateHdTpvs = null; }	
		consoleLog.logMsg(showLogMsg,"->HdsService->updateDateHdTpvs","Exiting","...................>>..");
		return readDateHdTpvs;
	}
	// -------------------HdTpvs----->readDataPreviousDate------------------------------------------------
	@Transactional
	public List<HdTpvs> readDataPreviousDate(Date workingDate) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->readDataPreviousDate","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((workingDate==null) ) throw new EmptyFieldException("->HdTpvsService->readDataPreviousDate->Null Object or Empty Mandatoy Parameters");	
		consoleLog.logMsg(showLogMsg,"->HdsService->readDataPreviousDate","Working date.....",workingDate.toString());
		Date previousDate;
		previousDate = new Date();
		Calendar cal= Calendar.getInstance();
		cal.setTime(workingDate);
		cal.add(Calendar.DATE, -1);
		previousDate = cal.getTime();			
		consoleLog.logMsg(showLogMsg,"->HdsService->readDataPreviousDate","Previous date.....",previousDate.toString());
		
		List<Hds> previousHds = hdsRepository.findByHdDateAndHdType(previousDate,"DCONT");
		
		consoleLog.logMsg(showLogMsg,"->HdsService->readDataPreviousDate","hd name.....",previousHds.get(0).getHdName());
		consoleLog.logMsg(showLogMsg,"->HdsService->readDataPreviousDate","hd amount.....",String.valueOf(previousHds.get(0).getHdAmount()) );
		
		List<HdTpvs> previousHdTpvs = hdTpvsRepository.findByHdTpvDate(previousDate);
		HdTpvs previousHdAmount;
		previousHdAmount = new HdTpvs(previousDate,"DCONT","DCONT",0,0,0,previousHds.get(0).getHdAmount(),previousHds.get(0).getHdAmount());
		previousHdTpvs.add(previousHdAmount);
		consoleLog.logMsg(showLogMsg,"->HdsService->readDataPreviousDate","Exiting","...................>>..");
		return previousHdTpvs;
	}
	
	// -------------------HD TYPES----------------------------------------------------------------------
	// -------------------HdTypes----->getAllHdTypes-------------------------------------------	
	@Transactional(readOnly = true)
	public List<HdTypes> getAllHdTypes() {
		consoleLog.logMsg(showLogMsg,"->HdsService->getAllHdTypes","Entering","..>>...................");
		List<HdTypes> hdTypesList = hdTypesRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->HdsService->getAllHdTypes","Exiting","...................>>..");
		return hdTypesList;
	}	
	// -------------------HdTypes----->createBarHdTypes-------------------------------------------	
	@Transactional
	public List<HdTypes> createBarHdTypes(String currentBar) {
		consoleLog.logMsg(showLogMsg,"->HdsService->createBarHdTypes","Entering","..>>...................");
		this.startCreateHdTypes(currentBar);
		List<HdTypes> hdTypesList = hdTypesRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->HdsService->createBarHdTypes","Exiting","...................>>..");
		return hdTypesList;
	}	
	// -------------------HdTypes----->getOneHdType------------------------------------------------
	@Transactional
	public HdTypes getOneHdType(HdTypes modHdType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdsService->getOneHdType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modHdType==null) | (modHdType.getHdTypeName()=="")) throw new EmptyFieldException("->HdTypesService->getOneHdType->Null Object or Empty Mandatoy Parameters");	
		HdTypes readHdType = hdTypesRepository.findByHdTypeId(modHdType.getHdTypeId());
		if (readHdType == null) throw new EntityNotFoundException("--HdType Name-->"+modHdType.getHdTypeName());
		consoleLog.logMsg(showLogMsg,"->HdsService->getOneHdType","Exiting","...................>>..");
		return readHdType;
	}	
	// -------------------HdTypes----->createOneHdType------------------------------------------------
	@Transactional
	public HdTypes createOneHdType(HdTypes newHdType) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->HdsService->saveHdType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newHdType==null) | (newHdType.getHdTypeName()=="")) throw new EmptyFieldException("->HdsService->createOneHdType->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newHdType.checkHdTypeAtributes()==false) throw new 
				WrongParametersException("--HdType Name-->"+newHdType.getHdTypeName()+"--HdType Long Name-->"+newHdType.getHdTypeType());
		/* Checking there no other object with same name */
		HdTypes readHdType = hdTypesRepository.findByHdTypeName(newHdType.getHdTypeName());
		if (readHdType != null) throw new EntityAlreadyExistsException("--HdType Name-->"+readHdType.getHdTypeName());
		readHdType = hdTypesRepository.save(newHdType);
		consoleLog.logMsg(showLogMsg,"->HdsService->createOneHdType","Exiting","...................>>..");
		return readHdType;
	}	
	// -------------------HdTypes----->updateOneHdType------------------------------------------------
	@Transactional
	public HdTypes updateOneHdType(HdTypes modHdType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->HdTypesService->updateOneHdType","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ((modHdType==null) | (modHdType.getHdTypeName()=="")) throw new EmptyFieldException("->HdsService->updateOneHdType->Null Object or Empty Mandatoy Parameters");	
		HdTypes readHdType = hdTypesRepository.findByHdTypeId(modHdType.getHdTypeId());
		if (readHdType == null) throw new EntityNotFoundException("--HdType Name-->"+modHdType.getHdTypeName());
		readHdType = hdTypesRepository.save(modHdType);
		consoleLog.logMsg(showLogMsg,"->HdsService->updateOneHdType","Exiting","...................>>..");
		return readHdType;
	}	
	// -------------------HdTypes----->DeleteOne------------------------------------------------
	@Transactional
	public void deleteOneHdType(HdTypes delHdType) {
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteOneHdType","Entering","..>>...................");
		// Check if the field exist 
		HdTypes checkHdType = hdTypesRepository.findByHdTypeId(delHdType.getHdTypeId());
		if (checkHdType == null) throw new UserDoesNotExistException("Field does NOT exists-->");
		hdTypesRepository.deleteById(delHdType.getHdTypeId());
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteOneHdType","Exiting","...................>>..");
	}
	// -------------------HdTypes----->DeleteAll------------------------------------------------
	@Transactional
	public void deleteAllHdTypes() {
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteAllHdTypes","Entering","..>>...................");
		hdTypesRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->HdsService->deleteAllHdTypes","Exiting","...................>>..");
	}
	// -------------------HDS-----HdTypes----------------------------------------------------------
	// -------------------HdTypes----->At start up creates default HD TYPES--------------------------------------
	//@Transactional
	public boolean startCreateHdTypes(String selectedBarName) {
		consoleLog.logMsg(showLogMsg,"->HdsService->startCreateHdTypes","Entering","...................>>..");	
		consoleLog.logMsg(showLogMsg,"->HdsService->startCreateHdTypes","selectedBarName.....",selectedBarName);	

		// --------------hdTypeName---hdTypeType---hdTypePlace---hdTypeOrder---hdTypeStatus	
		switch(selectedBarName) {
        case "RUBI":
        	createHdTypesRubi();
        	break;
        case "ESGO":
        	createHdTypesEsgo();
            break;
        default:
        	//createHdTypesRubi();
            break;
		}
		consoleLog.logMsg(showLogMsg,"->HdsService->startCreateHdTypes","Exiting","...................>>..");	
		return true;
    }	
	// -------------------HDS-----HdTypes----------------------------------------------------------
	// -------------------HdTypes----->At start up creates default HD TYPES for ESGO -----------------
	//@Transactional
	public void createHdTypesEsgo() {
    	consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypesEsgo","Starting","...................>>..");	

		this.createHdTypes("T02-Barra-Noche"	,"TPV1",	"TPV",10,true);
		this.createHdTypes("T02-Barra-Día"	    ,"TPV2",	"TPV",20,true);
		this.createHdTypes("T01-Salon-Día"		,"TPV2",	"TPV",21,true);
		
		this.createHdTypes("Sante Fe"   		,"MAQB",	"SUM",11,true);	
		this.createHdTypes("Gnomos Mix"			,"MAQB",	"SUM",11,true);	
		this.createHdTypes("Tabaco"				,"TAB" ,	"SUM",31,true);
		this.createHdTypes("Genero Vendido"		,"GOODS" ,	"SUM",31,true);
		this.createHdTypes("Varios Suma"		,"OTHER" ,	"SUM",31,true);
		this.createHdTypes("Cesta Navidad Mahou","OTHER" ,	"SUM",31,true);
		this.createHdTypes("Aceite Usado"       ,"OTHER" ,	"SUM",31,true);
		this.createHdTypes("Mahou Rappel"       ,"OTHER" ,	"SUM",31,true);

		this.createHdTypes("B.Sabad.CB"			,"BANK",	"RES",52,true);	
		this.createHdTypes("B.Sabad.4.Tit."		,"BANK",	"RES",52,true);	
		this.createHdTypes("Bankinter.C1"		,"BANK",	"RES",53,true);	
		this.createHdTypes("Caja.C1"			,"CAJA",	"RES",53,true);	
		this.createHdTypes("Paz"				,"RET",		"RES",53,true);	
		this.createHdTypes("Fran"				,"RET",		"RES",53,true);	
		this.createHdTypes("Alber"				,"RET",		"RES",53,true);
		this.createHdTypes("Vales.Empleados"	,"RET",		"RES",53,true);	
		this.createHdTypes("Vales.Clientes"		,"RET",		"RES",53,true);	
		
		this.createHdTypes("HojaDia"			,"HojaDia",	"HD" ,55,true);
		
    	consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypesEsgo","Exiting","...................>>..");	
    	return;
	}
	// -------------------HdTypes----->At start up creates default HD TYPES for RUBI -----------------
	//@Transactional
	public void createHdTypesRubi() {
    	consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypesRubi","Starting","...................>>..");	

		this.createHdTypes("T02-Cafetera-Noche"	,"TPV1",	"TPV",10,true);
		this.createHdTypes("T01-Cocina-Noche"	,"TPV1",	"TPV",11,true);
		this.createHdTypes("T02-Cafetera-Día"	,"TPV2",	"TPV",20,true);
		this.createHdTypes("T01-Cocina-Día"		,"TPV2",	"TPV",21,true);
		
		this.createHdTypes("Tesoro"				,"MAQB",	"SUM",11,true);	
		this.createHdTypes("Neopolis"			,"MAQB",	"SUM",12,true);	
		this.createHdTypes("Gnomos"				,"MAQB",	"SUM",11,true);	
		this.createHdTypes("Cochecito"			,"MAQA",	"SUM",21,true);	
		this.createHdTypes("Tabaco"				,"TAB" ,	"SUM",31,true);
		this.createHdTypes("Genero Vendido"		,"GOODS" ,	"SUM",31,true);
		this.createHdTypes("Varios Suma"		,"OTHER" ,	"SUM",31,true);
		this.createHdTypes("Cesta Navidad Mahou","OTHER" ,	"SUM",31,true);
		
		this.createHdTypes("B.Sabad.C1"			,"BANK",	"RES",52,true);	
		this.createHdTypes("Bankinter.C1"		,"BANK",	"RES",53,true);	
		this.createHdTypes("Caja.C1"			,"CAJA",	"RES",53,true);	
		this.createHdTypes("Paz"				,"RET",		"RES",53,true);	
		this.createHdTypes("Fran"				,"RET",		"RES",53,true);	
		this.createHdTypes("Alber"				,"RET",		"RES",53,true);
		this.createHdTypes("Vales.Empleados"	,"RET",		"RES",53,true);	
		this.createHdTypes("Vales.Clientes"		,"RET",		"RES",53,true);	
		
		this.createHdTypes("HojaDia"			,"HojaDia",	"HD" ,55,true);
		
    	consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypesRubi","Exiting","...................>>..");	
    	return;
	}
	// -------------------HdTypes----- Creates HD TYPES if they do not exist yet -------------------------------------------
	//@Transactional
	public void createHdTypes(String hdTypeName, String hdTypeType, String hdTypePlace, int hdTypeOrder, boolean hdTypeStatus) {
		consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypes","Entering","...................>>..");	
		if ((hdTypeName == null) || (hdTypeName == "") || (hdTypeType == null) || (hdTypePlace == null) ) return;	

		if ( (hdTypesRepository.findByHdTypeName(hdTypeName)) == null ) {
			Date currentDate = new Date();
			HdTypes savedHdType = hdTypesRepository.save(new HdTypes(hdTypeName,currentDate,hdTypeType,hdTypePlace,hdTypeOrder,hdTypeStatus));
			if (savedHdType == null) consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypes","hdType null...................>>..",hdTypeName);	
		}
    	consoleLog.logMsg(showLogMsg,"->HdsService->createHdTypes","Exiting","...................>>..");	
    	return;
	}
	// ------------------------------------INITIALIZE HD TYPES DATE--------------------------------------------------------------	
	// -------------------Hd Types----->setHdTypesCurrentDate---------------------------------------------------------------	
	@Transactional
	public void setHdTypesCurrentDate() {
		consoleLog.logMsg(showLogMsg,"->HdsService->setHdTypesCurrentDate","Entering","..>>...................");
		Date currentDate = new Date();
		List<HdTypes> hdTypesList = hdTypesRepository.findAll();
		for(int j=0; j<hdTypesList.size(); j++) {
			if (hdTypesList.get(j).getHdTypeDate() == null) {
				hdTypesList.get(j).setHdTypeDate(currentDate);
				hdTypesRepository.save(hdTypesList.get(j));
			}
		}		
		consoleLog.logMsg(showLogMsg,"->HdsService->setHdTypesCurrentDate","Exiting","...................>>..");
		return ;
	}	

}
