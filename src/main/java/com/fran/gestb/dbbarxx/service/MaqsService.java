package com.fran.gestb.dbbarxx.service;

import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarxx.model.Hds;
import com.fran.gestb.dbbarxx.model.Maqs;
import com.fran.gestb.dbbarxx.repo.HdsRepository;
import com.fran.gestb.dbbarxx.repo.MaqsRepository;

@Service
public class MaqsService {
	@Autowired
	private MaqsRepository maqsRepository;
	@Autowired
	private HdsRepository hdsRepository;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	// -------------------Maqs------------------------------------------------------------------------------------
	// -------------------Maqs----->getMaqsLits----------------------------------------------------------	
	@Transactional
	public List<Maqs> getMaqsLits( List<Date> firstAndLastDayOfYear) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->getMaqsLits","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfYear==null) || (firstAndLastDayOfYear.size()==0)) 
			throw new EmptyFieldException("->MaqsService->getMaqsLits->Null Object or Empty Mandatoy Parameters");
		 List<Maqs> readMaqReclist = maqsRepository.findByMaqSumDataTypeAndMaqEndDateGreaterThanEqualAndMaqEndDateLessThanEqual("SUMM",firstAndLastDayOfYear.get(0),firstAndLastDayOfYear.get(1));
		 consoleLog.logMsg(showLogMsg,"->MaqsService->getMaqsLits","Exiting","..>>...................");
		 return readMaqReclist;
	}
	// -------------------Maqs----->checkIfMaqsIsInDB----------------------------------------------------------	
	@Transactional
	public List<Maqs> checkIfMaqsIsInDB(List<ValueRecord> dateRangeMaqBNameVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->checkIfMaqsIsInDB","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		//formatter.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));	 
		//String dateInString = "22-01-2015 10:15:55 AM"; 
		Date startDate,endDate;
		try {
			endDate = formatter.parse(dateRangeMaqBNameVRList.get(1).getValue());
			//consoleLog.logMsg(showLogMsg,"->MaqsService->checkIfMaqsIsInDB","..endDate........",endDate.toString());
			startDate = formatter.parse(dateRangeMaqBNameVRList.get(0).getValue());
			//consoleLog.logMsg(showLogMsg,"->MaqsService->checkIfMaqsIsInDB","..startDate..",startDate.toString());

		} catch (ParseException e) {
			throw new EmptyFieldException("->MaqsService->getHdsMaqs.1.->Wrong DATE Object ");
			}
		 List<Maqs> readMaqReclist = maqsRepository.findByMaqMaqBNameAndMaqStartDateAndMaqEndDate(dateRangeMaqBNameVRList.get(2).getValue(),
				 						startDate,endDate);
			// consoleLog.logMsg(showLogMsg,"->MaqsService->checkIfMaqsIsInDB","....",cardPayoutVRList.get(j).getValue());
		return readMaqReclist;
	}
	// -------------------Maqs----->getHdsMaqs----------------------------------------------------------	
	@Transactional
	public List<Hds> getHdsMaqs(List<ValueRecord> dateRangeMaqBNameVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->getHdsMaqs","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		//formatter.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));	 
		//String dateInString = "22-01-2015 10:15:55 AM"; 
		Date startDate,endDate;
		try {
			endDate = formatter.parse(dateRangeMaqBNameVRList.get(1).getValue());
			//consoleLog.logMsg(showLogMsg,"->MaqsService->getHdsMaqs","..endDate........",endDate.toString());
			startDate = formatter.parse(dateRangeMaqBNameVRList.get(0).getValue());
			//consoleLog.logMsg(showLogMsg,"->MaqsService->getHdsMaqs","..startDate..",startDate.toString());

		} catch (ParseException e) {
			throw new EmptyFieldException("->MaqsService->getHdsMaqs.1.->Wrong DATE Object ");
			}
		
		List<Hds> readHdsMaqReclist = hdsRepository.findAllByHdNameAndHdDateGreaterThanEqualAndHdDateLessThanEqual(dateRangeMaqBNameVRList.get(2).getValue(),
				 						startDate,endDate);
			// consoleLog.logMsg(showLogMsg,"->MaqsService->getHdsMaqs","....",cardPayoutVRList.get(j).getValue());
		return readHdsMaqReclist;
	}
	// -------------------Maqs----->createOneMaq----------------------------------------------------------	
	@Transactional
	public List<Maqs> createOneMaq( List<Maqs> newMaqSummList) 
									throws EmptyFieldException,EntityNotFoundException,EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->createOneMaq","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newMaqSummList==null) || (newMaqSummList.size()==0)) 
									throw new EmptyFieldException("->MaqsService->createOneMaq->Null Object or Empty Mandatoy Parameters");
		/* Checking there no other object with same name */	
		/*
		List<Maqs> readMaqSummList = maqsRepository.findByMaqMaqBNameAndMaqStartDateAndMaqEndDate(newMaqSummList.get(0).getMaqMaqBName(),
													newMaqSummList.get(0).getMaqStartDate() ,newMaqSummList.get(0).getMaqEndDate());

		if ( (readMaqSummList != null) ) 
									throw new EntityAlreadyExistsException("--Hd Date-->"+newMaqSummList.get(0).getMaqMaqBName()+
													newMaqSummList.get(0).getMaqStartDate()+newMaqSummList.get(0).getMaqEndDate());
													*/
		List<Maqs> createdMaqSummList;
		createdMaqSummList = new ArrayList <Maqs>();
		Maqs createdMaqSumm;
		for(int j = 0;j<newMaqSummList.size();j++) {
			boolean attrOk = newMaqSummList.get(j).checkMaqAtributes();
			if (attrOk == false) throw new WrongParametersException("->MaqsService->createOneMaq->Null Object or Empty Mandatoy Parameters");
			createdMaqSumm = maqsRepository.save(newMaqSummList.get(j));
			createdMaqSummList.add(createdMaqSumm);
		}
		consoleLog.logMsg(showLogMsg,"->MaqsService->createOneMaq","Exiting","..>>...................");
		return createdMaqSummList;
	}
	// -------------------Maqs----->updateOneMaq----------------------------------------------------------	
	@Transactional
	public Maqs updateOneMaq( Maqs updMaqSumm) throws EmptyFieldException,EntityNotFoundException,EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->updateOneMaq","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if (updMaqSumm==null) throw new EmptyFieldException("->MaqsService->updateOneMaq->Null Object or Empty Mandatoy Parameters");
		Maqs updatedMaqSumm = maqsRepository.save(updMaqSumm);
		consoleLog.logMsg(showLogMsg,"->MaqsService->updateOneMaq","Exiting","..>>...................");
		return updatedMaqSumm;
	}
	// -------------------Maqs----->deleteOneMaq----------------------------------------------------------	
	@Transactional
	public long deleteOneMaq( Maqs delMaqSumm) throws EmptyFieldException,EntityNotFoundException,EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->deleteOneMaq","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if (delMaqSumm==null) throw new EmptyFieldException("->MaqsService->deleteOneMaq->Null Object or Empty Mandatoy Parameters");
		long deletedMaqSumm = maqsRepository.deleteByMaqMaqBNameAndMaqStartDateAndMaqEndDate(delMaqSumm.getMaqMaqBName(),delMaqSumm.getMaqStartDate(),delMaqSumm.getMaqEndDate());
		consoleLog.logMsg(showLogMsg,"->MaqsService->deleteOneMaq","Exiting","..>>...................");
		return deletedMaqSumm;
	}
	// -------------------Maqs----->deleteAllMaqs----------------------------------------------------------	
	@Transactional
	public long deleteAllMaqs( List<Date> firstAndLastDayOfYear) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->deleteAllMaqs","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((firstAndLastDayOfYear==null) || (firstAndLastDayOfYear.size()==0)) 
								throw new EmptyFieldException("->MaqsService->deleteAllMaqs->Null Object or Empty Mandatoy Parameters");
		 long delMaqReclist = maqsRepository.deleteByMaqEndDateGreaterThanEqualAndMaqEndDateLessThanEqual(firstAndLastDayOfYear.get(0),firstAndLastDayOfYear.get(1));
		 consoleLog.logMsg(showLogMsg,"->MaqsService->deleteAllMaqs","Exiting","..>>...................");
		return delMaqReclist;
	}
	// -------------------Maqs----->getPrevMaqData----------------------------------------------------------	
	@Transactional
	public List<Maqs>  getPrevMaqData( Maqs newMaqSumm) throws EmptyFieldException,EntityNotFoundException,EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->getPrevMaqData","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if (newMaqSumm==null) throw new EmptyFieldException("->MaqsService->getPrevMaqData->Null Object or Empty Mandatoy Parameters");
		// Read previous Maq summaries endDate smaller that startDate of current summary
		// and get the one with the end date more close to startDate of current summary 
		List<Maqs>  readMaqSummList = maqsRepository.findByMaqMaqBNameAndMaqSumDataTypeAndAndMaqEndDateLessThanEqual(newMaqSumm.getMaqMaqBName(),
														"SUMM",newMaqSumm.getMaqStartDate());
		if (readMaqSummList == null) return null;
		if (readMaqSummList.size() == 0) return null;
		Maqs selectedMaqSumm = readMaqSummList.get(0);
		for(int j = 1;j<readMaqSummList.size();j++) {
			if (readMaqSummList.get(j).getMaqEndDate().after(selectedMaqSumm.getMaqEndDate())) {
				selectedMaqSumm = readMaqSummList.get(j);
			}			
		}
		List<Maqs>  prevMaqSummList = maqsRepository.findByMaqMaqBNameAndMaqStartDateAndMaqEndDate(selectedMaqSumm.getMaqMaqBName(),
																	selectedMaqSumm.getMaqStartDate(),selectedMaqSumm.getMaqEndDate());

		consoleLog.logMsg(showLogMsg,"->MaqsService->getPrevMaqData","Exiting","..>>...................");
		return prevMaqSummList;
	}
	// -------------------Maqs----->getOneMaq----------------------------------------------------------	
	@Transactional
	public List<Maqs>  getOneMaq( Maqs newMaqSumm) throws EmptyFieldException,EntityNotFoundException,EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->getOneMaq","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if (newMaqSumm==null) throw new EmptyFieldException("->MaqsService->getOneMaq->Null Object or Empty Mandatoy Parameters");
		List<Maqs>  readMaqSummList = maqsRepository.findByMaqMaqBNameAndMaqStartDateAndMaqEndDate(newMaqSumm.getMaqMaqBName(),newMaqSumm.getMaqStartDate(),newMaqSumm.getMaqEndDate());
		consoleLog.logMsg(showLogMsg,"->MaqsService->getOneMaq","Exiting","..>>...................");
		return readMaqSummList;
	}
	// -------------------Maqs----->updateOneMaqSumm----------------------------------------------------------	
	@Transactional
	public List<Maqs> updateOneMaqSumm( List<Maqs> newMaqSummList) 
									throws EmptyFieldException,EntityNotFoundException,EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->MaqsService->updateOneMaqSumm","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newMaqSummList==null) || (newMaqSummList.size()==0)) 
									throw new EmptyFieldException("->MaqsService->updateOneMaqSumm->Null Object or Empty Mandatoy Parameters");
		List<Maqs> createdMaqSummList;
		createdMaqSummList = new ArrayList <Maqs>();
		Maqs createdMaqSumm;
		for(int j = 0;j<newMaqSummList.size();j++) {
			boolean attrOk = newMaqSummList.get(j).checkMaqAtributes();
			if (attrOk == false) throw new WrongParametersException("->MaqsService->updateOneMaqSumm->Null Object or Empty Mandatoy Parameters");
			createdMaqSumm = maqsRepository.save(newMaqSummList.get(j));
			createdMaqSummList.add(createdMaqSumm);
		}
		consoleLog.logMsg(showLogMsg,"->MaqsService->updateOneMaqSumm","Exiting","..>>...................");
		return createdMaqSummList;
	}
}