package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.fran.gestb.dbbarxx.model.Maqs;

public interface MaqsRepository extends JpaRepository<Maqs, Long> {
	List<Maqs> findAll();
	List<Maqs> findByMaqStartDateAndMaqEndDate(Date maqStartDate,Date maqEndDate);
	List<Maqs> findByMaqMaqBNameAndMaqStartDateAndMaqEndDate(String maqMaqBName,Date maqStartDate,Date maqEndDate);
	List<Maqs> findByMaqSumDataTypeAndMaqEndDateGreaterThanEqualAndMaqEndDateLessThanEqual(String maqSumDataType,Date maqStartDate,Date maqEndDate);
	List<Maqs> findByMaqMaqBNameAndMaqSumDataTypeAndAndMaqEndDateLessThanEqual(String maqMaqBName,String maqSumDataType,Date maqEndDate);

	long deleteByMaqMaqBNameAndMaqStartDateAndMaqEndDate(String maqMaqBName,Date maqStartDate,Date maqEndDate);
	long deleteByMaqEndDateGreaterThanEqualAndMaqEndDateLessThanEqual(Date maqStartDate,Date maqEndDate);

}
