package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.CardPayouts;

public interface CardPayoutsRepository extends JpaRepository<CardPayouts, Long> {
	List<CardPayouts> findAll();
	CardPayouts findByCardPayoutId(Long cardPayoutId);
	List<CardPayouts> findByCardPayoutDate(Date cardPayoutDate);
	List<CardPayouts> findByCardPayoutTpvId(Long cardPayoutTpvId);
	List<CardPayouts> findByCardPayoutTotalId(Long cardPayoutTotalId);
	List<CardPayouts> findByCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(Date StartHdDate,Date EndHdDate);
	List<CardPayouts> findByCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(Long cardPayoutTpvId,Date StartHdDate,Date EndHdDate);
	List<CardPayouts> findByCardPayoutTotalIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(Long cardPayoutTotalId,Date StartHdDate,Date EndHdDate);	
	List<CardPayouts> findByCardPayoutTotalIdAndCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(Long cardPayoutTotalId,Long cardPayoutTpvId,Date StartHdDate,Date EndHdDate);
	List<CardPayouts> findByCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqualAndCardPayoutTotalDateGreaterThan(Date StartHdDate,Date EndHdDate,Date cardPayoutTotalDate);
	List<CardPayouts> findByCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqualAndCardPayoutTotalDateGreaterThan(Long cardPayoutTpvId,Date StartHdDate,Date EndHdDate,Date cardPayoutTotalDate);
}
