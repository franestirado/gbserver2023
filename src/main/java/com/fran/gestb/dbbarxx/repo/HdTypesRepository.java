package com.fran.gestb.dbbarxx.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.HdTypes;

public interface HdTypesRepository extends JpaRepository<HdTypes, Long> {

	HdTypes findByHdTypeId(Long HdTypeId);
	List<HdTypes> findAll();
	HdTypes findByHdTypeName(String HdTypeName);

}

