package com.fran.gestb.dbbarxx.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.CardTpvs;

public interface CardTpvsRepository extends JpaRepository<CardTpvs, Long> {
		List<CardTpvs> findAll();
		CardTpvs findByCardTpvId(Long cardTpvId);
		CardTpvs findByCardTpvName(String cardTpvName);
		CardTpvs findByCardTpvType(String cardTpvType);
}
