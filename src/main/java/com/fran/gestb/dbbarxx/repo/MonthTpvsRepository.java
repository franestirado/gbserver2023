package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.MonthTpvs;

public interface MonthTpvsRepository extends JpaRepository<MonthTpvs, Long> {
		List<MonthTpvs> findAll();	
		MonthTpvs findByMonthTpvId(Long MonthTpvId);	
		MonthTpvs findByMonthTpvName(String MonthTpvName);
		List<MonthTpvs> findByMonthTpvDate(Date MonthTpvDate);
		List<MonthTpvs> findByMonthTpvDateAndMonthTpvType(Date MonthTpvDate, String MonthTpvType);
		List<MonthTpvs> findAllByMonthTpvTypeAndMonthTpvDateGreaterThanEqualAndMonthTpvDateLessThanEqual(String MonthTpvType,Date StartMonthTpvDate,Date EndMonthTpvDate);
		List<MonthTpvs> findAllByMonthTpvDateGreaterThanEqualAndMonthTpvDateLessThanEqual(Date StartMonthTpvDate,Date EndMonthTpvDate);
		long deleteAllByMonthTpvDateGreaterThanEqualAndMonthTpvDateLessThanEqual(Date StartMonthTpvDate,Date EndMonthTpvDate);
		long deleteAllByMonthTpvDate(Date MonthTpvDate);
}
