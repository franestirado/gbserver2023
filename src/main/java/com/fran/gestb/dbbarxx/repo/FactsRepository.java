package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.Facts;

public interface FactsRepository extends JpaRepository<Facts, Long> {

	Facts findByFactId(Long factId);
	List<Facts> findAll();
	List<Facts> findAllByFactType(String factType);
	List<Facts> findByFactProvId(Long factProvId);
	List<Facts> findByFactDate(Date factDate);
	List<Facts> findByFactDateAndFactPayType(Date factDate,String factPayType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqual(Date startHdDate,Date endHdDate);
	
	List<Facts> findByFactPayTypeAndFactDateGreaterThanEqualAndFactDateLessThanEqual(String factPayType,Date startHdDate,Date endHdDate);
	
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvName(Date startHdDate,Date endHdDate,String provName);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactType(Date startHdDate,Date endHdDate,String factType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactPayType(Date startHdDate,Date endHdDate,String factPayType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactNote(Date startHdDate,Date endHdDate,boolean factNote);
	
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactType(Date startHdDate,Date endHdDate,String provName,String factType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactPayType(Date startHdDate,Date endHdDate,String provName,String factPayType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactNote(Date startHdDate,Date endHdDate,String provName,boolean factNote);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactTypeAndFactPayType(Date startHdDate,Date endHdDate,String factType,String factPayType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactTypeAndFactNote(Date startHdDate,Date endHdDate,String factType,boolean factNote);

	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactTypeAndFactPayType(Date startHdDate,Date endHdDate,String provName,String factType,String factPayType);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactTypeAndFactNote(Date startHdDate,Date endHdDate,String provName,String factType,boolean factNote);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactPayTypeAndFactNote(Date startHdDate,Date endHdDate,String provName,String factPayType,boolean factNote);
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactTypeAndFactPayTypeAndFactNote(Date startHdDate,Date endHdDate,String factType,String factPayType,boolean factNote);
	
	List<Facts> findByFactDateGreaterThanEqualAndFactDateLessThanEqualAndFactProvNameAndFactTypeAndFactPayTypeAndFactNote(Date startHdDate,Date endHdDate,String provName,String factType,String factPayType,boolean factNote);

	
	int deleteByFactId(Long factId);
}

