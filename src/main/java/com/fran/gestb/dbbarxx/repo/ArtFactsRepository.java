package com.fran.gestb.dbbarxx.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.ArtFacts;

public interface ArtFactsRepository extends JpaRepository<ArtFacts, Long> {

	ArtFacts findByArtFactArtId(Long artFactArtId);
	List<ArtFacts> findByArtFactFactId(Long artFactFactId);
	List<ArtFacts> findAllByArtFactArtId(Long artFactArtId);
	List<ArtFacts> findAll();
	int deleteAllByArtFactFactId(Long artFactFactId);

}