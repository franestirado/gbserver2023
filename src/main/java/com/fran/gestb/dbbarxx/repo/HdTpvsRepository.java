package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.HdTpvs;

public interface HdTpvsRepository extends JpaRepository<HdTpvs, Long> {

	List<HdTpvs> findAll();	
	HdTpvs findByHdTpvId(Long HdTpvId);	
	HdTpvs findByHdTpvName(String HdTpvName);
	List<HdTpvs> findByHdTpvDate(Date HdTpvDate);
	List<HdTpvs> findByHdTpvDateAndHdTpvType(Date HdTpvDate, String HdTpvType);
	List<HdTpvs> findAllByHdTpvTypeAndHdTpvDateGreaterThanEqualAndHdTpvDateLessThanEqual(String HdTpvType,Date StartHdTpvDate,Date EndHdTpvDate);
	List<HdTpvs> findAllByHdTpvDateGreaterThanEqualAndHdTpvDateLessThanEqual(Date StartHdTpvDate,Date EndHdTpvDate);
	long deleteAllByHdTpvDate(Date HdTpvDate);

}
