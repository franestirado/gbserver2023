package com.fran.gestb.dbbarxx.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.dbbarxx.model.MonthTpvs;
import com.fran.gestb.dbbarxx.model.Months;
import com.fran.gestb.dbbarxx.service.MonthsService;

@RestController
@RequestMapping("/months")
public class MonthsController {
	
	@Autowired
	private MonthsService monthsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	// -------------------MONTHS---------------------------------------------------------------

	// -------------------Months----->getMonths------------------------------------------------
	@PostMapping(value = "/getMonths")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Months> getMonthSummRecords(@RequestBody List <Date> firstAndLastDayOfYear) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->getMonthSummRecords","Entering","..>>...................");
		List<Months> savedMonthsList = monthsService.getMonthSummRecords(firstAndLastDayOfYear);
		consoleLog.logMsg(showLogMsg,"->MonthsController->getMonthSummRecords","Exiting","...................>>..");
		return savedMonthsList;	
	}
	// -------------------Months----->getRetPrevMonths------------------------------------------------
	@PostMapping(value = "/getRetPrevMonths")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Months> getRetCountPrevMonthSummRecords(@RequestBody List <Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->getRetCountPrevMonthSummRecords","Entering","..>>...................");
		List<Months> savedMonthsList = monthsService.getRetCountPrevMonthSummRecords(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->MonthsController->getRetCountPrevMonthSummRecords","Exiting","...................>>..");
		return savedMonthsList;	
	}
	// -------------------Months----->getRetMonths------------------------------------------------
	@PostMapping(value = "/getRetMonths")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Months> getRetStartCountMonthSummRecords(@RequestBody List <Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->getRetStartCountMonthSummRecords","Entering","..>>...................");
		List<Months> savedMonthsList = monthsService.getRetStartCountMonthSummRecords(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->MonthsController->getRetStartCountMonthSummRecords","Exiting","...................>>..");
		return savedMonthsList;	
	}
	// -------------------Months----->createMonths------------------------------------------------
	@PostMapping(value = "/createMonths")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Months> createMonthsList(@RequestBody List<Months> newMonthsList) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->createMonthsList","Entering","..>>...................");
		List<Months> savedMonthsList = monthsService.createMonthsList(newMonthsList);
		consoleLog.logMsg(showLogMsg,"->MonthsController->createMonthsList","Exiting","...................>>..");
		return savedMonthsList;	
	}
	// -------------------Months----->updateMonths------------------------------------------------
	@PostMapping(value = "/updateMonths")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Months> updateMonthsList(@RequestBody List<Months> updatedMonthsList) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->updateMonthsList","Entering","..>>...................");
		List<Months> savedMonthsList = monthsService.updateMonthsList(updatedMonthsList);
		consoleLog.logMsg(showLogMsg,"->MonthsController->updateMonthsList","Exiting","...................>>..");
		return savedMonthsList;	
	}
	// -------------------Months----->delMonths------------------------------------------------
	@PostMapping(value = "/delMonths")
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteMonthsRecords(@RequestBody List <Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->deleteMonthsRecords","Entering","..>>...................");
		long deletedRecords = monthsService.deleteMonthsRecords(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->MonthsController->deleteMonthsRecords","Exiting","...................>>..");
		return deletedRecords;	
	}
	// -------------------MONTH TPVS---------------------------------------------------------------
	// -------------------Months TPVS---->createDateHds------------------------------------------------
	@PostMapping(value = "/createMonthsTpvs")
	@ResponseStatus(HttpStatus.CREATED)
	public List<MonthTpvs> createMonthsTpvsList(@RequestBody List<MonthTpvs> newMonthsTpvsList) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->createMonthsTpvsList","Entering","..>>...................");
		List<MonthTpvs> savedMonthsTpvsList = monthsService.createMonthsTpvsList(newMonthsTpvsList);
		consoleLog.logMsg(showLogMsg,"->MonthsController->createMonthsTpvsList","Exiting","...................>>..");
		return savedMonthsTpvsList;	
	}
	// -------------------Months TPVS----->getMonthTpvRecords------------------------------------------------
	@PostMapping(value = "/getMonthTpvs")
	@ResponseStatus(HttpStatus.CREATED)
	public List<MonthTpvs> getMonthTpvRecords(@RequestBody List <Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->MonthsController->getMonthTpvRecords","Entering","..>>...................");
		List<MonthTpvs> savedMonthTpvsList = monthsService.getMonthTpvRecords(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->MonthsController->getMonthTpvRecords","Exiting","...................>>..");
		return savedMonthTpvsList;	
	}
	
}
