package com.fran.gestb.dbbarxx.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.dbbarxx.model.CardPayouts;
import com.fran.gestb.dbbarxx.model.CardTotals;
import com.fran.gestb.dbbarxx.model.CardTpvs;
import com.fran.gestb.dbbarxx.service.CardsService;

@RestController
@RequestMapping("/cards")
public class CardsController {
	@Autowired
	private CardsService cardsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	// -------------------CardPayouts ------------------------------------------------------------------------------------
	// -------------------CardPayouts ----->getAllCardPayouts-------------------------------------------
	@PostMapping(value = "/getAllPayouts")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> getAllCardPayouts(@RequestBody List<ValueRecord> cardPayoutVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayouts","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.getAllCardPayouts(cardPayoutVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayouts","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->getPrevDatePayouts-------------------------------------------
	@PostMapping(value = "/getPrevDatePayouts")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> getPrevDateCardPayouts(@RequestBody List<ValueRecord> cardPayoutVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getPrevDateCardPayouts","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.getAllCardPayouts(cardPayoutVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->getPrevDateCardPayouts","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->createCardPayouts-------------------------------------------
	@PostMapping(value = "/createPayouts")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> createCardPayoutsList(@RequestBody List<CardPayouts> newCardPayoutsList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->createCardPayouts","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.createCardPayoutsList(newCardPayoutsList);
		consoleLog.logMsg(showLogMsg,"->cardsController->createCardPayouts","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->updateBeforeAfterCardPayoutsListTotalId-------------------------------------------
	@PostMapping(value = "/updPayoutsTotalId")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> updateBeforeAfterCardPayoutsListTotalId(@RequestBody List<CardPayouts> updateCardPayoutsList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->updateBeforeAfterCardPayoutsListTotalId","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.updateBeforeAfterCardPayoutsListTotalId(updateCardPayoutsList);
		consoleLog.logMsg(showLogMsg,"->cardsController->updateBeforeAfterCardPayoutsListTotalId","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->updateCardPayouts-------------------------------------------
	@PostMapping(value = "/updatePayouts")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> updateCardPayoutsList(@RequestBody List<CardPayouts> updateCardPayoutsList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->updateCardPayouts","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.createCardPayoutsList(updateCardPayoutsList);
		consoleLog.logMsg(showLogMsg,"->cardsController->updateCardPayouts","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->delOneCardPayout-------------------------------------------
	@PostMapping(value = "/delOnePayout")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneCardPayout(@RequestBody CardPayouts delCardPayout) {
		consoleLog.logMsg(showLogMsg,"->cardsController->delOneCardPayout","Entering","..>>...................");
		cardsService.delOneCardPayout(delCardPayout);
		consoleLog.logMsg(showLogMsg,"->cardsController->delOneCardPayout","Exiting","...................>>..");
		return; 
	}
	// -------------------CardPayouts ----->updateCardPayouts-------------------------------------------
	@PostMapping(value = "/delAllPayouts")
	@ResponseStatus(HttpStatus.CREATED)
	public void delAllPayoutLists(@RequestBody List<CardPayouts> delCardPayoutsList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->delAllPayoutLists","Entering","..>>...................");
		cardsService.delAllPayoutLists(delCardPayoutsList);
		consoleLog.logMsg(showLogMsg,"->cardsController->delAllPayoutLists","Exiting","...................>>..");
		return; 
	}
	// -------------------CardPayouts ----->getAllCards-------------------------------------------
	@GetMapping(value = "/getAll")
	@ResponseStatus(HttpStatus.OK)
	public List<CardPayouts> getAllCards() {
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCards","Entering","..>>...................");
		//List<Cards> CardsList = cardsService.getAllCards();
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCards","Exiting","...................>>..");
		return null; 
	}
	// -------------------CardPayouts ----->getAllCardPayoutsNotTotal-------------------------------------------
	@PostMapping(value = "/getPayoutsNoTot")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> getAllCardPayoutsNotTotal(@RequestBody List<ValueRecord> cardPayoutVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayoutsNotTotal","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.getAllCardPayoutsNotTotal(cardPayoutVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayoutsNotTotal","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->getAllCardPayoutsWithCardTotalId-------------------------------------------
	@PostMapping(value = "/getPayoutsTotalId")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> getAllCardPayoutsWithCardTotalId(@RequestBody List<ValueRecord> cardPayoutVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayoutsWithCardTotalId","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.getAllCardPayoutsWithCardTotalId(cardPayoutVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayoutsWithCardTotalId","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------CardPayouts ----->getCardPayoutsNoTotal-------------------------------------------
	@PostMapping(value = "/getPayoutsNoTotal")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardPayouts> getCardPayoutsNoTotal(@RequestBody List<ValueRecord> cardPayoutVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getCardPayoutsNoTotal","Entering","..>>...................");
		List<CardPayouts> readCardsList = cardsService.getCardPayoutsNoTotal(cardPayoutVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->getCardPayoutsNoTotal","Exiting","...................>>..");
		return readCardsList; 
	}
	// -------------------Cards Tpvs------------------------------------------------------------------------------------
	// -------------------Cards Tpvs----->getAllCardTpvs-------------------------------------------
	@GetMapping(value = "/getAllTpvs")
	@ResponseStatus(HttpStatus.OK)
	public List<CardTpvs> getAllCardTpvs() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardTpvs","Entering","..>>...................");
		List<CardTpvs> CardTpvsList = cardsService.getAllCardTpvs();
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardTpvs","Exiting","...................>>..");
		return CardTpvsList; 
	}
	// -------------------Cards Tpvs----->createOneCardTpv------------------------------------------------
	@PostMapping(value = "/createOneTpv")
	@ResponseStatus(HttpStatus.CREATED)
	public CardTpvs createOneCardTpv(@RequestBody CardTpvs newCardTpv) {
		consoleLog.logMsg(showLogMsg,"->cardsController->createOneCardTpv","Entering","..>>...................");
		CardTpvs createdCardTpv = cardsService.createOneCardTpv(newCardTpv);
		consoleLog.logMsg(showLogMsg,"->cardsController->createOneCardTpv","Exiting","...................>>..");
		return createdCardTpv;	
	}
	// -------------------Cards Tpvs----->getOneCardTpv------------------------------------------------
	@PostMapping(value="/getOneTpv")
	@ResponseStatus(HttpStatus.OK)
	public CardTpvs getOneCardTpv(@RequestBody CardTpvs updCardTpv) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->getOneCardTpv","Entering","..>>...................");
		CardTpvs checkCardTpv = cardsService.getOneCardTpv(updCardTpv);
		consoleLog.logMsg(showLogMsg,"->ArtsController->getOneCardTpv","Exiting","...................>>..");
		return checkCardTpv;	
	}
	// -------------------Cards Tpvs----->updateOneCardTpv------------------------------------------------
	@PostMapping(value="/updOneTpv")
	@ResponseStatus(HttpStatus.OK)
	public CardTpvs updateOneCardTpv(@RequestBody CardTpvs updCardTpv) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateOneCardTpv","Entering","..>>...................");
		CardTpvs checkCardTpv = cardsService.updateOneCardTpv(updCardTpv);
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateOneCardTpv","Exiting","...................>>..");
		return checkCardTpv;	
	}
	// ------------------- Cards Tpvs----->DeleteOneCardTpv------------------------------------------------
	@PostMapping(value="/delOneTpv")
	@ResponseStatus(HttpStatus.CREATED)
	public void DeleteOneCardTpv(@RequestBody CardTpvs delCardTpv) {
		consoleLog.logMsg(showLogMsg,"->cardsController->DeleteOneCardTpv","Entering","..>>...................");
		cardsService.DeleteOneCardTpv(delCardTpv);
		consoleLog.logMsg(showLogMsg,"->cardsController->DeleteOneCardTpv","Exiting","...................>>..");
		return;
	}
	// ------------------- Cards Tpvs----->DeleteAllCardTpvs------------------------------------------------
	@PostMapping(value="/delAllTpvs")
	@ResponseStatus(HttpStatus.CREATED)
	public void DeleteAllCardTpvs(@RequestBody CardTpvs delCardTpv) {
		consoleLog.logMsg(showLogMsg,"->cardsController->DeleteAllCardTpvs","Entering","..>>...................");
		cardsService.DeleteAllCardTpvs(delCardTpv);
		consoleLog.logMsg(showLogMsg,"->cardsController->DeleteAllCardTpvs","Exiting","...................>>..");
		return;
	}
	// -------------------Card Totals ------------------------------------------------------------------------------------
	// -------------------Card Totals ----->getAllCardsTotals-------------------------------------------
	@PostMapping(value = "/getAllTotals")
	@ResponseStatus(HttpStatus.CREATED)
	public List<CardTotals> getAllCardsTotals(@RequestBody List<ValueRecord> cardTotalVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardsTotals","Entering","..>>...................");
		List<CardTotals> readCardTotalsList = cardsService.getAllCardsTotals(cardTotalVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardsTotals","Exiting","...................>>..");
		return readCardTotalsList; 
	}
	// -------------------Cards Totals----->updateOneCardTotal------------------------------------------------
	@PostMapping(value = "/updateOneTotal")
	@ResponseStatus(HttpStatus.CREATED)
	public CardTotals updateOneCardTotal(@RequestBody CardTotals updCardTotal) {
		consoleLog.logMsg(showLogMsg,"->cardsController->updateOneCardTotal","Entering","..>>...................");
		CardTotals updatedCardTotal = cardsService.updateOneCardTotal(updCardTotal);
		consoleLog.logMsg(showLogMsg,"->cardsController->updateOneCardTotal","Exiting","...................>>..");
		return updatedCardTotal;	
	}
	// -------------------Cards Totals----->createOneCardTotal------------------------------------------------
	@PostMapping(value = "/createOneTotal")
	@ResponseStatus(HttpStatus.CREATED)
	public CardTotals createOneCardTotal(@RequestBody CardTotals newCardTotal) {
		consoleLog.logMsg(showLogMsg,"->cardsController->createOneCardTotal","Entering","..>>...................");
		CardTotals createdCardTotal = cardsService.createOneCardTotal(newCardTotal);
		consoleLog.logMsg(showLogMsg,"->cardsController->createOneCardTotal","Exiting","...................>>..");
		return createdCardTotal;	
	}
	// -------------------Cards Totals----->getOneCardTotal------------------------------------------------
	@PostMapping(value = "/getOneTotal")
	@ResponseStatus(HttpStatus.CREATED)
	public CardTotals getOneCardTotal(@RequestBody CardTotals updCardTotal) {
		consoleLog.logMsg(showLogMsg,"->cardsController->getOneCardTotal","Entering","..>>...................");
		CardTotals updatedCardTotal = cardsService.getOneCardTotal(updCardTotal);
		consoleLog.logMsg(showLogMsg,"->cardsController->getOneCardTotal","Exiting","...................>>..");
		return updatedCardTotal;	
	}
	// ------------------- Cards Totals----->deleteOneCardTotal------------------------------------------------
	@PostMapping(value="/delOneTotal")
	@ResponseStatus(HttpStatus.CREATED)
	public void deleteOneCardTotal(@RequestBody CardTotals delCardTotal) {
		consoleLog.logMsg(showLogMsg,"->cardsController->deleteOneCardTotal","Entering","..>>...................");
		cardsService.deleteOneCardTotal(delCardTotal);
		consoleLog.logMsg(showLogMsg,"->cardsController->deleteOneCardTotal","Exiting","...................>>..");
		return;
	}
	// ------------------- Cards Totals----->deleteAllCardsTotals------------------------------------------------
	@PostMapping(value="/delAllTotals")
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteAllCardsTotals(@RequestBody List<ValueRecord> delCardTpvVRList) {
		consoleLog.logMsg(showLogMsg,"->cardsController->deleteAllCardsTotals","Entering","..>>...................");
		long delCardsTotals = cardsService.deleteAllCardsTotals(delCardTpvVRList);
		consoleLog.logMsg(showLogMsg,"->cardsController->deleteAllCardsTotals","Exiting","...................>>..");
		return delCardsTotals;
	}
}
