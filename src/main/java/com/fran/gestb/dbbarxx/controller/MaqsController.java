package com.fran.gestb.dbbarxx.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.dbbarxx.model.Hds;
import com.fran.gestb.dbbarxx.model.Maqs;
import com.fran.gestb.dbbarxx.service.MaqsService;

@RestController
@RequestMapping("/maqs")
public class MaqsController {
	@Autowired
	private MaqsService maqsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	// -------------------Maqs ------------------------------------------------------------------------------------
	// -------------------Maqs ----->getMaqsLits-------------------------------------------
	@PostMapping(value = "/getAll")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Maqs> getMaqsLits(@RequestBody List<Date> firstAndLastDayOfYear) {
		consoleLog.logMsg(showLogMsg,"->maqsController->getMaqsLits","Entering","..>>...................");
		List<Maqs> readMaqsList = maqsService.getMaqsLits(firstAndLastDayOfYear);
		consoleLog.logMsg(showLogMsg,"->maqsController->getMaqsLits","Exiting","...................>>..");
		return readMaqsList; 
	}
	// -------------------Maqs ----->checkIfMaqsIsInDB-------------------------------------------
	@PostMapping(value = "/check")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Maqs>  checkIfMaqsIsInDB(@RequestBody List<ValueRecord> dateRangeMaqBNameVRList) {
		consoleLog.logMsg(showLogMsg,"->maqsController->checkIfMaqsIsInDB","Entering","..>>...................");
		List<Maqs> readMaqsList = maqsService.checkIfMaqsIsInDB(dateRangeMaqBNameVRList);
		consoleLog.logMsg(showLogMsg,"->maqsController->checkIfMaqsIsInDB","Exiting","...................>>..");
		return readMaqsList; 
	}
	// -------------------Maqs ----->getHdsMaqs-------------------------------------------
	@PostMapping(value = "/hdsMaq")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Hds>  getHdsMaqs(@RequestBody List<ValueRecord> dateRangeMaqBNameVRList) {
		consoleLog.logMsg(showLogMsg,"->maqsController->getHdsMaqs","Entering","..>>...................");
		List<Hds> readHdsMaqReclist = maqsService.getHdsMaqs(dateRangeMaqBNameVRList);
		consoleLog.logMsg(showLogMsg,"->maqsController->getHdsMaqs","Exiting","...................>>..");
		return readHdsMaqReclist; 
	}
	// -------------------Maqs ----->createOneMaq-------------------------------------------
	@PostMapping(value = "/creatOne")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Maqs> createOneMaq(@RequestBody List<Maqs> newMaqSummList) {
		consoleLog.logMsg(showLogMsg,"->maqsController->createOneMaq","Entering","..>>...................");
		List<Maqs> readMaqSummList = maqsService.createOneMaq(newMaqSummList);
		consoleLog.logMsg(showLogMsg,"->maqsController->createOneMaq","Exiting","...................>>..");
		return readMaqSummList; 
	}
	// -------------------Maqs ----->updateOneMaq-------------------------------------------
	@PutMapping(value = "/updOne")
	@ResponseStatus(HttpStatus.CREATED)
	public Maqs updateOneMaq(@RequestBody Maqs updMaqSumm) {
		consoleLog.logMsg(showLogMsg,"->maqsController->updateOneMaq","Entering","..>>...................");
		Maqs readMaqSumm = maqsService.updateOneMaq(updMaqSumm);
		consoleLog.logMsg(showLogMsg,"->maqsController->updateOneMaq","Exiting","...................>>..");
		return readMaqSumm; 
	}
	// -------------------Maqs ----->deleteOneMaq-------------------------------------------
	@PostMapping(value = "/delOne")
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteOneMaq(@RequestBody Maqs delMaqSumm) {
		consoleLog.logMsg(showLogMsg,"->maqsController->deleteOneMaq","Entering","..>>...................");
		long deletedMaqSumm = maqsService.deleteOneMaq(delMaqSumm);
		consoleLog.logMsg(showLogMsg,"->maqsController->deleteOneMaq","Exiting","...................>>..");
		return deletedMaqSumm; 
	}
	// -------------------Maqs ----->deleteAllMaqs-------------------------------------------
	@PostMapping(value = "/delAll")
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteAllMaqs(@RequestBody List<Date> firstAndLastDayOfYear) {
		consoleLog.logMsg(showLogMsg,"->maqsController->deleteAllMaqs","Entering","..>>...................");
		long delMaqsList = maqsService.deleteAllMaqs(firstAndLastDayOfYear);
		consoleLog.logMsg(showLogMsg,"->maqsController->deleteAllMaqs","Exiting","...................>>..");
		return delMaqsList; 
	}
	// -------------------Maqs ----->getPrevMaqData-------------------------------------------
	@PostMapping(value = "/getPrev")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Maqs> getPrevMaqData(@RequestBody Maqs newMaqSumm) {
		consoleLog.logMsg(showLogMsg,"->maqsController->getPrevMaqData","Entering","..>>...................");
		List<Maqs> readMaqSummList = maqsService.getPrevMaqData(newMaqSumm);
		consoleLog.logMsg(showLogMsg,"->maqsController->getPrevMaqData","Exiting","...................>>..");
		return readMaqSummList; 
	}
	// -------------------Maqs ----->getOneMaq-------------------------------------------
	@PostMapping(value = "/getOne")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Maqs> getOneMaq(@RequestBody Maqs newMaqSumm) {
		consoleLog.logMsg(showLogMsg,"->maqsController->getOneMaq","Entering","..>>...................");
		List<Maqs> readMaqSummList = maqsService.getOneMaq(newMaqSumm);
		consoleLog.logMsg(showLogMsg,"->maqsController->getOneMaq","Exiting","...................>>..");
		return readMaqSummList; 
	}
	// -------------------Maqs ----->updateOneMaqSumm-------------------------------------------
	@PostMapping(value = "/updOneSumm")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Maqs> updateOneMaqSumm(@RequestBody List<Maqs> newMaqSummList) {
		consoleLog.logMsg(showLogMsg,"->maqsController->updateOneMaqSumm","Entering","..>>...................");
		List<Maqs> readMaqSummList = maqsService.updateOneMaqSumm(newMaqSummList);
		consoleLog.logMsg(showLogMsg,"->maqsController->updateOneMaqSumm","Exiting","...................>>..");
		return readMaqSummList; 
	}
}