package com.fran.gestb.dbbarxx.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarxx.model.ArtFacts;
import com.fran.gestb.dbbarxx.model.Facts;
import com.fran.gestb.dbbarxx.service.FactsService;

@RestController
@RequestMapping("/facts")
public class FactsController {

	@Autowired
	private FactsService factsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	//----------------------------- Facts------------------------------------------------
	//-------------------Facts----->getAllFacts-------------------------------------------
	@GetMapping(value = "/getAll")
	@ResponseStatus(HttpStatus.OK)
	public List<Facts> getAllFacts() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFacts","Entering","..>>...................");
		List<Facts> factsList = factsService.findAllFacts();
		if (factsList == null) throw new EmptyFieldException("No allFacts found");
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFacts","Exiting","...................>>..");
		return factsList; 
	}
	// -------------------Facts----->getAllFactsTypeMonth-------------------------------------------
	@PostMapping(value = "/getAllTypeMonth")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Facts> getAllFactsTypeMonth(@RequestBody List<ValueRecord> factsTypeVRList) throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsTypeMonth","Entering","..>>...................");
		List<Facts> factsList = factsService.getAllFactsTypeMonth(factsTypeVRList);		
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsTypeMonth","Exiting","...................>>..");
		return factsList; 
	}
	
	
	
	// -------------------Facts----->getAllFactsFilter-------------------------------------------
	@PostMapping(value = "/getAllFilter")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Facts> getAllFactsFilter(@RequestBody List<ValueRecord> factsFilterVRList) throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsFilter","Entering","..>>...................");
		List<Facts> factsList = factsService.getAllFactsFilter(factsFilterVRList);		
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsFilter","Exiting","...................>>..");
		return factsList; 
	}
	// -------------------Facts----->getAllFactsMonth-------------------------------------------
	@PostMapping(value = "/getAllMonth")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Facts> getAllFactsMonth(@RequestBody List <Date> firstAndLastDayOfMonth) throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsMonth","Entering","..>>...................");
		List<Facts> factsList = factsService.getAllFactsMonth(firstAndLastDayOfMonth);		
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsMonth","Exiting","...................>>..");
		return factsList; 
	}
	// -------------------Facts----->createOneFact------------------------------------------------
	@PostMapping(value = "/createOne")
	@ResponseStatus(HttpStatus.CREATED)
	public Facts createOneFact(@RequestBody Facts newFact) {
		consoleLog.logMsg(showLogMsg,"->FactsController->createOneFact","Entering","..>>...................");
		Facts checkFact = factsService.saveFact(newFact);
		consoleLog.logMsg(showLogMsg,"->FactsController->createOneFact","Exiting","...................>>..");
		return checkFact;	
	}
	// -------------------Facts----->getOneFact------------------------------------------------
	@PostMapping(value="/getOne")
	@ResponseStatus(HttpStatus.CREATED)
	public Facts getOneFact(@RequestBody Facts modFact) {
		consoleLog.logMsg(showLogMsg,"->FactsController->getOneFact","Entering","..>>...................");
		factsService.getOneFact(modFact);
		consoleLog.logMsg(showLogMsg,"->FactsController->getOneFact","Exiting","...................>>..");
		return modFact;
	}
	// -------------------Facts----->getOneFactById------------------------------------------------
	@PostMapping(value="/getOneById")
	@ResponseStatus(HttpStatus.CREATED)
	public Facts getOneFactById(@RequestBody long factId) {
		consoleLog.logMsg(showLogMsg,"->FactsController->getOneFactById","Entering","..>>...................");
		Facts readFact = factsService.getOneFactById(factId);
		consoleLog.logMsg(showLogMsg,"->FactsController->getOneFactById","Exiting","...................>>..");
		return readFact;
	}
	// -------------------Facts----->updateOneFact------------------------------------------------
	@PutMapping(value = "/updOne")
	@ResponseStatus(HttpStatus.OK)
	public Facts updateOneFact(@RequestBody Facts updFact) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->updateOneFact","Entering","..>>...................");
		// Check mandatory parameters in newField
		if ((updFact.getFactNumber()=="") | (updFact==null)) throw new EmptyFieldException("Wrong Field parameters");	
		Facts checkFact = factsService.updateOneFact(updFact);
		consoleLog.logMsg(showLogMsg,"->FactsController->updateOneFact","Exiting","...................>>..");
		return checkFact;		
	}	
	// -------------------Facts----->delOneFact------------------------------------------------
	@PostMapping(value="/delOne")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneFact(@RequestBody Facts delFact) {
		consoleLog.logMsg(showLogMsg,"->FactsController->delOneFact","Entering","..>>...................");
		factsService.deleteOneFact(delFact);
		consoleLog.logMsg(showLogMsg,"->FactsController->delOneFact","Exiting","...................>>..");
		return;
	}
	// -------------------Facts----->deleteOneFactById------------------------------------------------
	@PostMapping(value="/delOneById")
	@ResponseStatus(HttpStatus.CREATED)
	public void deleteOneFactById(@RequestBody long delFactId) {
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteOneFactById","Entering","..>>...................");
		factsService.deleteOneFactById(delFactId);
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteOneFactById","Exiting","...................>>..");
		return;
	}
	// -------------------Facts----->delAllFacts------------------------------------------------
	@DeleteMapping()
	public void delAllFacts() {
		consoleLog.logMsg(showLogMsg,"->FactsController->delAllFacts","Entering","..>>...................");
		factsService.deleteAllFacts();
		consoleLog.logMsg(showLogMsg,"->FactsController->delAllFacts","Exiting","...................>>..");
		return;
	}
	// -------------------Facts----->getAllFactsHd-------------------------------------------
	@PostMapping(value = "/getAllHd")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Facts> getAllFactsHd(@RequestBody Date workingDate) throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsHd","Entering","..>>...................");
		List<Facts> factsList = factsService.getAllFactsHd(workingDate);		
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactsHd","Exiting","...................>>..");
		return factsList; 
	}
	// -------------------Facts----->deleteAllHd-------------------------------------------
	@PostMapping(value = "/deleteAllHd")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Facts> deleteAllFactsHd(@RequestBody Date workingDate) throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteAllFactsHd","Entering","..>>...................");
		List<Facts> factsList = factsService.deleteAllFactsDatePayType(workingDate,"HojaDia");		
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteAllFactsHd","Exiting","...................>>..");
		return factsList; 
	}

	// -------------------Arts Facts------------------------------------------------
	// -------------------Arts Facts----->createArtsFact------------------------------------------------
	@PostMapping(value = "/createArtsFact")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtFacts> createArtsFact(@RequestBody List<ArtFacts> newArtsFactList) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->createArtsFact","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (newArtsFactList==null) throw new EmptyFieldException("Wrong Field parameters");
		List<ArtFacts> readArtsFactList = factsService.createArtsFact(newArtsFactList);
		consoleLog.logMsg(showLogMsg,"->FactsController->createArtsFact","Exiting","...................>>..");
		return readArtsFactList;		
	}
	// -------------------Arts Facts----->getArtsFact------------------------------------------------
	@PostMapping(value = "/getArtsFact")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtFacts> getArtsFact(@RequestBody Facts updateFact) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtsFact","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (updateFact==null) throw new EmptyFieldException("Wrong Field parameters");
		List<ArtFacts> readArtsFactList  = factsService.getArtsFact(updateFact);
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtsFact","Exiting","...................>>..");
		return readArtsFactList;		
	}	
	// -------------------Arts Facts----->getArtsFactByFactId------------------------------------------------
	@PostMapping(value = "/getArtsFactByFactId")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtFacts> getArtsFactByFactId(@RequestBody long factId) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtsFactByFactId","Entering","..>>...................");
		// Check mandatory parameters in newField
		List<ArtFacts> readArtsFactList  = factsService.getArtsFactByFactId(factId);
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtsFactByFactId","Exiting","...................>>..");
		return readArtsFactList;		
	}	
	// -------------------Arts Facts----->getArtFactsWithArt------------------------------------------------
	@PostMapping(value = "/getArtsFactWithArt")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtFacts> getArtFactsWithArt(@RequestBody Arts selArt) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtFactsWithArt","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (selArt==null) throw new EmptyFieldException("Wrong Field parameters");
		List<ArtFacts> readArtsFactList  = factsService.getArtFactsWithArt(selArt);
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtFactsWithArt","Exiting","...................>>..");
		return readArtsFactList;		
	}
	// -------------------Arts Facts----->updateArtsFact------------------------------------------------
	@PostMapping(value = "/updateArtsFact")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtFacts> updateArtsFact(@RequestBody List<ArtFacts> updateArtsFactList) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->updateArtsFactList","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (updateArtsFactList==null) throw new EmptyFieldException("Wrong Field parameters");
		List<ArtFacts> readArtsFactList = factsService.updateArtsFact(updateArtsFactList);
		consoleLog.logMsg(showLogMsg,"->FactsController->updateArtsFactList","Exiting","...................>>..");
		return readArtsFactList;		
	}
	// -------------------Arts Facts----->deleteArtsFact------------------------------------------------
	@PostMapping(value = "/deleteArtsFact")
	@ResponseStatus(HttpStatus.OK)
	public int deleteArtsFact(@RequestBody List<ArtFacts> deleteArtsFactList) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteArtsFact","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (deleteArtsFactList==null) throw new EmptyFieldException("Wrong Field parameters");
		int deleteArtsFact = factsService.deleteArtsFact(deleteArtsFactList);
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteArtsFact","Exiting","...................>>..");
		return deleteArtsFact;		
	}
	// -------------------Arts Facts----->deleteAllArtsFact------------------------------------------------
	@PostMapping(value = "/deleteAllArtsFact")
	@ResponseStatus(HttpStatus.OK)
	public int deleteAllArtsFact(@RequestBody int deleteNumber) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteAllArtsFact","Entering","..>>...................");
		// Check mandatory parameters in newField		
		int deleteArtsFact = factsService.deleteAllArtsFact();
		consoleLog.logMsg(showLogMsg,"->FactsController->deleteAllArtsFact","Exiting","...................>>..");
		return deleteArtsFact;		
	}
}
