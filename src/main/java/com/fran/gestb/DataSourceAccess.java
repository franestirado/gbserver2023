package com.fran.gestb;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "accessEntityManagerFactory", transactionManagerRef = "accessTransactionManager", 
basePackages = { "com.fran.gestb.access.repo" })
public class DataSourceAccess {

	@Autowired
	private Environment env;
	
	@Bean(name = "accessDataSource")
	public DataSource accessDataSource() {
    	//System.out.printf("--->DataSourceAccess-------->accessDataSource-------->\n");    	

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(env.getProperty("access.datasource.url"));
		dataSource.setUsername(env.getProperty("access.datasource.username"));
		dataSource.setPassword(env.getProperty("access.datasource.password"));
		dataSource.setDriverClassName(env.getProperty("access.datasource.driver-class-name"));		
		return dataSource;
	}	

	//@Primary
	@Bean(name = "accessEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean accessEntityManagerFactory() {
    	//System.out.printf("--->DataSourceAccess-------->accessEntityManagerFactory-------->\n");    	

		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(accessDataSource());
		em.setPackagesToScan("com.fran.gestb.access.model");
		
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", env.getProperty("access.jpa.hibernate.ddl-auto"));
        properties.put("hibernate.physical_naming_strategy", env.getProperty("access.jpa.properties.hibernate.physical_naming_strategy"));
        properties.put("hibernate.id.db_structure_naming_strategy", env.getProperty("access.jpa.properties.hibernate.id.db_structure_naming_strategy"));
        //properties.put("hibernate.show-sql", env.getProperty("access.jpa.show-sql"));
		//properties.put("hibernate.dialect", env.getProperty("access.jpa.database-platform"));
		
		em.setJpaPropertyMap(properties);
		
		return em;
		
	}
	
	//@Primary
	@Bean(name = "accessTransactionManager")
	public PlatformTransactionManager accessTransactionManager() {
    	//System.out.printf("--->DataSourceAccess-------->accessTransactionManager-------->\n");    	

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(accessEntityManagerFactory().getObject());
		
		return transactionManager;
	}
}