package com.fran.gestb.dbbarc.service;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.UserDoesNotExistException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarc.model.ArtsFactModels;
import com.fran.gestb.dbbarc.model.FactModels;
import com.fran.gestb.dbbarc.model.Provs;
import com.fran.gestb.dbbarc.repo.ArtsRepository;
import com.fran.gestb.dbbarc.repo.ArtsFactModelsRepository;

import com.fran.gestb.dbbarc.repo.FactModelsRepository;
import com.fran.gestb.dbbarc.repo.ProvsRepository;
import com.fran.gestb.dbbarxx.model.ArtFacts;
import com.fran.gestb.dbbarxx.model.Facts;
import com.fran.gestb.dbbarxx.repo.ArtFactsRepository;
import com.fran.gestb.dbbarxx.repo.FactsRepository;

@Service
public class FactModelsService {
	@Autowired
	private FactModelsRepository factModelsRepository;
	@Autowired
	private ArtsFactModelsRepository artsFactModelsRepository;
	@Autowired
	private ArtsRepository artsRepository;
	@Autowired
	private ProvsRepository provsRepository;
	@Autowired
	private FactsRepository factsRepository;
	@Autowired
	private ArtFactsRepository artFactsRepository;
	@Autowired
	private FieldsService fieldsService;

	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	// -------------------Model Facts------------------------------------------------
	// -------------------Model Facts----->findAllFacts-------------------------------------------	
	@Transactional(readOnly = true)
	public List<FactModels> getAllFactModels() {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getAllFactModels","Entering","..>>...................");
		List<FactModels> modelFactsList = factModelsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getAllFactModels","Exiting","...................>>..");
		return modelFactsList;
	}
	// -------------------Model Facts----->getFactModelsProv-------------------------------------------	
	@Transactional(readOnly = true)
	public List<FactModels> getFactModelsProv(Provs selectedProv) {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getFactModelsProv","Entering","..>>...................");
		List<FactModels> modelFactsList = factModelsRepository.findByFactModelsProvId(selectedProv.getProvId());
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getFactModelsProv","Exiting","...................>>..");
		return modelFactsList;
	}
	// -------------------Model Facts----->saveModelFact------------------------------------------------
	@Transactional
	public FactModels createOneFactModel(FactModels newModelFact) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->createOneFactModel","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newModelFact==null) | (newModelFact.getFactModelsName()=="")) throw 
			new EmptyFieldException("->FactModelsService->createOneFactModel->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newModelFact.checkFactModelsAtributes()==false) throw 
			new WrongParametersException("--Fact Model Name-->"+newModelFact.getFactModelsName()+"--Fact Model Prov Name-->"+newModelFact.getFactModelsProvName());
		/* Checking there no other object with same name */	
		FactModels checkModelFact = factModelsRepository.findByFactModelsName(newModelFact.getFactModelsName());
		if (checkModelFact != null) throw 
			new EntityAlreadyExistsException("--Fact Model Name-->"+newModelFact.getFactModelsName()+"--Fact Model Prov Name-->"+newModelFact.getFactModelsProvName());
		FactModels readModelFact = factModelsRepository.save(newModelFact);
		consoleLog.logMsg(showLogMsg,"->FactModelsService->createOneFactModel","Exiting","...................>>..");
		return readModelFact;
	}
	// -------------------Model Facts----->createProvFactModel------------------------------------------------
	@Transactional
	public FactModels createProvFactModel(String provNameFactModel) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->createProvFactModel","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((provNameFactModel==null) | (provNameFactModel=="")) throw new EmptyFieldException("->FactModelsService->createProvFactModel->Null Object or Empty Mandatoy Parameters");
		// Create new Fact model with prov data of the received selFactModel
		String selProvName = provNameFactModel;
		Provs selProv = provsRepository.findByProvName(selProvName);
		if (selProv==null) throw new EmptyFieldException("->FactModelsService->createProvFactModel->selProv->Null Object or Empty Mandatoy Parameters");			
		FactModels newFactModel = new FactModels();
		Date currentDate = new Date();
		newFactModel.setFactModelsDate(currentDate);
		newFactModel.setFactModelsProvName(selProv.getProvName());
		newFactModel.setFactModelsProvId(selProv.getProvId());
		newFactModel.setFactModelsFactType(selProv.getProvFactType());
		newFactModel.setFactModelsPayType(selProv.getProvPayType());
		newFactModel.setFactModelsNote(selProv.isProvFactNote());
		// Get name of current bar
		String currentBarName = fieldsService.getCurrentBarString();
		if (currentBarName==null) throw new EmptyFieldException("->FactModelsService->createProvFactModel->currentBarName->Null Object or Empty Mandatoy Parameters");			
		// Checking there no other fact model with same name	
		FactModels checkModelFact = null;
		long index = 0;
		do {
			newFactModel.setFactModelsName(currentBarName+"..Aut."+index+".."+selProv.getProvName());
			checkModelFact = factModelsRepository.findByFactModelsName(newFactModel.getFactModelsName());
			index = index + 1;
		} while (checkModelFact !=null);
		// Cheking all attributes are right
		if (newFactModel.checkFactModelsAtributes()==false) throw new 
			WrongParametersException("--Fact Model Name-->"+newFactModel.getFactModelsName()+"--Fact Model Prov Name-->"+newFactModel.getFactModelsProvName());
		// Saving FactModel data
		FactModels savedFactModel = factModelsRepository.save(newFactModel);
		// Retrieving all arts in all facts of this Prov,
		// First get all facts with this prov id
		List <Facts> selProvFactsList = factsRepository.findByFactProvId(selProv.getProvId());
		if ( (selProvFactsList == null) || (selProvFactsList.size() == 0) ) return savedFactModel;
		// Then get all arts in all these facts
		List <ArtFacts> selProvArtsFactsList = artFactsRepository.findByArtFactFactId(selProvFactsList.get(0).getFactId());
		if ( (selProvArtsFactsList == null) || (selProvArtsFactsList.size() == 0) ) return savedFactModel;
		List <ArtFacts> filteredProvArtsFactsList;
		filteredProvArtsFactsList = new  ArrayList<ArtFacts>();
		if (selProvArtsFactsList.size() == 1) {
			filteredProvArtsFactsList.addAll(selProvArtsFactsList);
		} else {
			List <ArtFacts> readProvArtsFactsList;
			for(int i = 1;i<selProvFactsList.size();i++) {
				readProvArtsFactsList = artFactsRepository.findByArtFactFactId(selProvFactsList.get(i).getFactId());
				selProvArtsFactsList.addAll(readProvArtsFactsList);								
			}
			// Sort ArtsFact first by artName and then by factDate
			Collections.sort(selProvArtsFactsList,Comparator.comparing(ArtFacts::getArtFactArtName).
	                thenComparing(ArtFacts::getArtFactFactDate));
			// and finally eliminate duplicated
			
			int indexLast = 0;
			for(int i = 1;i<selProvArtsFactsList.size();i++) {
				indexLast = i;
				if (!selProvArtsFactsList.get(i-1).getArtFactArtName().equals(selProvArtsFactsList.get(i).getArtFactArtName()))
					filteredProvArtsFactsList.add(selProvArtsFactsList.get(i-1));
			}
			filteredProvArtsFactsList.add(selProvArtsFactsList.get(indexLast));
		}
		// Create ArtsFactModels
		for(int i = 0;i<filteredProvArtsFactsList.size();i++) {
			ArtsFactModels createdArtsFactModel = new ArtsFactModels();
			createdArtsFactModel.setArtId(filteredProvArtsFactsList.get(i).getArtFactArtId());
			createdArtsFactModel.setArtName(filteredProvArtsFactsList.get(i).getArtFactArtName());
			createdArtsFactModel.setFactModelsId(savedFactModel.getFactModelsId()); 
			createdArtsFactModel.setFactModelsName(savedFactModel.getFactModelsName());
			ArtsFactModels savedArtsFactModel = artsFactModelsRepository.save(createdArtsFactModel);
		}
		// update Art with latest ArfFact info
		for(int i = 0;i<filteredProvArtsFactsList.size();i++) {
			Arts savedArt;
			Arts readArt = artsRepository.findByArtId(filteredProvArtsFactsList.get(i).getArtFactArtId());
			readArt.setArtMeasurement(filteredProvArtsFactsList.get(i).getArtFactArtMeasurement());
			readArt.setArtPackagePrice(filteredProvArtsFactsList.get(i).getArtFactArtPackagePrice());
			readArt.setArtPackageUnits(filteredProvArtsFactsList.get(i).getArtFactArtPackageUnits());
			readArt.setArtSize(filteredProvArtsFactsList.get(i).getArtFactArtSize());
			readArt.setArtUnitPrice(filteredProvArtsFactsList.get(i).getArtFactArtUnitPrice());
			//readArt.setArtType(filteredProvArtsFactsList.get(i).getArtFactArtType());
			//readArt.setArtTax(filteredProvArtsFactsList.get(i).getArtFactArtTax());
			if (readArt.checkArtAtributes()==true) {
				savedArt = artsRepository.save(readArt);
			}
		}					
		consoleLog.logMsg(showLogMsg,"->FactModelsService->createProvFactModel","Exiting","...................>>..");
		return savedFactModel;
	}
	// -------------------Model Facts----->getOneFactModel------------------------------------------------
	@Transactional
	public FactModels getOneFactModel(FactModels modModelFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getOneFactModel","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modModelFact==null) | (modModelFact.getFactModelsName()=="")) throw new EmptyFieldException("->FactModelsService->getOneFactModel->Null Object or Empty Mandatoy Parameters");	
		FactModels readModelFact = factModelsRepository.findByFactModelsId(modModelFact.getFactModelsId());
		if (readModelFact == null) throw new EntityNotFoundException("--Prov Name-->"+modModelFact.getFactModelsName()+"--Fact Number-->"+modModelFact.getFactModelsId());
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getOneFactModel","Exiting","...................>>..");
		return readModelFact;
	}
	// -------------------Model Facts----->delOneFactModel------------------------------------------------
	@Transactional
	public void delOneFactModel(FactModels delModelFact) {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->delOneFactModel","Entering","..>>...................");
		/* Check if the field exist */
		FactModels checkFactModels = factModelsRepository.findByFactModelsId(delModelFact.getFactModelsId());		
		if (checkFactModels == null) throw new UserDoesNotExistException("Field does NOT exists-->");
		int artsDeleted = artsFactModelsRepository.deleteByFactModelsId(checkFactModels.getFactModelsId());
		consoleLog.logMsg(showLogMsg,"->FactModelsService->delOneFactModel","artsDeleted......>>..",Integer.toString(artsDeleted));
		factModelsRepository.deleteById(delModelFact.getFactModelsId());
		consoleLog.logMsg(showLogMsg,"->FactModelsService->delOneFactModel","Exiting","...................>>..");
	}
	// -------------------Model Facts----->DeleteAllModel------------------------------------------------
	@Transactional
	public void deleteAllFactModels() {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->deleteAllFactModels","Entering","..>>...................");
		List<FactModels> checkFactModelsList = factModelsRepository.findAll();
		int artsDeleted = 0;
		for(int i = 0;i<checkFactModelsList.size();i++) {
			artsDeleted = artsDeleted + artsFactModelsRepository.deleteByFactModelsId(checkFactModelsList.get(i).getFactModelsId());
		}
		consoleLog.logMsg(showLogMsg,"->FactModelsService->deleteAllFactModels","artsDeleted......>>..",Integer.toString(artsDeleted));
		factModelsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->FactModelsService->deleteAllFactModels","Exiting","...................>>..");
	}
	// -------------------Arts Model Facts------------------------------------------------
	// -------------------Arts Model Facts----->createArtsFactModel------------------------------------------------
	@Transactional
	public List<Arts> createArtsFactModel(List<Arts> artsModelFactList) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->createArtsFactModel","Entering","..>>...................");
		/* Create couples modelFactId-ArtId and return back a list of Arts
		/* Checking null new object or empty mandatory attributes  */
		if ((artsModelFactList==null)) throw new EmptyFieldException("->FactModelsService->createArtsFactModel-> Null Object or Empty Mandatoy Parameters");	
		/* Check if modelFactId exist, first element of arts list */
		FactModels readModelFact=factModelsRepository.findByFactModelsId(artsModelFactList.get(0).getArtId());
		if ((readModelFact==null)) throw new EntityNotFoundException("->FactModelsService->createArtsFactModel-> Model Fact NOT Found"+artsModelFactList.get(0).getArtId().toString());
		List<Arts> createdArtModelFactList = new ArrayList<Arts>();
		ArtsFactModels createdArtModelFact;
		for(int i = 1;i<artsModelFactList.size();i++)	{
			// Check Art exists in Arts entity
			if ( artsRepository.findByArtId(artsModelFactList.get(i).getArtId()) != null ) {
				ArtsFactModels newArtModelFact = new ArtsFactModels(readModelFact.getFactModelsId(),readModelFact.getFactModelsName(),
						artsModelFactList.get(i).getArtId(), artsModelFactList.get(i).getArtName());
				createdArtModelFact = artsFactModelsRepository.save(newArtModelFact);
				if (createdArtModelFact == null) throw new EntityAlreadyExistsException("->FactModelsService->createArtsFactModel->Error Creating ArtModelFact");
				createdArtModelFactList.add(artsModelFactList.get(i));
				consoleLog.logMsg(showLogMsg,"->FactModelsService->createArtsFactModel","createdArtModelFact...................>>..",createdArtModelFact.getArtName());

			}
		}

		consoleLog.logMsg(showLogMsg,"->FactModelsService->createArtsFactModel","Exiting","...................>>..");
		return createdArtModelFactList;
	}
	// -------------------Arts Model Facts----->deleteArtsFactModel------------------------------------------------
	@Transactional
	public int deleteArtsFactModel(FactModels updFactModels) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->deleteArtsFactModel","Entering","..>>...................");
		/* Delete current list of artsModelFact and create new received list of Arts */ 
		int deletedArtsFactModel = 0;
		deletedArtsFactModel = artsFactModelsRepository.deleteByFactModelsId(updFactModels.getFactModelsId());
		consoleLog.logMsg(showLogMsg,"->FactModelsService->deleteArtsFactModel","deletedArtsFactModel..>>...................",String.valueOf(deletedArtsFactModel));

		consoleLog.logMsg(showLogMsg,"->FactModelsService->deleteArtsFactModel","Exiting","..>>...................");
		return deletedArtsFactModel;
	}
	// -------------------Arts Model Facts----->getArtsFactModel------------------------------------------------
	@Transactional
	public List<Arts> getArtsFactModel(FactModels updModelFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getArtsFactModel","Entering","..>>...................");		
		/* Checking null new object or empty mandatory attributes  */
		if ((updModelFact==null)) throw new EmptyFieldException("->FactModelsService->getArtsFactModel-> Null Object or Empty Mandatoy Parameters");	
		/* Check if modelFactId exist */
		FactModels readModelFact=factModelsRepository.findByFactModelsId(updModelFact.getFactModelsId());
		if ((readModelFact==null)) throw new EntityNotFoundException("->FactModelsService->getArtsFactModel-> Model Fact Not Found"+updModelFact.getFactModelsName());
		/* Getting all Arts id from ArstFactModels entity */
		List<ArtsFactModels> readArstFactModels = new ArrayList<ArtsFactModels>();
		readArstFactModels = artsFactModelsRepository.findByFactModelsId(updModelFact.getFactModelsId());
		/* Creating the list of arts corresponding to previous arts id found for this ModelFact */
		List<Arts> readArtsList = new ArrayList<Arts>();
		for(int i = 0;i<readArstFactModels.size();i++)	{
			// Get the Art object for each Art id in ArstFactModels
			Arts readArt = artsRepository.findByArtId(readArstFactModels.get(i).getArtId());
			if ( readArt != null ) {
				readArtsList.add(readArt);
			}
		}
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getArtsFactModel","Exiting","...................>>..");
		return readArtsList;
	}
	// -------------------Arts Model Facts----->getArtFactModelsWithArt------------------------------------------------
	@Transactional
	public List<ArtsFactModels> getArtFactModelsWithArt(Arts selArt) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getArtsFactModel","Entering","..>>...................");		
		/* Checking null new object or empty mandatory attributes  */
		if ((selArt==null)) throw new EmptyFieldException("->FactModelsService->getArtFactModelsWithArt-> Null Object or Empty Mandatoy Parameters");	
		/* Check if modelFactId exist */
		Arts readArt=artsRepository.findByArtId(selArt.getArtId());
		if ((readArt==null)) throw new EntityNotFoundException("->FactModelsService->getArtFactModelsWithArt-> Art Not Found"+selArt.getArtName());
		/* Getting all ArstFactModels having this Art id */
		List<ArtsFactModels> readArstFactModels = new ArrayList<ArtsFactModels>();
		readArstFactModels = artsFactModelsRepository.findByArtId(selArt.getArtId());		
		consoleLog.logMsg(showLogMsg,"->FactModelsService->getArtFactModelsWithArt","Exiting","...................>>..");
		return readArstFactModels;
	}
	// ------------------------------------INITIALIZE FACTS MODEL DATE--------------------------------------------------------------	
	// -------------------Facts Model----->setFactModelsCurrentDate---------------------------------------------------------------	
	@Transactional
	public void setFactModelsCurrentDate() {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->setFactModelsCurrentDate","Entering","..>>...................");
		Date currentDate = new Date();
		List<FactModels> modelFactsList = factModelsRepository.findAll();
		for(int j=0; j<modelFactsList.size(); j++) {
			if (modelFactsList.get(j).getFactModelsDate() == null) {
				modelFactsList.get(j).setFactModelsDate(currentDate);
				factModelsRepository.save(modelFactsList.get(j));
			}
		}		
		consoleLog.logMsg(showLogMsg,"->FactModelsService->setFactModelsCurrentDate","Exiting","...................>>..");
		return ;
	}
	// -------------------Art Facts Model----->set FactModels name and Art name --------------------------------------------------	
	@Transactional
	public void setArtFactModelsNames() {
		consoleLog.logMsg(showLogMsg,"->FactModelsService->setArtFactModelsNames","Entering","..>>...................");
		List<ArtsFactModels> artsFactModelsList = artsFactModelsRepository.findAll();
		FactModels readFactModel;
		Arts readArt;
		for(int j=0; j<artsFactModelsList.size(); j++) {
			readFactModel = factModelsRepository.findByFactModelsId(artsFactModelsList.get(j).getFactModelsId());
			if (readFactModel != null) artsFactModelsList.get(j).setFactModelsName(readFactModel.getFactModelsName());
			else artsFactModelsList.get(j).setFactModelsName("Fact Model NOT Found");
			
			readArt = artsRepository.findByArtId(artsFactModelsList.get(j).getArtId());
			if (readArt != null) artsFactModelsList.get(j).setArtName(readArt.getArtName());
			else artsFactModelsList.get(j).setArtName("Art NOT Found");
			
			artsFactModelsRepository.save(artsFactModelsList.get(j));
		}		
		consoleLog.logMsg(showLogMsg,"->FactModelsService->setArtFactModelsNames","Exiting","...................>>..");
		return ;
	}

}