package com.fran.gestb.dbbarc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.DSourceDbBarXXMultiTenantManager;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.UserDoesNotExistException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.Fields;
import com.fran.gestb.dbbarc.model.Values;
import com.fran.gestb.dbbarc.repo.FieldsRepository;
import com.fran.gestb.dbbarc.repo.ValuesRepository;
import com.google.gson.JsonArray;

@Service
public class FieldsService {

		@Autowired
		private FieldsRepository fieldsRepository;
		@Autowired
		private ValuesRepository valuesRepository;
		@Autowired
		DSourceDbBarXXMultiTenantManager multiTenantManager;
		@Autowired
		private ConsoleLog consoleLog; 
		private boolean showLogMsg = false;
		
		private Fields otherField,startField;
		private long startFieldId;
		private List<Values> startValuesList;
		private List<Values> valuesList;

		private boolean valueAlreadyExists;
		
		private List<ValueRecord> valueRecordListElement;
		private List<List<ValueRecord>> valueRecordList;

		private String fieldName,fieldExplan;
	    private String[] valuesNames;
	    
		// -------FIELDS--------FIELDS---------FIELDS---------FIELDS---------FIELDS--------------FIELDS----------------
		// -------------------Fields----->getAllFields-----------------------------------------------------------
		@Transactional(readOnly = true)
		public List<Fields> getAllFields() {
			consoleLog.logMsg(showLogMsg,"->FieldsService->getAllFields","Entering","..>>...................");
			List<Fields> fieldsList = fieldsRepository.findAll();
			consoleLog.logMsg(showLogMsg,"->FieldsService->getAllFields","Exiting","...................>>..");
			return fieldsList;
		}
		// -------------------Fields----->getOneField-----------------------------------------------------------
		@Transactional(readOnly = true)
		public Fields getOneField(Fields newField) throws EmptyFieldException, UserDoesNotExistException {
			consoleLog.logMsg(showLogMsg,"->FieldsService->getOneField","Entering","..>>...................");
			if ((newField==null) | (newField.getFieldName()=="")) throw new EmptyFieldException("->FieldsService->getOneField->Null Object or Empty Mandatoy Parameters");
			/* Checking attributes of new object */
			Fields readField = fieldsRepository.findByFieldId(newField.getFieldId());
			if (readField==null) throw new UserDoesNotExistException("Field does not exist-->"+newField.getFieldName());						
			consoleLog.logMsg(showLogMsg,"->FieldsService->getOneField","Exiting","...................>>..");
			return readField;
		}
		// -------------------Fields----->addOneField----------------------------------------------------------------	    
		@Transactional
		public Fields addOneField(Fields newField) throws EmptyFieldException, EntityAlreadyExistsException {
			consoleLog.logMsg(showLogMsg,"->FieldsService->addOneField","Entering","..>>...................");
			/* Checking null new object or empty mandatory attributes  */
			if ((newField==null) | (newField.getFieldName()=="")) throw new EmptyFieldException("->FieldsService->addOneField->Null Object or Empty Mandatoy Parameters");
			/* Checking attributes of new object */
			if (newField.checkFieldAtributes()==false) throw new 
					WrongParametersException("--Field Name-->"+otherField.getFieldName()+"--Field Explan-->"+newField.getFieldExplan());
			/* Checking there no other object with same name */
			Fields readField = fieldsRepository.findByFieldName(newField.getFieldName());
			if (readField != null) throw new EntityAlreadyExistsException("--Field Name-->"+readField.getFieldName());
			readField = fieldsRepository.save(newField);
			consoleLog.logMsg(showLogMsg,"->FieldsService->addOneField","Exiting","...................>>..");
			return readField;
		}
		// -------------------Fields----->updateOneField----------------------------------------------------------------
		@Transactional
		public void updateOneField(Fields updField) {
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneField","Entering","..>>...................");
			/* Check if the field exists */
			otherField = fieldsRepository.findByFieldName(updField.getFieldName());
			otherField = fieldsRepository.findByFieldId(updField.getFieldId());
			if (otherField == null) throw new UserDoesNotExistException("Field does not exist-->"+updField.getFieldName());
			fieldsRepository.save(updField);
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneField","Exiting","...................>>..");
		}
		// -------------------Fields----->updateOneFieldStatus----------------------------------------------------------------
		@Transactional
		public void updateOneFieldStatus(Fields updField) {
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneFieldStatus","Entering","..>>...................");
			/* Check if the field exists */
			Fields readField = fieldsRepository.findByFieldName(updField.getFieldName());
			if (readField == null) throw new UserDoesNotExistException("Field Name does not exist-->"+updField.getFieldName());			
			readField = fieldsRepository.findByFieldId(updField.getFieldId());
			if (readField == null) throw new UserDoesNotExistException("Field Id does not exist-->"+updField.getFieldId());
			fieldsRepository.save(updField);
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneFieldStatus","Exiting","...................>>..");
		}
		// -------------------Fields----->deleteOneField----------------------------------------------------------------
		@Transactional
		public void deleteOneField(Fields delField) {
			consoleLog.logMsg(showLogMsg,"->FieldsService->deleteOneField","Entering","..>>...................");
			/* Check if the field exist */
			Fields readField = fieldsRepository.findByFieldId(delField.getFieldId());
			if (readField == null) throw new UserDoesNotExistException("Field does NOT exists-->"+delField.getFieldName());
			this.deleteValuesOneField(delField.getFieldId());
			fieldsRepository.deleteById(delField.getFieldId());
			consoleLog.logMsg(showLogMsg,"->FieldsService->deleteOneField","Exiting","...................>>..");
		}	
		// -------------------Fields----->delAllFields------------------------------------------------
		@Transactional
		public void delAllFields() {
			consoleLog.logMsg(showLogMsg,"->FieldsService->delAllFields","Entering","..>>...................");
			fieldsRepository.deleteAll();
			consoleLog.logMsg(showLogMsg,"->FieldsService->delAllFields","Exiting","...................>>..");
		}		
		// -------VALUES--------VALUES---------VALUES---------VALUES---------VALUES--------------VALUES----------------
		// -------------------Values----->getOneFieldValuesList----------------------------------------------------------------
		@Transactional
		public List<Values> getOneFieldValuesList(Fields newField) {
			consoleLog.logMsg(showLogMsg,"->FieldsService->getOneFieldValuesList","Entering","..>>...................");
			valueRecordList = new ArrayList<List<ValueRecord>>();		
			List<Values> newFieldValuesList = valuesRepository.findByFieldId(newField.getFieldId());
			consoleLog.logMsg(showLogMsg,"->FieldsService->getOneFieldValuesList","Exiting","...................>>..");
			return newFieldValuesList;
		}	
		// -------------------Values----->createOneFieldValuesList----------------------------------------------------------------
		@Transactional
		public List<Values> createOneFieldValuesList(List<Values> newFieldValuesList) throws UserDoesNotExistException, EmptyFieldException, EntityAlreadyExistsException {
			consoleLog.logMsg(showLogMsg,"->FieldsService->createOneFieldValuesList","Entering","..>>...................");
			if ((newFieldValuesList==null) | (newFieldValuesList.size()==0)) 
					throw new EmptyFieldException("->FieldsService->createOneFieldValuesList->Null Object or Empty Mandatoy Parameters");
			Fields readField =fieldsRepository.findByFieldId(newFieldValuesList.get(0).getFieldId());
			if (readField==null) throw new UserDoesNotExistException("Field does not exist-->"+newFieldValuesList.get(0).getFieldId());						
			List<Values> createdFieldValuesList = new ArrayList<Values>();			
			for(int j=0; j<newFieldValuesList.size(); j++) {
				if (newFieldValuesList.get(j).checkValueAtributes()==false) throw new 
					WrongParametersException(	"--Value Name-->"+newFieldValuesList.get(j).getValueName()+
												"--Field Id-->"+newFieldValuesList.get(j).getFieldId());
				Values createdValue = valuesRepository.save(newFieldValuesList.get(j));
				if (createdValue != null) {
					createdFieldValuesList.add(createdValue);
				}
			}
			this.setValuesStringInField(fieldsRepository.findByFieldId(readField.getFieldId()));
			consoleLog.logMsg(showLogMsg,"->FieldsService->createOneFieldValuesList","Exiting","...................>>..");
			return createdFieldValuesList;
		}
		// -------------------Values----->updateOneFieldValuesList----------------------------------------------------------------
		@Transactional
		public List<Values> updateOneFieldValuesList(List<Values> updFieldValuesList) {
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneFieldValuesList","Entering","..>>...................");
			Fields readField =fieldsRepository.findByFieldId(updFieldValuesList.get(0).getFieldId());
			if (readField==null) throw new UserDoesNotExistException("Field does not exist-->"+updFieldValuesList.get(0).getFieldId());						
			int valuesDeleted = valuesRepository.deleteByFieldId(readField.getFieldId());
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneFieldValuesList","valuesDeleted",Integer.toString(valuesDeleted));
			List<Values> updatedFieldValuesList = this.createOneFieldValuesList(updFieldValuesList);
			consoleLog.logMsg(showLogMsg,"->FieldsService->updateOneFieldValuesList","Exiting","...................>>..");
			return updatedFieldValuesList;
		}
		// -------------------Values----->getFieldAndValues----------------------------------------------------------------
		@Transactional
		public List<List<ValueRecord>> getFieldsAndValues() {
			consoleLog.logMsg(showLogMsg,"->FieldsService->getFieldAndValues","Entering","..>>...................");
			valueRecordList = new ArrayList<List<ValueRecord>>();		
			List<Fields> fieldsList = fieldsRepository.findAll();
			for(int i = 0;i<fieldsList.size();i++) {
				valueRecordListElement = new ArrayList<ValueRecord>();							
				long fieldId = fieldsList.get(i).getFieldId();
				List<Values> valuesList = valuesRepository.findByFieldId(fieldId);
				valueRecordListElement.add(new ValueRecord(0,fieldsList.get(i).getFieldName()));
				for(int j = 0;j<valuesList.size();j++) {
					valueRecordListElement.add(new ValueRecord(j + 1,valuesList.get(j).getValueName()));
				}
				valueRecordList.add(valueRecordListElement);
			}
			consoleLog.logMsg(showLogMsg,"->FieldsService->getFieldAndValues","Exiting","...................>>..");
			return valueRecordList;
		}	
		// -------------------Values----->deleteValuesOneField----------------------------------------------------------------
		@Transactional
		public void deleteValuesOneField(Long myFieldId){	
			consoleLog.logMsg(showLogMsg,"->FieldsService->deleteValuesOneField","Entering","..>>...................");
			valuesList=valuesRepository.findByFieldId(myFieldId);
			for(int indice = 0;indice<valuesList.size();indice++) {
				valuesRepository.deleteById(valuesList.get(indice).getValueId());
			}
			consoleLog.logMsg(showLogMsg,"->FieldsService->deleteValuesOneField","Exiting","...................>>..");
			return;
		}
		// -------------------Values----->addOneValue----------------------------------------------------------------
		@Transactional
		public Values addOneValue(Values newValues) throws EmptyFieldException, EntityAlreadyExistsException  {
			consoleLog.logMsg(showLogMsg,"->FieldsService->addOneValue","Entering","..>>...................");
			String valueName = newValues.getValueName();
			// Check there is no other value for this field with same name 
			long fieldId = newValues.getFieldId();
			valuesList = valuesRepository.findByFieldId(fieldId);
			for(int j = 0;j<valuesList.size();j++) {
				if ( valuesList.get(j).getValueName().equals(valueName) ) {
					throw new EntityAlreadyExistsException("Value already exists-->"+newValues.getValueName());
				}	
			}
			Values readValue = valuesRepository.save(newValues);
			consoleLog.logMsg(showLogMsg,"->FieldsService->addOneValue","Exiting","...................>>..");
			return readValue;
		}
		// -------VALUES--------FIELDS---------AUXILIARY-------VALUES--------FIELDS---------AUXILIARY-----------------
		//----------------------- Current Bar ------------- Return current bar name string -------------------- 
		public String getCurrentBarValue() {
			consoleLog.logMsg(showLogMsg,"->FieldsService->setCurrentBarFieldAndValue","...Starting","................>>..");
			Fields currentBarField = fieldsRepository.findByFieldName("currentBar");
			if (currentBarField == null) 
				throw new EntityNotFoundException("->FieldsService->getCurrentBarValue---> NO currentBar Field...");
			List<Values> valuesCurrentBarField = valuesRepository.findByFieldId(currentBarField.getFieldId());		
			String currentBarName = valuesCurrentBarField.get(0).getValueName();
			if ( (currentBarName == null) | (currentBarName.length() == 0) ) 
				throw new EntityNotFoundException("->FieldsService->getCurrentBarValue---> CurrentBar Field Value is EMPTY...");
			return currentBarName;			
		}
		//----------------------- Current Bar ------------- Change and/or Create Field and Values -------------------- 
		@Transactional
		public void setCurrentBarFieldAndValue(String selectedBar) {
			consoleLog.logMsg(showLogMsg,"->FieldsService->setCurrentBarFieldAndValue","...Starting","................>>..");
			Fields currentBarField = fieldsRepository.findByFieldName("currentBar");
			if (currentBarField == null) {
				consoleLog.logMsg(showLogMsg,"->FieldsService->setCurrentBarFieldAndValue","..Creating..currentBar..Field..","..Value.."+selectedBar+"....");
				fieldName = "currentBar";
		    	fieldExplan = "Bar de trabajo";
		    	valuesNames = new String[] {selectedBar};	    
		    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
		    	return;
			}
			List<Values> valuesCurrentBarField = valuesRepository.findByFieldId(currentBarField.getFieldId());		
			valuesCurrentBarField.get(0).setValueName(selectedBar);
			Values savedCurrentBarNameValue = valuesRepository.save(valuesCurrentBarField.get(0));
			if (savedCurrentBarNameValue == null) 
				throw new EntityNotFoundException("->FieldsService->setCurrentBarFieldAndValue"+selectedBar);
		}
		//----------------------- Current Bar ------------- Change and/or Create Field and Values -------------------- 
		@Transactional
		public String getCurrentBarString() {
			consoleLog.logMsg(showLogMsg,"->FieldsService->getCurrentBarString","...Starting","................>>..");
			Fields currentBarField = fieldsRepository.findByFieldName("currentBar");
			if (currentBarField == null) {
				return null;
			}
			List<Values> valuesCurrentBarField = valuesRepository.findByFieldId(currentBarField.getFieldId());		
			String currentBarName = valuesCurrentBarField.get(0).getValueName();
			if (currentBarName == null) 
				throw new EntityNotFoundException("->FieldsService->getCurrentBarString->"+"no current bar found");
			return currentBarName;
		}
		// -------------------Values----->Creating string with all values of the field------------------------------------------------
		public void setValuesStringInField(Fields myField){
			consoleLog.logMsg(showLogMsg,"->FieldsService->setValuesStringInField","Entering","..>>...................");			
			List<Values> valuesList = valuesRepository.findByFieldId(myField.getFieldId());
			String valuesString="";
			for(int indice = 0;indice<valuesList.size();indice++)
			{
				valuesString=valuesString.concat(valuesList.get(indice).getValueName());
				valuesString=valuesString.concat("-");
			}
			myField.setFieldValuesString(valuesString);
			this.updateOneField(myField);
			consoleLog.logMsg(showLogMsg,"->FieldsService->setValuesStringInField","Exiting","...................>>..");
			return;
		}
		// -------------------Fields----->getValuesOfField----------------------------------------------------------------
		@Transactional
		public JsonArray getValuesOfField(String fieldName) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException{
			consoleLog.logMsg(showLogMsg,"->FieldsService->getValuesOfField","Entering","..>>...................");
			consoleLog.logMsg(showLogMsg,"->FieldsService->getValuesOfField","......fieldName.....",fieldName);
			if ((fieldName=="") || (fieldName==null)) throw new EmptyFieldException("->FieldsService->getValuesOfField->Empty Field Name");
			Fields getField;
			getField = fieldsRepository.findByFieldName(fieldName);
			if (getField==null) throw new EntityNotFoundException("->FieldsService->getValuesOfField->Field Not Defined->"+fieldName);	
			List<Values> valuesOfField = valuesRepository.findByFieldId(getField.getFieldId());
			JsonArray listValuesString = new JsonArray();
			for (int i=0 ; i< valuesOfField.size(); i++) {
				listValuesString.add( valuesOfField.get(i).getValueName() );
			}		
			consoleLog.logMsg(showLogMsg,"->FieldsService->getValuesOfField","Exiting","...................>>..");
			return listValuesString;
		}
		
		// -------------------Fields----->createFieldAndValues----------------------------------------------------------------
		@Transactional
		public void createFieldAndValues(String fieldName, String fieldExplan, String[] valuesNames) {	
			consoleLog.logMsg(showLogMsg,"->FieldsService->createFieldAndValues","Entering","...................>>..");
			if (fieldName == null) return;		
			if ( (fieldsRepository.findByFieldName(fieldName)) == null ) 
	    		fieldsRepository.save(new Fields(fieldName,fieldExplan));
			if (valuesNames == null) return;			
	    	startField = fieldsRepository.findByFieldName(fieldName);
	    	startFieldId = startField.getFieldId();
	    	startValuesList = valuesRepository.findByFieldId(startFieldId);
	    	for (int j=0;j<valuesNames.length;j++) {
	    		valueAlreadyExists = false;
				for(int i = 0;i<startValuesList.size();i++)	{
					if (startValuesList.get(i).getValueName().equals(valuesNames[j])) { 
						valueAlreadyExists = true;
					}
				}
				if (valueAlreadyExists == false) valuesRepository.save(new Values(valuesNames[j],startField.getFieldId()));
	    	}
			this.setValuesStringInField(startField);
			consoleLog.logMsg(showLogMsg,"->FieldsService->createFieldAndValues","Exiting","...................>>..");
			return;
		}
		// -------------------Fields----->startCreateFieldsAndValues----------------------------------------------------------------
		@Transactional
		public void startCreateFieldsAndValues() {
			consoleLog.logMsg(showLogMsg,"->FieldsService->startCreateFieldAndValues","Entering","...................>>..");
			// -----------------------------------------------------SETTING CURRTEN BAR---//
			fieldName = "currentBar";
	    	fieldExplan = "Bar de trabajo";
	    	valuesNames = new String[] {"RUBI"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
			// ----------------------------------------------------------------ARTS TYPES TAXES UNITS---//
			/*
	    	fieldName = "artTypes";
	    	fieldExplan = "Tipos de Artículos";
	    	valuesNames = new String[] {"Comida","Bebida","Acomp","SumAgua","SumElec","SumGas"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); */
	    	fieldName = "artTaxes";
	    	fieldExplan = "Impuesto aplicable a los Artículos";
	    	valuesNames = new String[] {"4","10","21","33"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	fieldName = "artUnits";
	    	fieldExplan = "Unidades de medida para los Artículos";
	    	valuesNames = new String[] {"Ud","Kg","Lt","Kw","Kwh","m3"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	// ----------------------------------------------------------------CIFTYPES---//
	    	fieldName = "cifTypes";
	    	fieldExplan = "Tipos de CIFs permitidos";
	    	valuesNames = new String[] {"CIF","NIF","NIE","DNI","Other"};	    	
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames );  
	    	// ----------------------------------------------------------------FACTTYPES---//
	    	/*
	    	fieldName = "factTypes";
	    	fieldExplan = "Tipos de Facturas";
	    	valuesNames = new String[] {"Bebidas","Comidas","Condimentos",
	    			"Reparaciones","Gas","Electricidad","Agua","Alarma","Comision Banco"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); */
	    	// ----------------------------------------------------------------FIELD NAMES---//
	    	fieldName = "fieldNames";
	    	fieldExplan = "Nombre de Campos en los que se puede seleccionar de una lista predefinida";
	    	valuesNames = new String[] {"Prueba_1","Prueba_2","screens","languages","cifTypes","hdTypes","hdPlaces","mProvTypes","artTypes",
	    								"artTaxes","artUnits","factTypes"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames );
	    	// ----------------------------------------------------------------HD PLACES TYPES---//
	    	fieldName = "hdPlaces";
	    	fieldExplan = "Lugares predeterminados donde mostrar la información en la Hoja del Día";
	    	valuesNames = new String[] {"TPV","FIX","SUM","RES","FACT","TARJ"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	fieldName = "hdTypes";
	    	fieldExplan = "Tipos en las Hojas del Día";
	    	valuesNames = new String[] {"TPV1","TPV2","TPV3","MAQB","MAQA","TAB","BANK","CAJA","RET","GOODS","OTHER"};	  
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames );    	
	    	// ----------------------------------------------------------------LANGUAGES---//
	    	fieldName = "languages";
	    	fieldExplan = "Idiomas soportados";
	    	valuesNames = new String[] {"SPA","ENG"};	    	  
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames );  	    
	    	// ----------------------------------------------------------------MPROVTYPES---//
	    	fieldName = "mProvTypes";
	    	fieldExplan = "Tipos de datos adicionales para los Proveedores";
	    	valuesNames = new String[] {"email","web","address","Ñ_phone","Ñ_mobile","phone","mobile","notes"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames );  
	    	// ----------------------------------------------------------------SCREENS---//	    	
	    	fieldName = "screens"; 
	    	fieldExplan = "Lista de pantallas para mostrar en diferentes Idiomas";
	    	valuesNames = new String[] {"gbviews","gbfields","gbprovs"};	    	
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames );    	
	    	// ----------------------------------------------------------------CARDS TPVTYPES---//  	
	    	fieldName = "tpvTypes";
	    	fieldExplan = "Tipos de TPVs para Tarjetas";
	    	valuesNames = new String[] {"Visas","Vale Comida","Just Eat"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	// ---------------------------------------------------------------DB URL---//  	
	    	fieldName = "dbUrl";
	    	fieldExplan = "Servidores donde están las Bases de Datos";
	    	valuesNames = new String[] {"jdbc:postgresql://localhost:5432/","jdbc:postgresql://127.0.0.1/"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	// ---------------------------------------------------------------DB USER NAME---//  	
	    	fieldName = "dbUserName";
	    	fieldExplan = "Usuario de acceso a las Bases de Datos";
	    	valuesNames = new String[] {"baresadmuser","baresuser"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	// ---------------------------------------------------------------DB DRIVER CLASS NAME---//  	
	    	fieldName = "dbDriverClassName";
	    	fieldExplan = "Driver para acceder a las Bases de Datos";
	    	valuesNames = new String[] {"org.postgresql.Driver","com.mysql.jdbc.Driver"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	// ---------------------------------------------------------------DB DDL AUTO---//  	
	    	fieldName = "dbDdlAuto";
	    	fieldExplan = "Valor Ddl Auto para las Bases de Datos";
	    	valuesNames = new String[] {"update","validate","none"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	// ---------------------------------------------------------------DB PHYSICAL NAME STRATEGY---//  	
	    	fieldName = "dbPhysNStrategy";
	    	fieldExplan = "Valor Physical Naming Strategy para las Bases de Datos";
	    	valuesNames = new String[] {"org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy",
	    								"org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl"};	    
	    	this.createFieldAndValues(fieldName,fieldExplan,valuesNames ); 
	    	
	    	consoleLog.logMsg(showLogMsg,"->FieldsService->startCreateFieldAndValues","Exiting","...................>>..");
	    	return;
		}
		
}
