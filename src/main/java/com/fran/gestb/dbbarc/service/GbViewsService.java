package com.fran.gestb.dbbarc.service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.UserDoesNotExistException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.GbViews;
import com.fran.gestb.dbbarc.repo.GbViewsRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


@Service
public class GbViewsService {

	@Autowired
	private GbViewsRepository gbViewsRepository;
	@Autowired
	private FieldsService fieldsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	private GbViews otherGbView;
	private List<GbViews> gbViewsList;
	private String gbViewName, gbViewLang;
	
	@Transactional
	public GbViews[] save(GbViews[] newGbViews) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->save","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newGbViews==null) | (newGbViews[0].getGbViewName()=="")) throw new EmptyFieldException("Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newGbViews[0].checkGbViewAtributes()==false) throw new 
				WrongParametersException("--GbView Name-->"+otherGbView.getGbViewName()+"--GbView Explan-->"+otherGbView.getObjName());
		/* Checking there no other object with same name */
		consoleLog.logMsg(showLogMsg,"->GbViewsService->save","Exiting","...................>>..");
		return newGbViews;
	}

	@Transactional(readOnly = true)
	public List<GbViews> findAllGbViews() {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findAllGbViews","Entering","..>>...................");
		List<GbViews> gbViewsList = gbViewsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findAllGbViews","Exiting","...................>>..");
	return gbViewsList;
	}
	
	@Transactional(readOnly = true)
	public List<GbViews> findByObjName(String objName) {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByObjName","Entering","..>>...................");
		List<GbViews> gbViewsList = gbViewsRepository.findByObjName(objName);
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByObjName","Exiting","...................>>..");
	return gbViewsList;
	}

	@Transactional(readOnly = true)
	public List<GbViews> findByGbViewName(String gbViewName) {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByGbViewName","Entering","..>>...................");
		List<GbViews> gbViewsList = gbViewsRepository.findByGbViewName(gbViewName);
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByGbViewName","Exiting","...................>>..");
	return gbViewsList;
	}
	
	@Transactional(readOnly = true)
	public GbViews findByGbViewId(long gbViewsId) {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByGbViewId","Entering","..>>...................");
		return gbViewsRepository.findByGbViewId(gbViewsId);
	}

	@Transactional(readOnly = true)
	public List<GbViews> findByGbViewNameAndObjLang(String gbViewName, String gbViewlang) {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByGbViewNameAndObjLang","Entering","..>>...................");
		List<GbViews> gbViewsList = gbViewsRepository.findByGbViewNameAndObjLang(gbViewName, gbViewlang);
		consoleLog.logMsg(showLogMsg,"->GbViewsService->findByGbViewNameAndObjLang","Exiting","...................>>..");
	return gbViewsList;
	}
	
	@Transactional
	public void update(GbViews updGbView) {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->update","Entering","..>>...................");
		/* Check if the gbView exists */
		consoleLog.logMsg(showLogMsg,"->GbViewsService->update","Exiting","...................>>..");
	}

	@Transactional
	public void delete(GbViews delGbView) {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->delete","Entering","..>>...................");
		/* Check if the gbView exist */
		gbViewName = delGbView.getGbViewName();
		gbViewLang = delGbView.getObjLang();
		gbViewsList = gbViewsRepository.findByGbViewNameAndObjLang(gbViewName,gbViewLang);
		if (gbViewsList.size() == 0) throw new UserDoesNotExistException("GbView does NOT exists-->"+gbViewName);
		Long rowsRemoved = gbViewsRepository.removeByGbViewNameAndObjLang(gbViewName,gbViewLang);
		consoleLog.logMsg(showLogMsg,"->GbViewsService->delete","Exiting",rowsRemoved+"...................>>..");
	}
	@Transactional
	public void deleteAll() {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->deleteAll","Entering","..>>...................");
		gbViewsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->GbViewsService->deleteAll","Exiting","...................>>..");
	}
	@Transactional
	public HashMap<String,Object> getGbViews(String gbViewName) throws JsonParseException, JsonMappingException, IOException {
		consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","Entering","..>>...................");
		
		JsonObject formTitles = new JsonObject();
		List<GbViews> titlesList = gbViewsRepository.findByGbViewNameAndObjTypeAndObjLang(gbViewName,"TITLE","APP");
		for (int i=0; i< titlesList.size();i++) {
			formTitles.addProperty(titlesList.get(i).getObjName(),titlesList.get(i).getObjText());
		}

		JsonObject formLabels = new JsonObject();
		JsonObject formErrors = new JsonObject();
		List<GbViews> labelsList = gbViewsRepository.findByGbViewNameAndObjTypeAndObjLang(gbViewName,"LABEL","APP");
		for (int i=0; i< labelsList.size();i++) {
			formLabels.addProperty(labelsList.get(i).getObjName(),labelsList.get(i).getObjText());
			formErrors.addProperty(labelsList.get(i).getObjName(),"");
		}
	
		ArrayList<String> objSetList = new ArrayList<String>();
		List<GbViews> errorsList = gbViewsRepository.findByGbViewNameAndObjTypeAndObjLang(gbViewName,"ERROR","APP");
		objSetList.add(errorsList.get(0).getObjSet());
		for (int i=0; i< errorsList.size();i++) {
			if ( !objSetList.contains(errorsList.get(i).getObjSet()) ) {
				objSetList.add(errorsList.get(i).getObjSet());
			}
		}
		
		JsonObject validationMessages = new JsonObject();
		for (int i=0; i< objSetList.size();i++) {
			List<GbViews> validationMsgsList = gbViewsRepository.findByGbViewNameAndObjTypeAndObjSetAndObjLang(gbViewName,"ERROR",objSetList.get(i),"APP");
			JsonObject tempValMgs = new JsonObject();
			for (int j=0; j< validationMsgsList.size();j++) {
				tempValMgs.addProperty(validationMsgsList.get(j).getObjName(),validationMsgsList.get(j).getObjText());
			}
			validationMessages.add(objSetList.get(i), tempValMgs);	
		} 
		consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","JSON TITLES","...............................");
		System.out.println(formTitles.toString());
		consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","JSON LABELS","...............................");
		System.out.println(formLabels.toString());
		consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","JSON ERRORS","...............................");
		System.out.println(formErrors.toString());
		consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","JSON VALIDATION MESSAGES","...............................");
		System.out.println(validationMessages.toString());
		
		JsonObject completeMessage = new JsonObject();
		completeMessage.add("formTitles", formTitles);
		completeMessage.add("formLabels", formLabels);
		completeMessage.add("formErrors", formErrors);
		completeMessage.add("validationMessages", validationMessages);

		switch (gbViewName) {
		case "gbprovs":
			JsonArray listCifTypes;
			listCifTypes = fieldsService.getValuesOfField("CifTypes");
			consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","JSON ARRAY CIF TYPES","...............................");
			System.out.println(listCifTypes.toString());
			completeMessage.add("cifTypes", listCifTypes);
			break;
		default:
			break;
		}

		ObjectMapper mapper = new ObjectMapper();
		TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};
		HashMap<String,Object> completeMessageMap = mapper.readValue(completeMessage.toString(), typeRef); 
		System.out.println("->GbViewsService->getGbViews-> Message HashMap--->\n" + completeMessageMap);
		
		consoleLog.logMsg(showLogMsg,"->GbViewsService->getGbViews","Exiting","...................>>..");
		return completeMessageMap;
	}
	
}
