package com.fran.gestb.dbbarc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.dbbarc.model.ArtTypes;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarc.model.Deps;
import com.fran.gestb.dbbarc.model.SubDeps;
import com.fran.gestb.dbbarc.service.ArtsService;

@RestController
@RequestMapping("/arts")
public class ArtsController {
	@Autowired
	private ArtsService artsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	// --------------------------------- ARTS ------------------------------------------------
	// -------------------Arts----->getAllArtsArtd-------------------------------------------
	@GetMapping(value = "/getAll")
	@ResponseStatus(HttpStatus.OK)
	public List<Arts> getAllArts() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->getAllArtsArt","Entering","..>>...................");
		List<Arts> ArtsList = artsService.getAllArts();
		if (ArtsList == null) throw new EmptyFieldException("No all Arts found");
		consoleLog.logMsg(showLogMsg,"->artsController->getAllArtsArt","Exiting","...................>>..");
		return ArtsList; 
	}
	// -------------------Arts----->getAllArtsInArtType------------------------------------------------
	@PostMapping(value = "/getAllInArtType")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Arts> getAllArtsInArtType(@RequestBody ArtTypes selArtType) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->getAllArtsInArtType","Entering","..>>...................");
		List<Arts> ArtsList = artsService.getAllArtsInArtType(selArtType);
		consoleLog.logMsg(showLogMsg,"->ArtsController->getAllArtsInArtType","Exiting","...................>>..");
		return ArtsList;	
	}
	// -------------------Arts----->getAllArtsInArtType------------------------------------------------
	@PostMapping(value = "/getAllWithArtTypeTag")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Arts> getAllArtsWithArtTypeTag(@RequestBody String selArtTypeTag) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->getAllArtsWithArtTypeTag","Entering","..>>...................");
		List<Arts> ArtsList = artsService.getAllArtsWithArtTypeTag(selArtTypeTag);
		consoleLog.logMsg(showLogMsg,"->ArtsController->getAllArtsWithArtTypeTag","Exiting","...................>>..");
		return ArtsList;	
	}
	// -------------------Arts----->createOne------------------------------------------------
	@PostMapping(value = "/createOne")
	@ResponseStatus(HttpStatus.CREATED)
	public Arts createOneArt(@RequestBody Arts newArt) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->createOneArt","Entering","..>>...................");
		Arts checkArt = artsService.createOneArt(newArt);
		consoleLog.logMsg(showLogMsg,"->ArtsController->createOneArt","Exiting","...................>>..");
		return checkArt;	
	}
	// -------------------Arts----->GetOne------------------------------------------------
	@PostMapping(value="/getOne")
	@ResponseStatus(HttpStatus.CREATED)
	public Arts getOneArt(@RequestBody Arts modArt) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->getOneArt","Entering","..>>...................");
		artsService.getOneArt(modArt);
		consoleLog.logMsg(showLogMsg,"->ArtsController->getOneArt","Exiting","...................>>..");
		return modArt;
	}
	// -------------------Arts----->getOneArtById------------------------------------------------
	@PostMapping(value="/getOneById")
	@ResponseStatus(HttpStatus.CREATED)
	public Arts getOneArtById(@RequestBody long artId) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->getOneArtById","Entering","..>>...................");
		Arts readArt = artsService.getOneArtById(artId);
		consoleLog.logMsg(showLogMsg,"->ArtsController->getOneArtById","Exiting","...................>>..");
		return readArt;
	}
	// -------------------Arts----->update Art------------------------------------------------
	@PostMapping(value="/updOne")
	@ResponseStatus(HttpStatus.OK)
	public Arts updateOneArt(@RequestBody Arts updArt) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateOneArt","Entering","..>>...................");
		Arts checkArt = artsService.updateOneArt(updArt);
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateOneArt","Exiting","...................>>..");
		return checkArt;	
	}
	// -------------------Arts----->updateOneArtSpecial------------------------------------------------
	@PostMapping(value="/updOneSpecial")
	@ResponseStatus(HttpStatus.OK)
	public Arts updateOneArtSpecial(@RequestBody Arts updArt) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateOneArtSpecial","Entering","..>>...................");
		Arts checkArt = artsService.updateOneArtSpecial(updArt);
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateOneArtSpecial","Exiting","...................>>..");
		return checkArt;	
	}
	// -------------------Arts----->updateArtWithArtFactData------------------------------------------------
	@PostMapping(value="/updWithArtFact")
	@ResponseStatus(HttpStatus.OK)
	public Arts updateArtWithArtFactData(@RequestBody Arts updArtFact) {
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateArtWithArtFactData","Entering","..>>...................");
		Arts checkArt = artsService.updateArtWithArtFactData(updArtFact);
		consoleLog.logMsg(showLogMsg,"->ArtsController->updateArtWithArtFactData","Exiting","...................>>..");
		return checkArt;	
	}
	// ------------------- Arts----->DeleteOne------------------------------------------------
	@PostMapping(value="/delOne")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneArt(@RequestBody Arts delArt) {
		consoleLog.logMsg(showLogMsg,"->artsController->delOneArt","Entering","..>>...................");
		artsService.deleteOneArt(delArt);
		consoleLog.logMsg(showLogMsg,"->artsController->delOneArt","Exiting","...................>>..");
		return;
	}
	// -------------------More Arts----->DeleteOne------------------------------------------------
	@DeleteMapping()
	public void delAllArts() {
		consoleLog.logMsg(showLogMsg,"->artsController->delAllArts","Entering","..>>...................");
		artsService.deleteAllArts();
		consoleLog.logMsg(showLogMsg,"->artsController->delAllArts","Exiting","...................>>..");
		return;
	}
	// --------------------------------- DEPS ------------------------------------------------
	// -------------------Deps----->getAllDeps-------------------------------------------
	@GetMapping(value = "/getDeps")
	@ResponseStatus(HttpStatus.OK)
	public List<Deps> getAllDeps() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->getAllDeps","Entering","..>>...................");
		List<Deps> DepsList = artsService.getAllDeps();
		if (DepsList == null) throw new EmptyFieldException("No all Deps found");
		consoleLog.logMsg(showLogMsg,"->artsController->getAllDeps","Exiting","...................>>..");
		return DepsList; 
	}
	// -------------------Deps----->createOneDep------------------------------------------------
	@PostMapping(value = "/createDep")
	@ResponseStatus(HttpStatus.CREATED)
	public Deps createOneDep(@RequestBody Deps newDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->createOneDep","Entering","..>>...................");
		Deps checkDep = artsService.createOneDep(newDep);
		consoleLog.logMsg(showLogMsg,"->artsController->createOneDep","Exiting","...................>>..");
		return checkDep;	
	}
	// -------------------Deps----->getOneDep------------------------------------------------
	@PostMapping(value="/getDep")
	@ResponseStatus(HttpStatus.CREATED)
	public Deps getOneDep(@RequestBody Deps modDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->getOneDep","Entering","..>>...................");
		artsService.getOneDep(modDep);
		consoleLog.logMsg(showLogMsg,"->artsController->getOneDep","Exiting","...................>>..");
		return modDep;
	}
	// -------------------Deps----->updateOneDepSpecial------------------------------------------------
	@PostMapping(value="/updDepSpecial")
	@ResponseStatus(HttpStatus.OK)
	public Deps updateOneDepSpecial(@RequestBody Deps updDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneDepSpecial","Entering","..>>...................");
		Deps checkDep = artsService.updateOneDepSpecial(updDep);
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneDepSpecial","Exiting","...................>>..");
		return checkDep;	
	}
	// -------------------Deps----->updateOneDep------------------------------------------------
	@PostMapping(value="/updDep")
	@ResponseStatus(HttpStatus.OK)
	public Deps updateOneDep(@RequestBody Deps updDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneDep","Entering","..>>...................");
		Deps checkDep = artsService.updateOneDep(updDep);
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneDep","Exiting","...................>>..");
		return checkDep;	
	}
	// ------------------- Deps----->delOneDep------------------------------------------------
	@PostMapping(value="/delDep")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneDep(@RequestBody Deps delDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->delOneDep","Entering","..>>...................");
		artsService.deleteOneDep(delDep);
		consoleLog.logMsg(showLogMsg,"->artsController->delOneDep","Exiting","...................>>..");
		return;
	}
	// ------------------- Deps----->delAllDeps------------------------------------------------
	@PostMapping(value="/delDeps")
	@ResponseStatus(HttpStatus.CREATED)
	public void delAllDeps(@RequestBody Deps delDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->delAllDeps","Entering","..>>...................");
		artsService.deleteAllDeps(delDep);
		consoleLog.logMsg(showLogMsg,"->artsController->delAllDeps","Exiting","...................>>..");
		return;
	}	
	// --------------------------------- SUBDEPS ------------------------------------------------
	// -------------------SubDeps----->getAllSubDeps-------------------------------------------
	@GetMapping(value = "/getSubDeps")
	@ResponseStatus(HttpStatus.OK)
	public List<SubDeps> getAllSubDeps() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->getAllSubDeps","Entering","..>>...................");
		List<SubDeps> SubDepsList = artsService.getAllSubDeps();
		if (SubDepsList == null) throw new EmptyFieldException("No all SubDeps found");
		consoleLog.logMsg(showLogMsg,"->artsController->getAllSubDeps","Exiting","...................>>..");
		return SubDepsList; 
	}
	// -------------------SubDeps----->createOneSubDep------------------------------------------------
	@PostMapping(value = "/createSubDep")
	@ResponseStatus(HttpStatus.CREATED)
	public SubDeps createOneSubDep(@RequestBody SubDeps newSubDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->createOneSubDep","Entering","..>>...................");
		SubDeps checkSubDep = artsService.createOneSubDep(newSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->createOneSubDep","Exiting","...................>>..");
		return checkSubDep;	
	}
	// -------------------SubDeps----->getOneSubDep------------------------------------------------
	@PostMapping(value="/getSubDep")
	@ResponseStatus(HttpStatus.CREATED)
	public SubDeps getOneSubDep(@RequestBody SubDeps modSubDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->getOneSubDep","Entering","..>>...................");
		artsService.getOneSubDep(modSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->getOneSubDep","Exiting","...................>>..");
		return modSubDep;
	}
	// -------------------SubDeps----->updateOneSubDep------------------------------------------------
	@PostMapping(value="/updSubDep")
	@ResponseStatus(HttpStatus.OK)
	public SubDeps updateOneSubDep(@RequestBody SubDeps updSubDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneSubDep","Entering","..>>...................");
		SubDeps checkSubDep = artsService.updateOneSubDep(updSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneSubDep","Exiting","...................>>..");
		return checkSubDep;	
	}
	// -------------------SubDeps----->updateOneSubDepSpecial------------------------------------------------
	@PostMapping(value="/updSubDepSpecial")
	@ResponseStatus(HttpStatus.OK)
	public SubDeps updateOneSubDepSpecial(@RequestBody SubDeps updSubDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneSubDepSpecial","Entering","..>>...................");
		SubDeps checkSubDep = artsService.updateOneSubDepSpecial(updSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneSubDepSpecial","Exiting","...................>>..");
		return checkSubDep;	
	}
	// ------------------- SubDeps----->delOneSubDep------------------------------------------------
	@PostMapping(value="/delSubDep")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneSubDep(@RequestBody SubDeps delSubDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->delOneSubDep","Entering","..>>...................");
		artsService.deleteOneSubDep(delSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->delOneSubDep","Exiting","...................>>..");
		return;
	}
	// ------------------- SubDeps----->delAllSubDeps------------------------------------------------
	@PostMapping(value="/delSubDeps")
	@ResponseStatus(HttpStatus.CREATED)
	public void delAllSubDeps(@RequestBody SubDeps delSubDep) {
		consoleLog.logMsg(showLogMsg,"->artsController->delAllSubDeps","Entering","..>>...................");
		artsService.deleteAllSubDeps(delSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->delAllSubDeps","Exiting","...................>>..");
		return;
	}	
	// -------------------SubDeps----->getAllSubDepsInDep-------------------------------------------
	@PostMapping(value = "/getSubDepsIn")
	@ResponseStatus(HttpStatus.CREATED)
	public List<SubDeps> getAllSubDepsInDep(@RequestBody Deps selectedDep) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->getAllSubDepsInDep","Entering","..>>...................");
		List<SubDeps> SubDepsList = artsService.getAllSubDepsInDep(selectedDep);
		consoleLog.logMsg(showLogMsg,"->artsController->getAllSubDepsInDep","Exiting","...................>>..");
		return SubDepsList; 
	}
	// --------------------------------- ARTTYPES ------------------------------------------------
	// -------------------ArtTypes----->getAllArtTypes-------------------------------------------
	@GetMapping(value = "/getArtTypes")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtTypes> getAllArtTypes() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->getAllArtTypes","Entering","..>>...................");
		List<ArtTypes> ArtTypesList = artsService.getAllArtTypes();
		if (ArtTypesList == null) throw new EmptyFieldException("No all ArtTypes found");
		consoleLog.logMsg(showLogMsg,"->artsController->getAllArtTypes","Exiting","...................>>..");
		return ArtTypesList; 
	}
	// -------------------ArtTypes----->createOneArtType------------------------------------------------
	@PostMapping(value = "/createArtType")
	@ResponseStatus(HttpStatus.CREATED)
	public ArtTypes createOneArtType(@RequestBody ArtTypes newArtType) {
		consoleLog.logMsg(showLogMsg,"->artsController->createOneArtType","Entering","..>>...................");
		ArtTypes checkArtType = artsService.createOneArtType(newArtType);
		consoleLog.logMsg(showLogMsg,"->artsController->createOneArtType","Exiting","...................>>..");
		return checkArtType;	
	}
	// -------------------ArtTypes----->getOneArtType------------------------------------------------
	@PostMapping(value="/getArtType")
	@ResponseStatus(HttpStatus.CREATED)
	public ArtTypes getOneArtType(@RequestBody ArtTypes modArtType) {
		consoleLog.logMsg(showLogMsg,"->artsController->getOneArtType","Entering","..>>...................");
		artsService.getOneArtType(modArtType);
		consoleLog.logMsg(showLogMsg,"->artsController->getOneArtType","Exiting","...................>>..");
		return modArtType;
	}
	// -------------------ArtTypes----->updateOneArtType------------------------------------------------
	@PostMapping(value="/updArtType")
	@ResponseStatus(HttpStatus.OK)
	public ArtTypes updateOneArtType(@RequestBody ArtTypes updArtType) {
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneArtType","Entering","..>>...................");
		ArtTypes checkArtType = artsService.updateOneArtType(updArtType);
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneArtType","Exiting","...................>>..");
		return checkArtType;	
	}
	// -------------------ArtTypes----->updateOneArtTypeSpecial------------------------------------------------
	@PostMapping(value="/updArtTypeSpecial")
	@ResponseStatus(HttpStatus.OK)
	public ArtTypes updateOneArtTypeSpecial(@RequestBody ArtTypes updArtType) {
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneArtTypeSpecial","Entering","..>>...................");
		ArtTypes checkArtType = artsService.updateOneArtTypeSpecial(updArtType);
		consoleLog.logMsg(showLogMsg,"->artsController->updateOneArtTypeSpecial","Exiting","...................>>..");
		return checkArtType;	
	}
	// ------------------- ArtTypes----->delOneArtType------------------------------------------------
	@PostMapping(value="/delArtType")
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneArtType(@RequestBody ArtTypes delArtType) {
		consoleLog.logMsg(showLogMsg,"->artsController->delOneArtType","Entering","..>>...................");
		artsService.deleteOneArtType(delArtType);
		consoleLog.logMsg(showLogMsg,"->artsController->delOneArtType","Exiting","...................>>..");
		return;
	}
	// ------------------- ArtTypes----->delAllArtTypes------------------------------------------------
	@PostMapping(value="/delArtTypes")
	@ResponseStatus(HttpStatus.CREATED)
	public void delAllArtTypes(@RequestBody ArtTypes delArtType) {
		consoleLog.logMsg(showLogMsg,"->artsController->delAllArtTypes","Entering","..>>...................");
		artsService.deleteAllArtTypes(delArtType);
		consoleLog.logMsg(showLogMsg,"->artsController->delAllArtTypes","Exiting","...................>>..");
		return;
	}	
	// -------------------ArtTypes----->getAllArtTypesInSubDep-------------------------------------------
	@PostMapping(value = "/getArtTypesIn")
	@ResponseStatus(HttpStatus.CREATED)
	public List<ArtTypes> getAllArtTypesInSubDep(@RequestBody SubDeps selectedSubDep) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->getAllArtTypesInSubDep","Entering","..>>...................");
		List<ArtTypes> ArtTypesList = artsService.getAllArtTypesInSubDep(selectedSubDep);
		consoleLog.logMsg(showLogMsg,"->artsController->getAllArtTypesInSubDep","Exiting","...................>>..");
		return ArtTypesList; 
	}	
	// -------------------ArtTypes----->checkArtTypesIn-------------------------------------------
	@GetMapping(value = "/checkArtTypesIn")
	@ResponseStatus(HttpStatus.OK)
	public List<ArtTypes> checkGenericArtTypes() throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->artsController->checkGenericArtTypes","Entering","..>>...................");
		List<ArtTypes> ArtTypesList = artsService.checkGenericArtTypes();
		if (ArtTypesList == null) throw new EmptyFieldException("No all ArtTypes found");
		consoleLog.logMsg(showLogMsg,"->artsController->checkGenericArtTypes","Exiting","...................>>..");
		return ArtTypesList; 
	}
	
}
