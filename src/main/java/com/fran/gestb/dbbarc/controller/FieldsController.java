package com.fran.gestb.dbbarc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarc.model.Fields;
import com.fran.gestb.dbbarc.model.Values;
import com.fran.gestb.dbbarc.service.FieldsService;

@RestController
@RequestMapping(value = "/fields")
public class FieldsController {

	@Autowired
	private FieldsService fieldsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	private Fields otherField;
	private List<Fields> allFields;
	
	private List<List<ValueRecord>> valueRecordList;
	
	//----------------------------- Fields-------------------------------------------------------------------
	// -------------------Fields----->getAllFields-----------------------------------------------------------
	@GetMapping(value = "/getAllFields")
	@ResponseStatus(HttpStatus.OK)
	public List<Fields> getAllFields() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getAllFields","Entering","..>>...................");
		allFields = fieldsService.getAllFields();
		if (allFields == null) throw new EmptyFieldException("No Fields found");
		consoleLog.logMsg(showLogMsg,"->FieldsController->getAllFields","Exiting","..>>...................");
		return allFields; 
	}
	// -------------------Fields----->getOneField-----------------------------------------------------------
	@PostMapping(value = "/getOneField")
	@ResponseStatus(HttpStatus.CREATED)
	public Fields getOneField (@RequestBody Fields newField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneField","Entering","..>>...................");
		Fields readField = fieldsService.getOneField(newField);
		if (readField == null) throw new EmptyFieldException("No Fields found");
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneField","Exiting","..>>...................");
		return readField; 
	}
	// -------------------Fields----->addOneField----------------------------------------------------------------
	@PostMapping(value = "/addOneField")
	@ResponseStatus(HttpStatus.CREATED)
	public Fields addOneField(@RequestBody Fields newField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->addOneField","Entering","..>>...................");
		otherField= fieldsService.addOneField(newField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->addOneField","Exiting","..>>...................");
		return otherField;	
	}
	// -------------------Fields----->updateOneField----------------------------------------------------------------
	@PutMapping(value = "/updateOneField")
	@ResponseStatus(HttpStatus.OK)
	public Fields updateOneField(@RequestBody Fields updField) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneField","Entering","..>>...................");
		// Check mandatory parameters in newField
		if ((updField.getFieldName()=="") | (updField==null)) throw new EmptyFieldException("Wrong Field parameters");	
		fieldsService.updateOneField(updField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneField","Exiting","..>>...................");
		return otherField;		
	}
	// -------------------Fields----->updateOneFieldStatus----------------------------------------------------------------
	@PostMapping(value = "/updateOneFieldStatus")
	@ResponseStatus(HttpStatus.OK)
	public Fields updateOneFieldStatus(@RequestBody Fields updField){
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldStatus","Entering","..>>...................");
		fieldsService.updateOneFieldStatus(updField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldStatus","Exiting","..>>...................");
		return otherField;		
	}	
	// -------------------Fields----->loadDefaultFieldsAndValues----------------------------------------------------------------
	@PostMapping(value = "/loadDefault")
	@ResponseStatus(HttpStatus.OK)
	public Fields loadDefaultFieldsAndValues(){
		consoleLog.logMsg(showLogMsg,"->FieldsController->loadDefaultFieldsAndValues","Entering","..>>...................");
		fieldsService.startCreateFieldsAndValues();
		consoleLog.logMsg(showLogMsg,"->FieldsController->loadDefaultFieldsAndValues","Exiting","..>>...................");
		return otherField;		
	}	
	// -------------------Fields----->deleteOneField Field----------------------------------------------------------------
	@PostMapping(value = "/deleteOneField")
	@ResponseStatus(HttpStatus.OK)
	public Fields deleteOneField(@RequestBody Fields delField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->deleteOneField","Entering","..>>...................");
		// Check mandatory parameters in newField
		fieldsService.deleteOneField(delField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->deleteOneField","Exiting","..>>...................");
		return otherField;		
	}	
	// -------------------Fields----->delAllFields----------------------------------------------------------------
	@DeleteMapping()
	public void delAllFields() {
		consoleLog.logMsg(showLogMsg,"->FieldsController->delAllFields","Entering","..>>...................");
		fieldsService.delAllFields();
		consoleLog.logMsg(showLogMsg,"->FieldsController->delAllFields","Exiting","..>>...................");
	}
	//----------------------------- Values-------------------------------------------------------------------
	// -------------------Values----->getOneFieldValuesList------------------------------------------------
	@PostMapping(value = "/getFieldValues")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Values>  getOneFieldValuesList(@RequestBody Fields newField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneFieldValuesList","Exiting","..>>...................");
		List<Values> readFieldValues=fieldsService.getOneFieldValuesList(newField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneFieldValuesList","Exiting","..>>...................");
		return readFieldValues;
	}
	// -------------------Values----->createOneFieldValuesList------------------------------------------------
	@PostMapping(value = "/addFieldValues")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Values> createOneFieldValuesList(@RequestBody List<Values> newFieldValuesList) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->createOneFieldValuesList","Exiting","..>>...................");
		List<Values> readFieldValues=fieldsService.createOneFieldValuesList(newFieldValuesList);
		consoleLog.logMsg(showLogMsg,"->FieldsController->createOneFieldValuesList","Exiting","..>>...................");
		return readFieldValues;
	}
	// -------------------Values----->updateOneFieldValuesList------------------------------------------------
	@PostMapping(value = "/updFieldValues")
	@ResponseStatus(HttpStatus.CREATED)
	public List<Values> updateOneFieldValuesList(@RequestBody List<Values> newFieldValuesList) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldValuesList","Exiting","..>>...................");
		List<Values> readFieldValues=fieldsService.updateOneFieldValuesList(newFieldValuesList);
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldValuesList","Exiting","..>>...................");
		return readFieldValues;
	}
	// -------------------Values----->getFieldsAndValues------------------------------------------------
	@GetMapping(value="/getFieldsAndValues")
	@ResponseStatus(HttpStatus.OK)
	public List<List<ValueRecord>> getAllFieldsAndValues()  {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getFieldAndValues","Entering","..>>...................");
		valueRecordList = fieldsService.getFieldsAndValues();
		consoleLog.logMsg(showLogMsg,"->FieldsController->getFieldAndValues","Exiting","..>>...................");
		return valueRecordList;
	}
	
}
