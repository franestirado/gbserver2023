package com.fran.gestb.dbbarc.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "deps")
public class Deps {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "depId", updatable = false, nullable = false)
	private Long depId;		
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date depDate;	
	@NotNull
	@Size(max = 32)
	@Column(unique = true, nullable=false)
	private String depName;		
	@NotNull
	@Size(max = 64)
	@Column(nullable=false)
	private String depDescription;		
	@NotNull
	@Column(nullable=false)
	@Size(max = 16)
	private String depTag;			
	@Column(nullable=false)
	private boolean depStatus;	
	// Checking attributes
	public boolean checkDepAtributes() {	
		if (this.depDate == null) return false;
		if ( (this.depName.length() < 6) | (this.depName.length() >32) )return false;		
		if ( (this.depDescription.length() < 1) | (this.depDescription.length() >64) )return false;
		//if ( (this.depTag.length() < 1) | (this.depTag.length() >16) )return false;
		return true;
	}
	// constructors
	public Deps() {}
	public Deps(Long depId, Date depDate, String depName, String depDescription, String depTag, boolean depStatus) {
		super();
		this.depId = depId;
		this.depDate = depDate;
		this.depName = depName;
		this.depDescription = depDescription;
		this.depTag = depTag;
		this.depStatus = depStatus;
	}
	//setters and getters
	public Long getDepId() {
		return depId;
	}
	public void setDepId(Long depId) {
		this.depId = depId;
	}
	public Date getDepDate() {
		return depDate;
	}
	public void setDepDate(Date depDate) {
		this.depDate = depDate;
	}
	public String getDepName() {
		return depName;
	}
	public void setDepName(String depName) {
		this.depName = depName;
	}
	public String getDepDescription() {
		return depDescription;
	}
	public void setDepDescription(String depDescription) {
		this.depDescription = depDescription;
	}
	public String getDepTag() {
		return depTag;
	}
	public void setDepTag(String depTag) {
		this.depTag = depTag;
	}
	public boolean isDepStatus() {
		return depStatus;
	}
	public void setDepStatus(boolean depStatus) {
		this.depStatus = depStatus;
	}
	
}