package com.fran.gestb.dbbarc.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "fields")
public class Fields {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "fieldId", updatable = false, nullable = false)
	private Long fieldId;

	@NotNull
	@Column(unique = true, nullable=false)
	@Size(max = 64)
	private String fieldName;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 128)
	private String fieldExplan;

	@Size(max = 512)
	private String fieldValuesString;
	
	@Column(nullable=false)
	private boolean fieldStatus;
	
	// Checking attributes
	public boolean checkFieldAtributes() {
		if (this.fieldName.length() == 0) return false;
		if ( (this.fieldName.length() < 5) | (this.fieldName.length() >20) )return false;
		if (this.fieldExplan.length() == 0) return false;
		if ( (this.fieldExplan.length() < 5) | (this.fieldExplan.length() >64) )return false;
		return true;
	}
	// constructors
	public Fields() {
	}
	public Fields(String fieldName, String fieldExplan) {
		this.fieldName = fieldName;
		this.fieldExplan = fieldExplan;
		this.fieldStatus = true;
	}
	public Fields(Long fieldId, String fieldName, String fieldExplan, String fieldValuesString) {
		this.fieldId = fieldId;
		this.fieldName = fieldName;
		this.fieldExplan = fieldExplan;
		this.fieldValuesString = fieldValuesString;
	}
	//setters and getters
	public Long getFieldId() {
		return this.fieldId;
	}
	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}
	public String getFieldName() {
		return this.fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldExplan() {
		return this.fieldExplan;
	}
	public void setFieldExplan(String fieldExplan) {
		this.fieldExplan = fieldExplan;
	}
	public String getFieldValuesString() {
		return this.fieldValuesString;
	}
	public void setFieldValuesString(String fieldValuesString) {
		this.fieldValuesString = fieldValuesString;
	}
	public boolean isFieldStatus() {
		return fieldStatus;
	}
	public void setFieldStatus(boolean fieldStatus) {
		this.fieldStatus = fieldStatus;
	}
}
