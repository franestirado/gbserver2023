package com.fran.gestb.dbbarc.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "subdeps")
public class SubDeps {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "subDepId", updatable = false, nullable = false)
	private Long subDepId;		
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date subDepDate;	
	@NotNull
	@Size(max = 32)
	@Column(unique = true, nullable=false)
	private String subDepName;		
	@NotNull
	@Size(max = 64)
	@Column(nullable=false)
	private String subDepDescription;		
	@NotNull
	@Column(nullable=false)
	@Size(max = 16)
	private String subDepTag;	
	@NotNull
	@Column(nullable=false)	
	private Long subDepSequence;
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String subDepDepName;
	@NotNull
	@Column(nullable=false)	
	private Long subDepDepId;
	@Column(nullable=false)	
	private boolean subDepStatus;	
	// Checking attributes
	public boolean checkSubDepAtributes() {	
		if (this.subDepDate == null) return false;
		if ( (this.subDepName.length() < 1) | (this.subDepName.length() >32) )return false;		
		if ( (this.subDepDescription.length() < 1) | (this.subDepDescription.length() >64) )return false;
		if ( (this.subDepTag.length() < 1) | (this.subDepTag.length() >16) )return false;
		return true;
	}
	// constructors
	public SubDeps() {}		
	public SubDeps	(	Long subDepId, Date subDepDate, String subDepName, String subDepDescription, String subDepTag,
			 			Long subDepSequence,  String subDepDepName,  Long subDepDepId, boolean subDepStatus) {
		super();
		this.subDepId = subDepId;
		this.subDepDate = subDepDate;
		this.subDepName = subDepName;
		this.subDepDescription = subDepDescription;
		this.subDepTag = subDepTag;
		this.subDepSequence = subDepSequence;
		this.subDepDepName = subDepDepName;
		this.subDepDepId = subDepDepId;
		this.subDepStatus = subDepStatus;
	}
	//setters and getters
	public Long getSubDepId() {
		return subDepId;
	}
	public void setSubDepId(Long subDepId) {
		this.subDepId = subDepId;
	}
	public Date getSubDepDate() {
		return subDepDate;
	}
	public void setSubDepDate(Date subDepDate) {
		this.subDepDate = subDepDate;
	}
	public String getSubDepName() {
		return subDepName;
	}
	public void setSubDepName(String subDepName) {
		this.subDepName = subDepName;
	}
	public String getSubDepDescription() {
		return subDepDescription;
	}
	public void setSubDepDescription(String subDepDescription) {
		this.subDepDescription = subDepDescription;
	}
	public String getSubDepTag() {
		return subDepTag;
	}
	public void setSubDepTag(String subDepTag) {
		this.subDepTag = subDepTag;
	}
	public Long getSubDepSequence() {
		return subDepSequence;
	}
	public void setSubDepSequence(Long subDepSequence) {
		this.subDepSequence = subDepSequence;
	}
	public String getSubDepDepName() {
		return subDepDepName;
	}
	public void setSubDepDepName(String subDepDepName) {
		this.subDepDepName = subDepDepName;
	}
	public Long getSubDepDepId() {
		return subDepDepId;
	}
	public void setSubDepDepId(Long subDepDepId) {
		this.subDepDepId = subDepDepId;
	}
	public boolean isSubDepStatus() {
		return subDepStatus;
	}
	public void setSubDepStatus(boolean subDepStatus) {
		this.subDepStatus = subDepStatus;
	}		
}
