package com.fran.gestb.dbbarc.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "artsfactmodels")
public class ArtsFactModels {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(updatable = false, nullable = false)
	private Long artsFactModelsId;
	@NotNull
	@Column
	private Long factModelsId;
	@NotNull
	@Size(max = 64)
	@Column	
	private String factModelsName;
	@NotNull
	@Column
	private Long artId;
	@NotNull
	@Size(max = 32)
	@Column	
	private String artName;
	// Checking attributes
	
	// constructors
	public ArtsFactModels() {}
	public ArtsFactModels(Long factModelsId, String factModelsName, Long artId, String artName) {
		super();
		this.factModelsId = factModelsId;
		this.artId = artId;
		this.factModelsName = factModelsName;
		this.artName = artName;
	}
	public ArtsFactModels(Long artsFactModelsId, Long factModelsId, String factModelsName, Long artId, String artName) {
		super();
		this.artsFactModelsId = artsFactModelsId;
		this.factModelsId = factModelsId;
		this.artId = artId;
		this.factModelsName = factModelsName;
		this.artName = artName;
	}
	//setters and getters
	public Long getArtsFactModelsId() {
		return artsFactModelsId;
	}
	public void setArtsFactModelsId(Long artsFactModelsId) {
		this.artsFactModelsId = artsFactModelsId;
	}
	public Long getFactModelsId() {
		return factModelsId;
	}
	public void setFactModelsId(Long factModelsId) {
		this.factModelsId = factModelsId;
	}
	public String getFactModelsName() {
		return factModelsName;
	}
	public void setFactModelsName(String factModelsName) {
		this.factModelsName = factModelsName;
	}
	public Long getArtId() {
		return artId;
	}
	public void setArtId(Long artId) {
		this.artId = artId;
	}
	public String getArtName() {
		return artName;
	}
	public void setArtName(String artName) {
		this.artName = artName;
	}		
}
