package com.fran.gestb.dbbarc.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "provs")
public class Provs {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "provId", updatable = false, nullable = false)
	private Long provId;
	@NotNull
	@Size(max = 32)
	private String provCifType;
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date provDate;
	@NotNull
	@Column(unique = true, nullable=false)
	@Size(max = 32)
	private String provCif;
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String provName;
	@NotNull
	@Column(nullable=false)
	@Size(max = 128)
	private String provLongName;
	@NotNull
	@Column
	@Size(max = 16)
	private String provFactType;
	@Column
	@Size(max = 16)
	private String provPayType;
	@Column
	private boolean provFactNote;
	@Column(nullable=false)
	private boolean provStatus;
	// Checking attributes
	public boolean checkProvAtributes() {
		if (this.provDate == null) return false;
		if (this.provCifType.length() == 0) return false;
		if ( (this.provCifType.length() < 3) | (this.provCifType.length() >32) )return false;
		if (this.provCif.length() == 0) return false;
		if ( (this.provCif.length() < 3) | (this.provCif.length() >32) )return false;
		if (this.provName.length() == 0) return false;
		if ( (this.provName.length() < 3) | (this.provName.length() >64) )return false;
		if (this.provLongName.length() == 0) return false;
		if ( (this.provLongName.length() < 3) | (this.provLongName.length() >128) )return false;
		if ( (this.provFactType.length() < 3) | (this.provFactType.length() >16) )return false;
		if ( (this.provPayType.length() < 3) | (this.provPayType.length() >16) )return false;
		if (this.provCif.length() == 0) return false;
		return true;
	}
	// constructors
	public Provs() {
		
	}
	public Provs(Long provId, Date provDate, String provCifType, String provCif,String provName, String provLongName, 
			String provFactType, String provPayType, boolean provFactNote) {
		this.provId = provId;
		this.provDate = provDate;
		this.provCifType = provCifType;
		this.provCif = provCif;
		this.provName = provName;
		this.provLongName = provLongName;
		this.provFactType = provFactType;
		this.provPayType = provPayType;
		this.provFactNote = provFactNote;
	}
	//setters and getters
	public Long getProvId() {
		return provId;
	}
	public void setProvId(Long provId) {
		this.provId = provId;
	}
	public Date getProvDate() {
		return provDate;
	}
	public void setProvDate(Date provDate) {
		this.provDate = provDate;
	}
	public String getProvCifType() {
		return provCifType;
	}
	public void setProvCifType(String provCifType) {
		this.provCifType = provCifType;
	}
	public String getProvCif() {
		return provCif;
	}
	public void setProvCif(String provCif) {
		this.provCif = provCif;
	}
	public String getProvName() {
		return provName;
	}
	public void setProvName(String provName) {
		this.provName = provName;
	}
	public String getProvLongName() {
		return provLongName;
	}
	public void setProvLongName(String provLongName) {
		this.provLongName = provLongName;
	}
	public String getProvFactType() {
		return provFactType;
	}
	public void setProvFactType(String provFactType) {
		this.provFactType = provFactType;
	}
	public String getProvPayType() {
		return provPayType;
	}
	public void setProvPayType(String provPayType) {
		this.provPayType = provPayType;
	}
	public boolean isProvFactNote() {
		return provFactNote;
	}
	public void setProvFactNote(boolean provFactNote) {
		this.provFactNote = provFactNote;
	}
	public boolean isProvStatus() {
		return provStatus;
	}
	public void setProvStatus(boolean provStatus) {
		this.provStatus = provStatus;
	}
}
