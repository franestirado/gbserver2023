package com.fran.gestb.dbbarc.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "gbviews")
public class GbViews {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "gbViewId", updatable = false, nullable = false)
	private Long gbViewId;

	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String gbViewName;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String objType;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String objSet;

	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String objName;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String objLang;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 128)
	private String objText;
	
	// constructors
	public GbViews() {
	}

	public GbViews(String gbViewName,String objType,String objSet,String objName,String objLang,String objText) {
		this.gbViewName = gbViewName;
		this.objName = objName;
		this.objType = objType;
		this.objSet = objSet;
		this.objLang = objLang;
		this.objText = objText;
	}
	public GbViews(Long gbViewId,String gbViewName,String objType,String objSet,String objName,String objLang,String objText) {
		this.gbViewId = gbViewId;
		this.gbViewName = gbViewName;
		this.objName = objName;
		this.objType = objType;
		this.objSet = objSet;
		this.objLang = objLang;
		this.objText = objText;
	}
	public Long getGbViewId() {
		return gbViewId;
	}

	public void setGbViewId(Long gbViewId) {
		this.gbViewId = gbViewId;
	}

	public String getGbViewName() {
		return gbViewName;
	}

	public void setGbViewName(String gbViewName) {
		this.gbViewName = gbViewName;
	}

	public String getObjName() {
		return objName;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	public String getObjType() {
		return objType;
	}

	public void setObjType(String objType) {
		this.objType = objType;
	}

	public String getObjSet() {
		return objSet;
	}

	public void setObjSet(String objSet) {
		this.objSet = objSet;
	}

	public String getObjLang() {
		return objLang;
	}

	public void setObjLang(String objLang) {
		this.objLang = objLang;
	}

	public String getObjText() {
		return objText;
	}

	public void setObjText(String objText) {
		this.objText = objText;
	}
	
	public boolean checkGbViewAtributes() {
		if (this.gbViewName.length() == 0) return false;
		if ( (this.gbViewName.length() < 5) | (this.gbViewName.length() >20) )return false;
		if (this.objName.length() == 0) return false;
		if ( (this.objName.length() < 5) | (this.objName.length() >64) )return false;
		return true;
	}
	
}
