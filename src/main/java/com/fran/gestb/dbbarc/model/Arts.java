package com.fran.gestb.dbbarc.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table(name = "arts")
public class Arts {
	@Id
	@GeneratedValue(strategy = GenerationType. SEQUENCE)
	@Column(name = "artId", updatable = false, nullable = false)
	private Long artId;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column()
	private Date artDate;
	@NotNull
	@Size(max = 16)
	@Column(unique = true, nullable=false)
	private String artReference;
	@NotNull
	@Size(max = 32)
	@Column(unique = true, nullable=false)
	private String artName;	
	@NotNull
	@Size(max = 64)
	@Column(nullable=false)
	private String artLongName;	
	@NotNull
	@Column(nullable=false)
	@Size(max = 16)
	private String artType;	
	@NotNull
	@Column(nullable=false)
	private double artSize;
	@NotNull
	@Column(nullable=true)
	@Size(max = 16)
	private String artMeasurement;
	@NotNull
	@Column(nullable=false)
	private double artUnitPrice;
	@NotNull
	@Column(nullable=false)
	private double artPackageUnits;
	@NotNull
	@Column(nullable=false)
	private double artPackagePrice;
	@NotNull
	@Size(max = 8)
	@Column(nullable=false)
	private String artTax;
	@Column(nullable=false)
	private boolean artStatus;
	// Checking attributes
	public boolean checkArtAtributes() {	
		if (this.artDate == null) return false;
		if ( (this.artName.length() < 1) | (this.artName.length() >32) )return false;		
		if ( (this.artLongName.length() < 1) | (this.artLongName.length() >64) )return false;
		if ( (this.artReference.length() < 1) | (this.artReference.length() >16) )return false;
		if ( (this.artType.length() < 1) | (this.artType.length() >16) )return false;
		if ( (this.artMeasurement.length() < 1) | (this.artMeasurement.length() >16) )return false;
		if ( (this.artTax.length() < 1) | (this.artTax.length() >16) )return false;		
		return true;
	}
	// constructors
	public Arts() {}
	public Arts(Long artId, Date artDate, String artReference, String artName, String artLongName, String artType,
			double artSize, String artMeasurement, double artUnitPrice,  double artPackageUnits,  
			double artPackagePrice, String artTax, boolean artStatus) {
		super();
		this.artId = artId;
		this.artDate = artDate;
		this.artReference = artReference;
		this.artName = artName;
		this.artLongName = artLongName;
		this.artType = artType;
		this.artSize = artSize;
		this.artMeasurement = artMeasurement;
		this.artUnitPrice = artUnitPrice;
		this.artPackageUnits = artPackageUnits;
		this.artPackagePrice = artPackagePrice;
		this.artTax = artTax;
		this.artStatus = artStatus;
	}	
	//setters and getters
	public Long getArtId() {
		return artId;
	}
	public void setArtId(Long artId) {
		this.artId = artId;
	}
	public Date getArtDate() {
		return artDate;
	}
	public void setArtDate(Date artDate) {
		this.artDate = artDate;
	}
	public String getArtReference() {
		return artReference;
	}
	public void setArtReference(String artReference) {
		this.artReference = artReference;
	}
	public String getArtName() {
		return artName;
	}
	public void setArtName(String artName) {
		this.artName = artName;
	}
	public String getArtLongName() {
		return artLongName;
	}
	public void setArtLongName(String artLongName) {
		this.artLongName = artLongName;
	}
	public String getArtType() {
		return artType;
	}
	public void setArtType(String artType) {
		this.artType = artType;
	}
	public double getArtSize() {
		return artSize;
	}
	public void setArtSize(double artSize) {
		this.artSize = artSize;
	}
	public String getArtMeasurement() {
		return artMeasurement;
	}
	public void setArtMeasurement(String artMeasurement) {
		this.artMeasurement = artMeasurement;
	}
	public double getArtUnitPrice() {
		return artUnitPrice;
	}
	public void setArtUnitPrice(double artUnitPrice) {
		this.artUnitPrice = artUnitPrice;
	}
	public double getArtPackageUnits() {
		return artPackageUnits;
	}
	public void setArtPackageUnits(double artPackageUnits) {
		this.artPackageUnits = artPackageUnits;
	}
	public double getArtPackagePrice() {
		return artPackagePrice;
	}
	public void setArtPackagePrice(double artPackagePrice) {
		this.artPackagePrice = artPackagePrice;
	}
	public String getArtTax() {
		return artTax;
	}
	public void setArtTax(String artTax) {
		this.artTax = artTax;
	}
	public boolean isArtStatus() {
		return artStatus;
	}
	public void setArtStatus(boolean artStatus) {
		this.artStatus = artStatus;
	}
	
}
