package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.fran.gestb.dbbarc.model.FactModels;

public interface FactModelsRepository extends JpaRepository<FactModels, Long> {

	FactModels findByFactModelsId(Long factModelsId);
	FactModels findByFactModelsName(String factModelsName);
	List<FactModels> findByFactModelsProvId(Long factModelsProvId);
	List<FactModels> findAllByFactModelsFactType(String factModelsFactType);
	List<FactModels> findAll();
	int deleteByFactModelsProvId(Long factModelsProvId);
	int deleteByFactModelsId(Long factModelsId);
}

