package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.Deps;

public interface DepsRepository extends JpaRepository<Deps, Long> {
	List<Deps> findAll();
	Deps findByDepId(Long depId);
	Deps findByDepName(String depName);
	Deps findByDepTag(String depTag);

}