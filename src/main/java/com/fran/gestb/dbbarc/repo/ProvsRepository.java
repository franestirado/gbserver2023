package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.Provs;

public interface ProvsRepository extends JpaRepository<Provs, Long> {

	Provs findByProvId(Long fieldId);
	List<Provs> findAll();
	List<Provs> findAllByProvFactType(String provFactType);
	List<Provs> findAllByProvPayType(String provPayType);
	List<Provs> findAllByProvFactNote(boolean provFactNote);
	Provs findByProvName(String fieldName);

}
