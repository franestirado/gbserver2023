package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.Values;

public interface ValuesRepository extends JpaRepository <Values, Long> {

	Values findByValueId (Long valueId);
	Values findByValueName(String valueName);
	List<Values> findAll();
	List<Values> findByFieldId(Long fieldId);
	int deleteByFieldId(Long fieldId);
}