package com.fran.gestb.aacommon;

import org.springframework.stereotype.Service;

@Service
public class ConsoleLog {

	public void logMsg (boolean showMsg, String msg1, String msg2, String msg3) {
		if (showMsg == true) {
			System.out.printf(msg1 + "-->" + msg2 + "-->" + msg3 + "-->\n");
		}
	}
	
}
