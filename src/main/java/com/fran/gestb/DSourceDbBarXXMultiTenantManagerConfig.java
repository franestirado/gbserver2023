package com.fran.gestb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Configuration;
import com.fran.gestb.config.model.Bares;
import com.fran.gestb.config.repo.BaresRepository;

@Slf4j
@Configuration
public class DSourceDbBarXXMultiTenantManagerConfig {
	@Autowired
	private BaresRepository baresRepository;
	
    public DSourceDbBarXXMultiTenantManagerConfig (DSourceDbBarXXMultiTenantManager tenantManager) {
    	//System.out.printf("..DSourceDbBarXXMultiTenantManagerConfig...........Constructor...........>>>>>>>>>>\n");
        tenantManager.setTenantResolver(this::tenantResolver);
    }

    private DataSourceProperties tenantResolver(String tenantId) {
    	//System.out.printf("..DSourceDbBarXXMultiTenantManagerConfig....tenantResolver....tenanId...>>>>..."+tenantId+"....<<<<<\n");
    	Bares readBar = baresRepository.findByBarName(tenantId);    	
    	DataSourceProperties properties = new DataSourceProperties();
    	if (readBar!= null) {
            properties.setUrl(readBar.getBarDbUrl()+readBar.getBarDbName());
            properties.setUsername(readBar.getBarDbUserName());
            properties.setPassword(readBar.getBarDbPassword());
    	} else {
            properties.setUrl("jdbc:postgresql://localhost:5432/dbbardefault");
            properties.setUsername("baresadmuser");
            properties.setPassword("Qhucct01!");
    	}
        return properties;
    }
        	
    /*
    private DataSourceProperties tenantResolver(String tenantId) {
        return tenantRepo.findById(tenantId).map(tenant -> {
            DataSourceProperties properties = new DataSourceProperties();
            properties.setUrl(tenant.getUrl());
            properties.setUsername(tenant.getUsername());
            properties.setPassword(tenant.getPassword());
            return properties;
        }).orElseThrow(() -> {
            String msg = "Tenant properties not found for " + tenantId;
            log.error("[!] " + msg);
            return new RuntimeException(msg);
        });
    }
    */
}
