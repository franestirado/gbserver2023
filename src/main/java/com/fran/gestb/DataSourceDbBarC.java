package com.fran.gestb;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "dbbarcEntityManagerFactory", transactionManagerRef = "dbbarcTransactionManager", 
basePackages = { "com.fran.gestb.dbbarc.repo" })
public class DataSourceDbBarC {

	@Autowired
	private Environment env;
	
	//@Primary
	@Bean(name = "dbbarcDataSource")
	public DataSource dbbarcDataSource() {
    	//System.out.printf("--->DataSourceDbBarC-------->dbbarcDataSource-------->\n");    	

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(env.getProperty("dbbarc.datasource.url"));
		dataSource.setUsername(env.getProperty("dbbarc.datasource.username"));
		dataSource.setPassword(env.getProperty("dbbarc.datasource.password"));
		dataSource.setDriverClassName(env.getProperty("dbbarc.datasource.driver-class-name"));		
		return dataSource;
	}
	//@Primary
	@Bean(name = "dbbarcEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean dbbarcEntityManagerFactory() {
    	//System.out.printf("--->DataSourceDbBarC-------->dbbarcEntityManagerFactory-------->\n");    	

		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dbbarcDataSource());
		em.setPackagesToScan("com.fran.gestb.dbbarc.model");
		
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", env.getProperty("dbbarc.jpa.hibernate.ddl-auto"));
        properties.put("hibernate.physical_naming_strategy", env.getProperty("dbbarc.jpa.properties.hibernate.physical_naming_strategy"));
        properties.put("hibernate.id.db_structure_naming_strategy", env.getProperty("dbbarc.jpa.properties.hibernate.id.db_structure_naming_strategy"));

		//properties.put("hibernate.dialect", env.getProperty("dbbarc.jpa.database-platform"));
		//properties.put("hibernate.show-sql", env.getProperty("dbbarc.jpa.show-sql"));

		em.setJpaPropertyMap(properties);
		
		return em;
		
	}
	
	//@Primary
	@Bean(name = "dbbarcTransactionManager")
	public PlatformTransactionManager dbbarcTransactionManager() {
    	//System.out.printf("--->DataSourceDbBarC-------->dbbarcTransactionManager-------->\n");    	

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(dbbarcEntityManagerFactory().getObject());
		
		return transactionManager;
	}
}