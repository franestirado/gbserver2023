package com.fran.gestb.access.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fran.gestb.access.model.User;
import com.fran.gestb.access.repo.IUserRepo;

@Service
public class UserService{

	@Autowired
	private IUserRepo repo;

	public void registrar(User t) {
		repo.save(t);
	}
}
