package com.fran.gestb.access.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.access.model.User;

public interface IUserRepo extends JpaRepository<User, Integer> {

}
